<?php
/**
 * @file   twocol-34-66-two-in-cols-flipped.tpl.php
 * @author Jon Garcia <jgarcia@mercy.edu>
 * @date   Fri April 25 2:00:00 2014
 *
 * @brief  Template for the three 34/66 with 2 columns in flipped.
 *
 *
 */
?>
<div class="panel-display twocol-34-66-two-in-cols-flipped clear-block" <?php if (!empty($css_id)) { print "id=\"$css_id\"";} ?>>

  <div class="panel-panel line">
    <div class="panel-panel unitnav left">
      <div class="inside">
        <?php print $content['left']; ?>
      </div>
    </div>

    <div class="panel-panel line">
      <div class="panel-panel unit right lastUnit">
         <div class="courses-group inside">
          <?php print $content['courses_group']; ?>
        </div>
         <div class="how-to-apply-group inside">
          <?php print $content['how_to_apply']; ?>
        </div>
        <div class="faqs-group inside">
          <?php print $content['faqs']; ?>
        </div>
        <div class="faculty-group inside">
          <?php print $content['faculty']; ?>
        </div>

        <div class="panel-panel unit top-middle overview-tab firstUnit">
          <div class="inside">
            <?php print $content['overview_middle']; ?>
          </div>
        </div>

        <div class="panel-panel unit top-right overview-tab lastUnit">
          <div class="inside">
            <?php print $content['overview_right']; ?>
          </div>
        </div>
        <div class="panel-panel unit top-middle programs-clear-left firstUnit">
          <div class="program-info-tab inside">
            <?php print $content['program_info_middle']; ?>
          </div>
        </div>

        <div class="panel-panel unit top-right lastUnit">
          <div class="program-info-tab inside">
            <?php print $content['program_info_right']; ?>
          </div>
        </div>
        <div class="panel-panel unit bottom-middle firstUnit">
          <div class="program-outcomes inside">
            <?php print $content['program_outcomes']; ?>
          </div>
        </div>
        <div class="panel-panel unit bottom-right lastUnit">
          <div class="learning-objectives inside">
            <?php print $content['learning_objectives']; ?>
          </div>
        </div>


      </div>
    </div>
  </div>

  <div class="panel-panel footer">
    <div class="inside">
      <?php print $content['footer']; ?>
    </div>
  </div>
</div>
