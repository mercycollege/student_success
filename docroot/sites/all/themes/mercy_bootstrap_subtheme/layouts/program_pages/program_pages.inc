<?php
/**
 * @author Jon Garcia <jgarcia@mercy.edu>
 * @date   Fri April 25 2:00:00 2014
 *
 * @brief  Template for the three 34/66 with 2 columns in flipped.
 *
 *
 */

// Plugin definition.
$plugin = array(
  'title' => t('Program Pages'),
  'category' => t('Columns: 2'),
  'icon' => 'program_pages.png',
  'theme' => 'program_pages',
  'css' => 'program_pages.css',
  'regions' => array(
    'left' => t('Left'),
    'courses_group' => t('Curriculum'),
    'how_to_apply' => t('How To Apply'),
    'faqs' => t('Faqs'),
    'faculty' => t('faculty'),
    'overview_middle' => t('Overview middle'),
    'overview_right' => t('Overview right'),
    'program_info_middle' => t('Program info middle'),
    'program_info_right' => t('Program info right'),
    'program_outcomes' => t('Program Outcomes'),
    'learning_objectives' => t('Learning Objectives'),
    'footer' => t('Footer'),
  ),
);
