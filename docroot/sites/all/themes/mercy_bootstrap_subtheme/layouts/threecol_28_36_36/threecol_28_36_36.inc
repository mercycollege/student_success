<?php

// Plugin definition
$plugin = array(
  'title' => t('Three column 28/36/36'),
  'category' => t('Columns: 3'),
  'icon' => 'threecol_28_36_36.png',
  'theme' => 'threecol_28_36_36',
  'css' => 'threecol_28_36_36.css',
  'regions' => array(
    'top' => t('Top'),
    'left' => t('Left side'),
    'middle' => t('Middle column'),
    'right' => t('Right side'),
    'bottom' => t('Bottom')
  ),
);
