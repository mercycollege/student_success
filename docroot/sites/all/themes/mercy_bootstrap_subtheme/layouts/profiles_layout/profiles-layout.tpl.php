<?php
?>
<div class="panel-display user-profile-layout clear-block" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>

<div class="panel-panel line">
<div class="left">
  <div class="username">
    <div class="name">
     <?php print $content['username']; ?>
    </div>
  </div>
</div>

<div class="lastUnit filter-container">
 <div class="panel-panel search-filter">
    <div class="inside">
      <?php print $content['search-filter']; ?>
    </div>
  </div>

 <div class="panel-panel dept-filter">
    <div class="inside">
      <?php print $content['dept-filter']; ?>
    </div>
  </div>
</div>
</div>

  <div class="panel-panel picture lastUnit" id="moveDivTarget">
  </div>

  <div class="panel-panel line"> 
  <div class="panel-panel unit left">
      <?php if ($content['left']) : ?>
      <div class="inside margin-bottom-fix">
        <?php print $content['left']; ?>
      </div>
    <?php endif; ?>
      
<?php if ($content['cv']): ?>
      <div class="inside margin-bottom-fix profile-line-below">
        <?php print $content['cv']; ?>
      </div>
    <?php endif;?>
    <?php if ($content['pro']): ?>
      <div class="margin-bottom-fix inside">
        <?php print $content['pro']; ?>
      </div>
    <?php endif;?>
    </div>

    <div class="panel-panel picture lastUnit" id="moveDiv">
      <div class="inside">
        <?php print $content['picture']; ?>
      </div>
       <div class="inside">
        <?php print $content['title']; ?>
      </div>
       <div class="inside">
        <?php print $content['contact']; ?>
      </div>
      <div class="inside">
        <?php print $content['linkedin']; ?>
      </div>
    </div>
  </div>

  <div class="panel-panel footer">
    <div class="inside">
      <?php print $content['footer']; ?>
    </div>
  </div>
</div>

