<?php
/**
 * @file   twocol_77_23_stacked.inc
 * @author António P. P. Almeida <appa@perusio.net>
 * @date   Fri Apr 15 16:51:31 2011
 *
 * @brief  The panels layout plugin for the two column 77/23 stacked layout.
 *
 *
 */

// Plugin definition.
$plugin = array(
  'title' => t('User Profile Layout'),
  'category' => t('Columns: 2'),
  'icon' => 'profiles_layout.png',
  'theme' => 'profiles_layout',
  'css' => 'profiles_layout.css',
  'regions' => array(
    'username' => t('username'),
    'search-filter' => t('search-filter'),
    'dept-filter' => t('dept-filter'),
    'left' => t('left'),
    'cv' => t('cv'),
    'pro' => t('pro'),
    'picture' => t('picture'),
    'title' => t('title'),
    'contact' => t('contact'),
    'linkedin' => t('Linkedin'),
    'footer' => t('footer'),
  ),
);
