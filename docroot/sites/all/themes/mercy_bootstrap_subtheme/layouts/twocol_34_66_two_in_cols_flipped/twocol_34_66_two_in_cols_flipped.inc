<?php
/**
 * @file   twocol-34-66-two-in-cols-flipped.tpl.php
 * @author Jon Garcia <jgarcia@mercy.edu>
 * @date   Fri April 25 2:00:00 2014
 *
 * @brief  Template for the three 34/66 with 2 columns in flipped.
 *
 *
 */

// Plugin definition.
$plugin = array(
  'title' => t('34/66 with 2 columns in flipped'),
  'category' => t('Columns: 2'),
  'icon' => 'twocol_34_66_two_in_cols_flipped.png',
  'theme' => 'twocol_34_66_two_in_cols_flipped',
  'css' => 'twocol_34_66_two_in_cols_flipped.css',
  'regions' => array(
    'header' => t('Header'),
    'left' => t('Left'),
    'right' => t('Right'),
    'bottom_middle' => t('Bottom middle'),
    'bottom_right' => t('Bottom right'),
    'footer' => t('Footer'),
  ),
);
