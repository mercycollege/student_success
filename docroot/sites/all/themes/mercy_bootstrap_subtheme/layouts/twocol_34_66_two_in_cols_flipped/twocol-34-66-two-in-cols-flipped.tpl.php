<?php
/**
 * @file   twocol-34-66-two-in-cols-flipped.tpl.php
 * @author Jon Garcia <jgarcia@mercy.edu>
 * @date   Fri April 25 2:00:00 2014
 *
 * @brief  Template for the three 34/66 with 2 columns in flipped.
 *
 *
 */
?>
<div class="panel-display twocol-34-66-two-in-cols-flipped clear-block" <?php if (!empty($css_id)) { print "id=\"$css_id\"";} ?>>

  <div class="panel-panel line header-space">
    <div class="panel-panel panel-header lastUnit">
      <?php print $content['header']; ?>
    </div>
  </div>

  <div class="panel-panel line">
    <div class="panel-panel unitnav left">
      <div class="inside">
        <?php print $content['left']; ?>
      </div>
    </div>

    <div class="panel-panel line">
      <div class="panel-panel unit right lastUnit">
         <div class="courses-group inside">
        <?php print $content['right']; ?>
      </div>

        <div class="panel-panel unit top-middle firstUnit">
          <div class="inside">
            <?php print $content['bottom_middle']; ?>
          </div>
        </div>

        <div class="panel-panel unit top-right lastUnit">
          <div class="inside">
            <?php print $content['bottom_right']; ?>
          </div>
        </div>
      </div>

     

    </div>
  </div>

  <div class="panel-panel footer">
    <div class="inside">
      <?php print $content['footer']; ?>
    </div>
  </div>
</div>
