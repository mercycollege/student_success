<?php
/**
 * @file   twocol-34-66-three-in-cols-flipped.tpl.php
 * @author Jon Garcia <jgarcia@mercy.edu>
 * @date   Fri April 25 12:00:00 2014
 *
 * @brief  Template for the 34/66 with 3 columns in flipped
 *
 *
 */

// Plugin definition.
$plugin = array(
  'title' => t('34/66 with 3 columns in flipped'),
  'category' => t('Columns: 2'),
  'icon' => 'twocol_34_66_three_in_cols_flipped.png',
  'theme' => 'twocol_34_66_three_in_cols_flipped',
  'css' => 'twocol_34_66_three_in_cols_flipped.css',
  'regions' => array(
    'header' => t('Header'),
    'left' => t('Left'),
    'right' => t('Right'),
    'middle_left' => t('Middle Left'),
    'middle_center' => t('Middle Center'),
    'middle_right' => t('Middle Right'),
    'footer' => t('Footer'),
  ),
);
