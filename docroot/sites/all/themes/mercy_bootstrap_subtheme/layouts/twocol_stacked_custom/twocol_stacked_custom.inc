<?php

// Plugin definition
$plugin = array(
  'title' => t('Two column stacked Custom'),
  'category' => t('Columns: 2'),
  'icon' => 'twocol_stacked_custom.png',
  'theme' => 'twocol_stacked_custom',
  'css' => 'twocol_stacked_custom.css',
  'regions' => array(
    'top' => t('Top'),
    'left' => t('Left side'),
    'right' => t('Right side'),
    'bottom' => t('Bottom')
  ),
);
