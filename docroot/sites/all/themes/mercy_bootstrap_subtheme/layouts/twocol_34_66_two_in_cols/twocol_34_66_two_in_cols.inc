<?php
/**
 * @file   twocol-34-66-two-in-cols.tpl.php
 * @author Jon Garcia <jgarcia@mercy.edu>
 * @date   Fri April 25 12:00:00 2014
 *
 * @brief  Template for the 34/66 with 2 columns in.
 *
 *
 */

// Plugin definition.
$plugin = array(
  'title' => t('34/66 with 2 columns in'),
  'category' => t('Columns: 2'),
  'icon' => 'twocol_34_66_two_in_cols.png',
  'theme' => 'twocol_34_66_two_in_cols',
  'css' => 'twocol_34_66_two_in_cols.css',
  'regions' => array(
    'header' => t('Header'),
    'left' => t('Left'),
    'top_middle' => t('Top middle'),
    'top_right' => t('Top right'),
    'right' => t('Right'),
    'footer' => t('Footer'),
  ),
);
