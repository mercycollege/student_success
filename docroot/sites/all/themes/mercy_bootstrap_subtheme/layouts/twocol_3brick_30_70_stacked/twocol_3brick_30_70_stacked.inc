<?php
/**
 * @file   twocol_3brick_30_70_stacked.inc
 * @author Jonas Flint
 * @date   Aug 6, 2014
 *
 * 
 *
 *
 */

// Plugin definition.
$plugin = array(
  'title' => t('twocol_3brick_30_70_stacked'),
  'category' => t('Columns: 2'),
  'icon' => 'twocol_3brick_30_70_stacked.png',
  'theme' => 'twocol_3brick_30_70_stacked',
  'css' => 'twocol_3brick_30_70_stacked.css',
  'regions' => array(
    'top_left' => t('Top Left'),
    'top_middle' => t('Top Middle'),
    'top_right' => t('Top Right'),
    'left' => t('Left'),
    'right' => t('Right'),
    'footer' => t('Footer'),
  ),
);
