<?php
/**
 * @file
 * Template for a 2 column panel layout.
 *
 * This template provides a two column panel display layout, with
 * additional areas for the top and the bottom.
 *
 * Variables:
 * - $id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 *   panel of the layout. This layout supports the following sections:
 *   - $content['top']: Content in the top row.
 *   - $content['left']: Content in the left column.
 *   - $content['right']: Content in the right column.
 *   - $content['bottom']: Content in the bottom row.
 */
?>
 <div class="panel-panel title-container field-mp-left-column-title title-bevel-corners"> <?php print $content['Title']; ?> </div>
<div class="clearfix panel-display newsroom-article-panel" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>
<div class="pane-content content-bevel-corners">
    <div class="panel-panel">
      <div class="inside"><?php print $content['Date']; ?></div>
    </div>
    <div class="panel-panel">
      <div class="inside"><?php if ($content['Campus']){

        print $content['Campus']."&nbsp|"; } ?> 

        <span class="article-category"> <?php print $content['Category']; ?> </span>
        </div>
       <span class='st st_sharethis' displayText=''></span>
<span class='st st_googleplus' displayText=''></span>
<span class='st st_facebook' displayText=''></span>
<span class='st st_twitter' displayText=''></span>
<span class='st st_linkedin' displayText=''></span>
<span class='st st_pinterest' displayText=''></span>
<span class='st st_email' displayText=''></span>
<script type="text/javascript">var switchTo5x=true;</script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({publisher: "123609b5-208b-465b-bc4e-87952d3c89cd", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>

    </div>


  <div class="center-wrapper">

  <div class="photo-container"><?php print $content['Photo']; ?>
    </div>
    </div>

    <div class="panel-col-last panel-panel main-content-container">
      <div class="inside"><?php print $content['MainContent']; ?></div>
    </div>
  

   
   </div>
   </div>
   <div class="title-bevel-corners" id="content-bottom-corner-fix"> </div>
   <div class="clearfix panel-display newsroom-article-panel" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>

    <div class="panel-col-bottom panel-panel boilerplate-container">
      <div class="inside"><?php print $content['Boilerplate']; ?></div>
    </div>
</div>
