<?php

// Plugin definition
$plugin = array(
  'title' => t('newsroom_article'),
  'category' => t('Columns: 2'),
  'icon' => 'newsroom_article.png',
  'theme' => 'newsroom_article',
  'css' => 'newsroom_article.css',
  'regions' => array(
    'Title' => t('Title'),
    'Date' => t('Date'),
    'Campus' => t('Campus'),
    'Category' => t('Category'),
    'Photo' => t('Photo'),
    'MainContent' => t('Maincontent'),
    'Boilerplate' => t('Boilerplate'),
  ),
);
