<?php

/**
 * implementation of hook_panels_layouts
 */
// Plugin definition
$plugin = array(
  'title' => t('Three column 25/50/25 Custom'),
  'category' => t('Columns: 3'),
  'icon' => 'threecol_25_50_25_custom.png',
  'theme' => 'threecol_25_50_25_custom',
  'theme arguments' => array('id', 'content'),
  'css' => 'threecol_25_50_25_custom.css',
  'regions' => array(
    'top' => t('Top'),
    'left' => t('Left side'),
    'middle' => t('Middle column'),
    'right' => t('Right side'),
    'bottom' => t('Bottom'),
  ),
);

