<?php

// Plugin definition
$plugin = array(
  'title' => t('Three column 33/34/33 Custom'),
  'category' => t('Columns: 3'),
  'icon' => 'threecol_33_34_33_custom.png',
  'theme' => 'threecol_33_34_33_custom',
  'css' => 'threecol_33_34_33_custom.css',
  'regions' => array(
    'top' => t('Top'),
    'left' => t('Left side'),
    'middle' => t('Middle column'),
    'right' => t('Right side'),
    'bottom' => t('Bottom')
  ),
);
