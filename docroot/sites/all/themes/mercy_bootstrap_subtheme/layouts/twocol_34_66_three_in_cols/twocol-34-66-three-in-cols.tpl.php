<?php
/**
 * @file   twocol-34-66-three-in-cols.tpl.php
 * @author Jon Garcia <jgarcia@mercy.edu>
 * @date   Fri April 25 1:00:00 2014
 *
 * @brief  Template for the 34/66 with 3 columns in.
 *
 *
 */
?>
<div class="panel-display smaller-title twocol_34_66_three_in_cols clear-block" <?php if (!empty($css_id)) { print "id=\"$css_id\"";} ?>>

 
  <div class="panel-panel line header-space">
    <div class="panel-panel cuatro panel-header">
      <?php print $content['header']; ?>
    </div>
  </div>

  <div class="panel-panel line">
    <div class="panel-panel unit left">
      <div class="inside">
        <?php print $content['left']; ?>
      </div>
    </div>

    <div class="panel-panel line">
      <div class="panel-panel right lastUnit">
       

        <div class="panel-panel unit middle-left firstUnit">
          <div class="uno">
            <?php print $content['middle_left']; ?>
          </div>
        </div>

        <div class="panel-panel unit middle-center ">
          <div class="dos">
            <?php print $content['middle_center']; ?>
          </div>
        </div>

         <div class="panel-panel unit middle-right">
          <div class="tres">
            <?php print $content['middle_right']; ?>
          </div>
        </div>

         <div class="clearer inside">
        <?php print $content['right']; ?>
      </div>

      </div>
    </div>
  </div>

  <div class="panel-panel cuatro footer">
    <div class="inside">
      <?php print $content['footer']; ?>
    </div>
  </div>
</div>


