<?php
/**
 * @file   twocol-34-66-three-in-cols.tpl.php
 * @author Jon Garcia <jgarcia@mercy.edu>
 * @date   Fri April 25 1:00:00 2014
 *
 * @brief  Template for the 34/66 with 3 columns in.
 *
 *
 */

// Plugin definition.
$plugin = array(
  'title' => t('34/66 with 3 columns in'),
  'category' => t('Columns: 2'),
  'icon' => 'twocol_34_66_three_in_cols.png',
  'theme' => 'twocol_34_66_three_in_cols',
  'css' => 'twocol_34_66_three_in_cols.css',
  'regions' => array(
    'header' => t('Header'),
    'left' => t('Left'),
    'middle_left' => t('Middle Left'),
    'middle_center' => t('Middle Center'),
    'middle_right' => t('Middle Right'),
    'footer' => t('Footer'),
    'right' => t('Right'),
  ),
);
