<?php

// Plugin definition
$plugin = array(
  'title' => t('Two column 33 67 stacked'),
  'category' => t('Columns: 2'),
  'icon' => 'twocol_33_67_stacked.png',
  'theme' => 'twocol_33_67_stacked',
  'css' => 'twocol_33_67_stacked.css',
  'regions' => array(
    'top' => t('Top'),
    'left' => t('Left side'),
    'right' => t('Right side'),
    'bottom' => t('Bottom')
  ),
);
