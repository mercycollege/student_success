'use strict';

//=======================================================
// Getting Started
//=======================================================
// If you haven't yet, install nvm:
// https://github.com/creationix/nvm

// Run the commands from the theme directory

// Use the right version of node with:
// nvm use

// If that version of node isn't installed, install it with:
// nvm install

// Install npm dependencies with
// npm install

// Compiles Sass
// npm run gulp -- sass

// Runs the watch command
// npm run gulp -- watch

//=======================================================
// Include gulp
//=======================================================
var gulp = require('gulp');

//=======================================================
// Include Our Plugins
//=======================================================
var sass       = require('gulp-sass');
var prefix     = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var sync       = require('browser-sync');
var filter     = require('gulp-filter');


//=======================================================
// Compile Our Sass
//=======================================================
gulp.task('sass', function() {
  gulp.src('./sass/{,**/}*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass({ outputStyle: 'nested' })
      .on('error', sass.logError))
    .pipe(prefix({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(sourcemaps.write('./maps'))
    .pipe(gulp.dest('./css'))
    .pipe(filter('*.css'))
    .pipe(sync.reload({
      stream: true
    }));
});

//=======================================================
// Watch and recompile sass.
//=======================================================
gulp.task('watch', function() {

  // BrowserSync proxy setup
  // Swap this with your local env url.
  sync({
      proxy: 'http://mercy.mcdev'
  });

  // Watch all my sass files and compile sass if a file changes.
  gulp.watch('sass/{,**/}*.scss', ['sass']);

});

//=======================================================
// Default Task
//=======================================================
gulp.task('default', ['sass']);
