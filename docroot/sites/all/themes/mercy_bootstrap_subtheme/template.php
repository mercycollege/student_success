<?php
/**
 * @file
 * template.php
 */
function mercy_bootstrap_subtheme_preprocess_page(&$vars, $hook) {
  $vars['page_path'] = $_GET['q'];
  if ($vars['page_path'] == 'school-lookup') {
    drupal_add_js('sites/all/themes/mercy_bootstrap_subtheme/js/ceeb_codes.js');
  }
  // Add JS & CSS by node type
  if (isset($vars['node']) && $vars['node']->type == 'programs') {
    drupal_add_js( 'sites/all/themes/mercy_bootstrap_subtheme/js/programs_pages.js' );
    drupal_add_js( 'sites/all/themes/mercy_bootstrap_subtheme/js/event_crm_codes.js' );
    drupal_add_css( 'sites/all/themes/mercy_bootstrap_subtheme/css/programs_pages.css' );
    drupal_add_js( 'sites/all/themes/mercy_bootstrap_subtheme/js/on_submit_tracking.js' );
    // Adds degree type to title.
  }
  if (isset($vars['node']) && $vars['node']->type == 'landing_page') {
    drupal_add_css( 'sites/all/themes/mercy_bootstrap_subtheme/css/landing.css' );
    drupal_add_js( 'sites/all/themes/mercy_bootstrap_subtheme/js/event_crm_codes.js' );
    drupal_add_js( 'sites/all/themes/mercy_bootstrap_subtheme/js/program_codes.js' );
    drupal_add_js( 'sites/all/themes/mercy_bootstrap_subtheme/js/on_submit_tracking.js' );
  }
  if (isset($vars['node']) && $vars['node']->type == 'calendar') {
    drupal_add_js( 'sites/all/themes/mercy_bootstrap_subtheme/js/program_codes.js' );
    drupal_add_js( 'sites/all/themes/mercy_bootstrap_subtheme/js/on_submit_tracking.js' );
  }
  if (isset($vars['node']) && $vars['node']->type == 'self_manage_dasa') {
    drupal_add_css( 'sites/all/themes/mercy_bootstrap_subtheme/css/dasa.css' );
  }
  if (isset($vars['node'])) {
      drupal_add_js( 'sites/all/themes/mercy_bootstrap_subtheme/js/responsive_menu.js' );
      drupal_add_css('sites/all/themes/mercy_bootstrap_subtheme/css/responsive_menu.css');
  }
   if (isset($vars['node']->type)) {
    // If the content type's machine name is "my_machine_name" the file
    // name will be "page--my-machine-name.tpl.php".

    $vars['theme_hook_suggestions'][] = 'page__' . $vars['node']->type;
  }

  // Set variable for primary links menu that will be used as mobile version.
  $primary_links_mobile = menu_navigation_links('navigation');
  $vars['primary_links_mobile'] = theme('links__navigation',
                                         array('links' => $primary_links_mobile,
                                               'attributes' => array('class' => array('menu', 'nav', 'navbar-nav',)),
                                              )
                                       );
}

//@todo load only where needed
libraries_load('owlcarousel');

/**
 * Implements hook_views_default_views_alter().
 */
  function mercy_bootstrap_views_default_views_alter(&$views) {
    if (isset($views['events_calendar'])) {
      // fix issue https://drupal.org/node/1397986 by setting the missing option
      $handler_day =& $views['events_calendar']->display['day']->handler;
      $handler_day->display->display_options['style_options']['groupby_times'] = 'hour';
      $handler_week =& $views['events_calendar']->display['week']->handler;
      $handler_week->display->display_options['style_options']['groupby_times'] = 'hour';
    }
  }
function mercy_bootstrap_subtheme_preprocess_html(&$vars) {
//adding clean css identifier of site name to every sub-site for easier styling {
  $site_name = variable_get('site_name');
  $vars['classes_array'][] = drupal_clean_css_identifier($site_name);

  // Force IE to use latest mode

  $IE_mode = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'http-equiv' => 'X-UA-Compatible',
      'content' => 'IE=edge'
      ),
    );
  drupal_add_html_head($IE_mode, 'http-equiv');
  // Adding a itunes id meta tag to show app when visited from an IOS device
#  $apple_itunes_app = array(
#  '#tag' => 'meta',
#  '#attributes' => array(
#    'name' => 'apple-itunes-app',
#    'content' => 'app-id=694873759',
#    ),
#  );
#  drupal_add_html_head($apple_itunes_app, 'apple_itunes_app');
}

/**
 * Implements hook_preprocess_views_view_fields().
 */
function mercy_bootstrap_subtheme_preprocess_views_view_fields(&$vars) {
  $view = $vars['view'];
  if ($view->name == 'main_page_top_slider') {

    // Default orange if no brand color is set.
    if (!isset($vars['fields']['field_slide_brand_color'])) {
      $vars['field_slide_brand_color'] = 'bg-mercy-orange';
    }
    else {
      $vars['field_slide_brand_color'] = $vars['fields']['field_slide_brand_color']->content;
    }

    // Check for values in other fields.
    $fields = array(
      'field_title',
      'field_image',
      'field_video_link',
      'field_slide_brand_label',
      'field_slider_description',
      'field_thumbnail_title',
      'field_thumbnail_description',
      'field_slider_button',
      'field_slide_overlay',
    );
    foreach ($fields as $id) {
      if (!isset($vars['fields'][$id])) {
        $vars[$id] = '';
      }
      else {
        $vars[$id] = $vars['fields'][$id]->content;
      }
    }

  }
}
/**
 * Implements hook_form_process().
 *
 * Bootstrap 7.x-3.6 stopped processing select elements
 * so the required "form-control" class is re-added here.
 */
function mercy_bootstrap_subtheme_form_process($element, &$form_state, &$form) {
  if (!empty($element['#type']) && $element['#type'] == "select") {
    $element['#attributes']['class'][] = 'form-control';
  }
  return $element;
}
