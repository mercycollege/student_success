<?php

/**
 * @file
 * Default theme implementation to display the block
 * for the system default navigation menu.
 *
 */
?>
<div class="primary-links <?php print $classes; ?>"<?php print $attributes; ?>>
  <div class="primary-links__admin">
    <?php print render($title_prefix); ?>
    <?php print render($title_suffix); ?>
  </div>
  <?php print $content ?>
</div>
