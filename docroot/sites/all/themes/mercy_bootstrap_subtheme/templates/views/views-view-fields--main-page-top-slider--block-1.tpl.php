<?php

/**
 * @file
 * Views template for fields output of the main slider for the Mercy homepage.
 */
?>
<div class="slider__slide-wrapper <?php print $field_slide_brand_color; ?> <?php print $field_slide_overlay; ?>">
  <div class="slider__slide-image">
    <?php print $field_image; ?>
  </div>
  <div class="slider__slide-text">
    <div class="slider__slide-label">
      <?php print $field_slide_brand_label; ?>
    </div>
    <?php if ($field_video_link): ?>
      <div class="slider__slide-video-link">
        <?php print $field_video_link; ?>
      </div>
    <?php endif; ?>
    <h3 class="slider__slide-title"><?php print $field_title; ?></h3>
    <p class="slider__slide-description"><?php print $field_slider_description; ?></p>
    <div class="slider__slide-cta">
      <?php print $field_slider_button; ?>
    </div>
  </div>
</div>
