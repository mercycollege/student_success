<?php

/**
 * Decode caption when displayed as entity within a view.
 * Allows emoji characters to be displayed in the cpation.
 *
 * var string $output
 *   Caption from the instagram photo
 */

print utf8_decode($output);
