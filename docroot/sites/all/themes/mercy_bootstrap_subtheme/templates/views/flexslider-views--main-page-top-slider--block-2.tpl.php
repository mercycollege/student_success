<?php

/**
 * @file
 * Views style output template for the slider controller of the main slider
 * on the Mercy homepage.
 */
?>
<div class="slider-controller">
  <?php print theme('flexslider', array('items' => $items, 'settings' => $settings)); ?>
</div>
