<?php

/**
 * @file
 * Views style output template for the main slider on the Mercy homepage.
 */
?>
<div class="slider--thumbnail-nav">
  <?php print theme('flexslider', array('items' => $items, 'settings' => $settings)); ?>
</div>
