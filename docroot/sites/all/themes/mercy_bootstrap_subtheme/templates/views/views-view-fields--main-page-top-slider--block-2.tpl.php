<?php

/**
 * @file
 * Views template for fields output of the slider controller
 * for the Mercy homepage slider.
 */
?>
<div class="slider-controller__slide-wrapper <?php print $field_slide_brand_color; ?>">
  <div class="slider-controller__slide-image">
    <?php print $field_image; ?>
  </div>
  <div class="slider-controller__slide-text">
    <p class="slider-controller__slide-description">
      <span class="slider-controller__slide-label"><?php print $field_thumbnail_title; ?></span>
      <?php print $field_thumbnail_description; ?>
    </p>
  </div>
</div>
