<?php
    $title=drupal_get_title();
/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see bootstrap_preprocess_page()
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see bootstrap_process_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup themeable
 */

?>


<?php if (($title == 'Graduation Application')||($title == 'Graduate Admissions Online Application')) { ?>
<!-- Tag for Activity Group: IP157158: Mercy College Spring, Activity Name: BROWN Mercy College Spring APPLY NOW Graduate Application~IP157158, Activity ID: 2549581 -->
<!-- Expected URL: https://www.mercy.edu/apply-now/graduate-admissions-online-application -->

<!--
Activity ID: 2549581
Activity Name: BROWN Mercy College Spring APPLY NOW Graduate Application~IP157158
Activity Group Name: IP157158: Mercy College Spring
-->

<!--
Start of DoubleClick Floodlight Tag: Please do not remove
Activity name of this tag: BROWN Mercy College Spring APPLY NOW Graduate Application~IP157158
URL of the webpage where the tag is expected to be placed: https://www.mercy.edu/apply-now/graduate-admissions-online-application
This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
Creation Date: 07/10/2015
-->
<script type="text/javascript">
var axel = Math.random() + "";
var a = axel * 10000000000000;
document.write('<iframe src="https://4652774.fls.doubleclick.net/activityi;src=4652774;type=ip1570;cat=brown0;ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
</script>
<noscript>
<iframe src="https://4652774.fls.doubleclick.net/activityi;src=4652774;type=ip1570;cat=brown0;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe>
</noscript>
<!-- End of DoubleClick Floodlight Tag: Please do not remove -->
<?php } ?>

<?php if ($title == 'Transfers'){ ?>
<!-- Tag for Activity Group: IP157158: Mercy College Spring, Activity Name: IP157158 BROWN Mercy Transfer Landing Page~IP157158, Activity ID: 2560246 -->
<!-- Expected URL: https://www.mercy.edu/admissions/transfers -->

<!--
Activity ID: 2560246
Activity Name: IP157158 BROWN Mercy Transfer Landing Page~IP157158
Activity Group Name: IP157158: Mercy College Spring
-->

<!--
Start of DoubleClick Floodlight Tag: Please do not remove
Activity name of this tag: IP157158 BROWN Mercy Transfer Landing Page~IP157158
URL of the webpage where the tag is expected to be placed: https://www.mercy.edu/admissions/transfers
This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
Creation Date: 07/14/2015
-->
<script type="text/javascript">
var axel = Math.random() + "";
var a = axel * 10000000000000;
document.write('<iframe src="https://4652774.fls.doubleclick.net/activityi;src=4652774;type=ip1570;cat=ip1570;ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
</script>
<noscript>
<iframe src="https://4652774.fls.doubleclick.net/activityi;src=4652774;type=ip1570;cat=ip1570;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe>
</noscript>
<!-- End of DoubleClick Floodlight Tag: Please do not remove -->
<?php } ?>

<!--Main Responsive menu, currently hardcoded in each template, replace everywhere if you change it here -->
<div class="alert-bar">
  <?php print render($page['alert_bar']); ?>
</div>

<div class="navbar-offcanvas off-toggle">
  <div class="offcanvas-search-container search_bar">
    <?php print render($page['search_bar']); ?>
  </div>
  <?php if (!empty($primary_nav) || !empty($secondary_nav) || !empty($page['navigation'])): ?>
    <div class="navbar-offcanvas-inner">
      <!-- Search icon and chat icons -->
      <div class="search-bar search search-bar-offcanvas">
        <?php
          $block = module_invoke('block', 'block_view', '3');
          print render($block['content']);
        ?>
      </div>
      <nav role="navigation">
        <div class="main_nav_container">
          <button class="navbar-toggle main-navigation-toggle main-navigation-close">
            <span class="glyphicon glyphicon-remove"> </span>
          </button>
          <?php if ($primary_links_mobile) : ?>
            <?php print $primary_links_mobile; ?>
          <?php endif; ?>
        </div>
        <div class="second_nav_container">
          <?php if (!empty($secondary_nav)): ?>
            <?php print render($secondary_nav); ?>
          <?php endif; ?>
          <?php if (!empty($primary_nav)): ?>
            <?php print render($primary_nav); ?>
          <?php endif; ?>
          <?php if (!empty($page['navigation'])): ?>
            <?php print render($page['navigation']); ?>
          <?php endif; ?>
        </div>
      </nav>
    </div><!-- /.navbar-offcanvas-inner -->
  <?php endif; ?>
</div>

<div class="info-offcanvas-wrapper">
  <div class="info-title">
    <h2>Get More Info</h2>
  </div>
  <div id="moveDivInfoTarget" class="info-offcanvas"></div>
</div>

<div id="whole-page-wrapper">
  <div class="search-container">
    <?php if ($page['search_bar']): ?>
      <div class="search_bar">
        <?php print render($page['search_bar']); ?>
      </div> <!-- /.search_bar -->
    <?php endif; ?>
  </div>
  <header id="navbar" role="banner" class="<?php print $navbar_classes; ?>">
    <div class="container">
      <div class="navbar-header">
        <?php if ($logo): ?>
          <a class="logo navbar-btn pull-left" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>">
            <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
          </a>
        <?php endif; ?>
        <?php if (!empty($site_name)): ?>
          <a class="name navbar-brand" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>"><?php print $site_name; ?></a>
        <?php endif; ?>
        <!-- .btn-navbar is used as the toggle for collapsed navbar content -->
        <button type="button" class="navbar-toggle main-navigation-toggle">
          <span class="sr-only nav-open">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
      </div><!-- /.navbar-header -->
      <!-- Search icon and chat icons -->
      <div class="search-bar search">
        <?php
          $block = module_invoke('block', 'block_view', '3');
          print render($block['content']);
        ?>
      </div>
      <button type="button" class="navbar-toggle request-info-toggle">Request Info</button>
      <?php if (!empty($primary_nav) || !empty($secondary_nav) || !empty($page['navigation'])): ?>
        <div class="navbar-collapse">
          <nav role="navigation">
            <?php if (!empty($primary_nav)): ?>
              <?php print render($primary_nav); ?>
            <?php endif; ?>
            <?php if (!empty($secondary_nav)): ?>
              <?php print render($secondary_nav); ?>
            <?php endif; ?>
            <?php if (!empty($page['navigation'])): ?>
              <?php print render($page['navigation']); ?>
            <?php endif; ?>
          </nav>
        </div>
      <?php endif;?>
    </div>
  </header>
  <div class="container">
    <?php if ($page['top-logo']): ?>
      <div class="top-logo">
        <?php print render($page['top-logo']); ?>
      </div> <!-- /.top-logo -->
    <?php endif; ?>
    <header role="banner" id="navigation-secondary">
      <?php if (!empty($site_slogan)): ?>
        <p class="lead"><?php print $site_slogan; ?></p>
      <?php endif; ?>
      <?php print render($page['header']); ?>
    </header>
  </div>
  <div id="slider-container">
    <?php if ($page['slider']): ?>
      <div class="slider">
        <?php print render($page['slider']); ?>
      </div> <!-- /.slider -->
    <?php endif; ?>
  </div>
  <div class="content-container">
    <div class="main-container container">
      <div class="row">
        <?php if (!empty($page['sidebar_first'])): ?>
          <aside class="col-sm-3" role="complementary">
            <?php print render($page['sidebar_first']); ?>
          </aside>
        <?php endif; ?>
        <section <?php print $content_column_class; ?>>
          <?php if (!empty($page['highlighted'])): ?>
            <div class="highlighted jumbotron">
              <?php print render($page['highlighted']); ?>
            </div>
          <?php endif; ?>
          <?php if (!empty($breadcrumb)): print $breadcrumb; endif; ?>
          <a id="main-content"></a>
          <?php print render($title_prefix); ?>
            <?php if (!empty($title)): ?>
             <!-- <h1 class="page-header"><?php //print $title; ?></h1> -->
            <?php endif; ?>
          <?php print render($title_suffix); ?>
          <?php print $messages; ?>
          <?php if (!empty($tabs)): ?>
            <?php print render($tabs); ?>
          <?php endif; ?>
          <?php if (!empty($page['help'])): ?>
            <?php print render($page['help']); ?>
          <?php endif; ?>
          <?php if (!empty($action_links)): ?>
            <ul class="action-links">
              <?php print render($action_links); ?>
            </ul>
          <?php endif; ?>
          <?php print render($page['content']); ?>
        </section>
        <?php if (!empty($page['sidebar_second'])): ?>
          <aside class="col-sm-3" role="complementary">
            <?php print render($page['sidebar_second']); ?>
          </aside>
        <?php endif; ?>
      </div>
    </div><!-- /.content-container -->
    <?php if (!empty($page['pre_footer'])): ?>
      <div class="pre-footer container-fluid">
        <?php print render($page['pre_footer']); ?>
      </div>
    <?php endif; ?>
  </div>
  <footer class="main-footer container">
    <?php print render($page['footer']); ?>
  </footer>
</div>
