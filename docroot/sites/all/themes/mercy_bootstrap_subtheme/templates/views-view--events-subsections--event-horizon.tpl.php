<div class="<?php print $classes; ?>">
  <?php if ($header): ?>
    <?php print $header; ?>
  <?php endif; ?>
  <?php if ($rows): ?>
    <?php print $rows; ?>
  <?php endif;?>
  <div class="customNavigation">
    <a class="btn prev">Previous</a>
    <a class="btn next">Next</a>
  </div>
</div><?php /* class view */ ?>