## Mercy Theme Sass Setup

### Installing NVM

1. Download and install [NVM](https://github.com/creationix/nvm). If you're using homebrew you can run `brew install nvm`.
2. Change directory into the `mercy_bootstrap_subtheme` theme folder and run `nvm install`. This will install the version of nodeJS specified in the theme's **.nvmrc** file.
3. From that same directory run `npm install`. This will install the node modules found in the themes **package.json**. These are needed to run Gulp.

### Compiling Sass with Gulp

From within the `mercy_bootstrap_subtheme` theme directory run:
`npm run gulp` to compile
`npm run gulp-watch` to watch the sass directory for changes
