/* Shows hides search bar
* Author: Jon Garcia jgarcia@mercy.edu
*/
// Unleashes the search bar when click on the search glyphicon on main menu bar
(function ($) {
  $(document).ready(function(){

    $(".search_bar").hide();
    $(".search-toggle-icon").show();
 
    $('.search-toggle-icon').click(function(){
      $(".navbar").hide();
      $(".search-bar-offcanvas").hide(); //search button
      $(".search_bar").show();
      $(".search_bar input").focus();
    });
    $('.search-bar-closer, .main-navigation-close').click(function(){
      $('.search_bar').hide();
      $(".search-bar-offcanvas").show(); 
      $(".navbar").show();
    });
    
  });

})(jQuery);