(function ($) {

	
	var menuSelectorsArray = ['#about-mercy-dropdown', '#admissions-and-aid-dropdown', 
						 '#academics-dropdown', '#student-affairs-dropdown', '#visit-dropdown', '#athletics-dropdown'];
	
	//Create a comma separated string of all menu items, to control them all with a single jQuery selector.
	var allMenusSelectorString = ""; 
	menuSelectorsArray.forEach(function(element, index, array) {
		allMenusSelectorString += element+",";
	});	
	var menuHasBeenEntered = false;
	var menuTimeout;

	var addHoverIntent = function (selectorVar, index) {
		
		//Use closures to pass context to HoverIntent.
		var slideThisMenuDown = function () {

			//For navigation links, slide down the appropriate menu
			if (selectorVar.indexOf("#navigation-secondary") >= 0){				
					$(allMenusSelectorString).stop(true, true).hide();
					$(menuSelectorsArray[index]).slideDown();				
			}
			//For menu dropdowns
			else {
				menuHasBeenEntered = true;
			}				

			
		};

		//If selectorVar is a navigation link, set timeout on slideup function and use otherwise sensible values
		if (selectorVar.indexOf("#navigation-secondary") >= 0){
				var interval = 50;
				var timeout = 50;
				var sensitivity = 12;
				var slideThisMenuUp = function() {
			
					menuTimeout = setTimeout(function(){
						//Only close the menu if user hasn't moused over it
						if (menuHasBeenEntered === false){
							$(menuSelectorsArray[index]).slideUp();
							menuTimeout=null; //make sure to clear the timeout

						}
					}, 100);};
		}
		
		//If selectorVar is a dropdown, use normal slideup function & custom options
		else {
				var sensitivity = 1200 ; //So moving the mouse quickly across the menu doesn't inadvertantly close it
				var interval = 100;
				var timeout = 0; //So the menu will still close even if the user leaves the dropdown right away

				var slideThisMenuUp = function () {		

					$(menuSelectorsArray[index]).slideUp();	
					menuHasBeenEntered = false;
					menuTimeout = null;
				};	
		}	

		$(selectorVar).hoverIntent({
			over: slideThisMenuDown,
			out: slideThisMenuUp,
			timeout: timeout,
			interval: interval,
			sensitivity: sensitivity
			
			});
	}

	$(document).ready(function(){
		
		menuSelectorsArray.forEach(function(element, index, array){
			//Call hoverIntent on each selector in menuSelectorsArray.
			addHoverIntent(element, index);
			//Call hoverintent on #navigation-secondary li:nth-child(index) for each menu item.
			addHoverIntent("#navigation-secondary li:nth-child("+(index+1)+"), ",index);					
		});
	});
})(jQuery);

