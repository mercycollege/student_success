// Add scrollable widget for events.
(function ($) {

    Drupal.behaviors.mercyEventSlider = {
      attach: function (context, settings) {
       
        var owl = $(".events-ticker");
        owl.owlCarousel({
          items : 5, //5 items above 1000px browser width
          pagination: false,
          autoplay: true,
          stopOnHover: true,
          addClassActive: true,
          afterAction: setFirstLast
        });
        
        $(".customNavigation .btn.next").click(function(){
          owl.trigger('owl.next');
        })
        $(".customNavigation .btn.prev").click(function(){
          owl.trigger('owl.prev');
        })
        
        
        function setFirstLast() {
          $('.owl-item.active').removeClass('first-visible');
          $('.owl-item.active').removeClass('last-visible');
          $('.owl-item.active').first().addClass('first-visible');
          $('.owl-item.active').last().addClass('last-visible');
        }
        
      }
    };

})(jQuery);