var hsname;
var hsadd1;
var hsadd2;
var hscity;
var hsstate;
var hszip;
var app;
var schooltype;
var hscode;
var pop;
function GetValues(hscode,schooltype,app,hsname,hsadd1,hsadd2,hscity,hsstate,hszip,pop) {
	if (app=='U' && schooltype=='H') {
		if (pop=='1') {
			self.opener.document.getElementsByName('submitted[educational_background_fieldset][highschool2attended][HS1ceebcode]')[0].value = hscode;
			self.opener.document.getElementsByName('submitted[educational_background_fieldset][highschool2attended][HS1Name]')[0].value = hsname;
			self.opener.document.getElementsByName('submitted[educational_background_fieldset][highschool2attended][HS1StAddr]')[0].value = hsadd1;
			self.opener.document.getElementsByName('submitted[educational_background_fieldset][highschool2attended][HS1Staddr1]')[0].value = hsadd2;
			self.opener.document.getElementsByName('submitted[educational_background_fieldset][highschool2attended][HS1City]')[0].value = hscity;
			self.opener.document.getElementsByName('submitted[educational_background_fieldset][highschool2attended][HS1State]')[0].value = hsstate;
			self.opener.document.getElementsByName('submitted[educational_background_fieldset][highschool2attended][HS1Zip]')[0].value = hszip;
		}
		if (pop=='2') {
			self.opener.document.getElementsByName('submitted[educational_background_fieldset][highschool2attended][HS2ceebcode]')[0].value = hscode;
			self.opener.document.getElementsByName('submitted[educational_background_fieldset][highschool2attended][HS2Name]')[0].value = hsname;
			self.opener.document.getElementsByName('submitted[educational_background_fieldset][highschool2attended][HS2StAddr]')[0].value = hsadd1;
			self.opener.document.getElementsByName('submitted[educational_background_fieldset][highschool2attended][HS2Staddr1]')[0].value = hsadd2;
			self.opener.document.getElementsByName('submitted[educational_background_fieldset][highschool2attended][HS2City]')[0].value = hscity;
			self.opener.document.getElementsByName('submitted[educational_background_fieldset][highschool2attended][HS2State]')[0].value = hsstate;
			self.opener.document.getElementsByName('submitted[educational_background_fieldset][highschool2attended][HS2Zip]')[0].value = hszip;
		}
	}
	if (app=='U' && schooltype=='C') {
		if (pop=='3') {
			self.opener.document.getElementsByName('submitted[Educational_Background_continued][colleges_attended][College1ceebcode]')[0].value = hscode;
			self.opener.document.getElementsByName('submitted[Educational_Background_continued][colleges_attended][College1Name]')[0].value = hsname;
			self.opener.document.getElementsByName('submitted[Educational_Background_continued][colleges_attended][College1StAddr]')[0].value = hsadd1;
			self.opener.document.getElementsByName('submitted[Educational_Background_continued][colleges_attended][College1Staddr1]')[0].value = hsadd2;
			self.opener.document.getElementsByName('submitted[Educational_Background_continued][colleges_attended][College1City]')[0].value = hscity;
			self.opener.document.getElementsByName('submitted[Educational_Background_continued][colleges_attended][College1State]')[0].value = hsstate;
			self.opener.document.getElementsByName('submitted[Educational_Background_continued][colleges_attended][College1Zip]')[0].value = hszip;
		}
		if (pop=='4') {
			self.opener.document.getElementsByName('submitted[Educational_Background_continued][colleges_attended][College2ceebcode]')[0].value = hscode;
			self.opener.document.getElementsByName('submitted[Educational_Background_continued][colleges_attended][College2Name]')[0].value = hsname;
			self.opener.document.getElementsByName('submitted[Educational_Background_continued][colleges_attended][College2StAddr]')[0].value = hsadd1;
			self.opener.document.getElementsByName('submitted[Educational_Background_continued][colleges_attended][College2Staddr1]')[0].value = hsadd2;
			self.opener.document.getElementsByName('submitted[Educational_Background_continued][colleges_attended][College2City]')[0].value = hscity;
			self.opener.document.getElementsByName('submitted[Educational_Background_continued][colleges_attended][College2State]')[0].value = hsstate;
			self.opener.document.getElementsByName('submitted[Educational_Background_continued][colleges_attended][College2Zip]')[0].value = hszip;
		}
	}
	if (app=='G') {
		/*if (self.opener.document.getElementsByName("HS1Name")[0].value == "") {
			self.opener.document.getElementsByName("HS1ceebcode")[0].value = hscode;
			self.opener.document.getElementsByName("HS1Name")[0].value = hsname;
			self.opener.document.getElementsByName("HS1StAddr")[0].value = hsadd1;
			self.opener.document.getElementsByName("HS1Staddr1")[0].value = hsadd2;
			self.opener.document.getElementsByName("HS1City")[0].value = hscity;
			self.opener.document.getElementsByName("HS1State")[0].value = hsstate;
			self.opener.document.getElementsByName("HS1Zip")[0].value = hszip;
		}
		else if (self.opener.document.getElementsByName("HS2Name")[0].value == "") {
			self.opener.document.getElementsByName("HS2ceebcode")[0].value = hscode;
			self.opener.document.getElementsByName("HS2Name")[0].value = hsname;
			self.opener.document.getElementsByName("HS2StAddr")[0].value = hsadd1;
			self.opener.document.getElementsByName("HS2Staddr1")[0].value = hsadd2;
			self.opener.document.getElementsByName("HS2City")[0].value = hscity;
			self.opener.document.getElementsByName("HS2State")[0].value = hsstate;
			self.opener.document.getElementsByName("HS2Zip")[0].value = hszip;
		}*/
		if (pop=='1') {
			self.opener.document.getElementsByName("submitted[educational_background][College1ceebcode]")[0].value = hscode;
			self.opener.document.getElementsByName("submitted[educational_background][College1Name]")[0].value = hsname;
			self.opener.document.getElementsByName("submitted[educational_background][College1StAddr]")[0].value = hsadd1;
			self.opener.document.getElementsByName("submitted[educational_background][College1Staddr1]")[0].value = hsadd2;
			self.opener.document.getElementsByName("submitted[educational_background][College1City]")[0].value = hscity;
			self.opener.document.getElementsByName("submitted[educational_background][College1State]")[0].value = hsstate;
			self.opener.document.getElementsByName("submitted[educational_background][College1Zip]")[0].value = hszip;
		}
		if (pop=='2') {
			self.opener.document.getElementsByName("submitted[educational_background][College2ceebcode]")[0].value = hscode;
			self.opener.document.getElementsByName("submitted[educational_background][College2Name]")[0].value = hsname;
			self.opener.document.getElementsByName("submitted[educational_background][College2StAddr]")[0].value = hsadd1;
			self.opener.document.getElementsByName("submitted[educational_background][College2Staddr1]")[0].value = hsadd2;
			self.opener.document.getElementsByName("submitted[educational_background][College2City]")[0].value = hscity;
			self.opener.document.getElementsByName("submitted[educational_background][College2State]")[0].value = hsstate;
			self.opener.document.getElementsByName("submitted[educational_background][College2Zip]")[0].value = hszip;
		}
		if (pop=='3') {
			self.opener.document.getElementsByName("submitted[graduate_school_information][HS1ceebcode]")[0].value = hscode;
			self.opener.document.getElementsByName("submitted[graduate_school_information][HS1Name]")[0].value = hsname;
			self.opener.document.getElementsByName("submitted[graduate_school_information][HS1StAddr]")[0].value = hsadd1;
			self.opener.document.getElementsByName("submitted[graduate_school_information][HS1Staddr1]")[0].value = hsadd2;
			self.opener.document.getElementsByName("submitted[graduate_school_information][HS1City]")[0].value = hscity;
			self.opener.document.getElementsByName("submitted[graduate_school_information][HS1State]")[0].value = hsstate;
			self.opener.document.getElementsByName("submitted[graduate_school_information][HS1Zip]")[0].value = hszip;
		}

	}
	if (app=='NM') {
		if (self.opener.document.getElementById("College1").value == "") {
			self.opener.document.getElementById("College1ceebcode").value = hscode;
			self.opener.document.getElementById("College1").value = hsname;
		}
		else if (self.opener.document.getElementById("College2").value == "") {
			self.opener.document.getElementById("College2ceebcode").value = hscode;
			self.opener.document.getElementById("College2").value = hsname;
			}
		else if (self.opener.document.getElementById("College3").value == "") {
			self.opener.document.getElementById("College3ceebcode").value = hscode;
			self.opener.document.getElementById("College3").value = hsname;
		}
		else if (self.opener.document.getElementById("College4").value == "") {
			self.opener.document.getElementById("College4ceebcode").value = hscode;
			self.opener.document.getElementById("College4").value = hsname;
		}
		else {
			self.opener.document.getElementById("College5ceebcode").value = hscode;
			self.opener.document.getElementById("College5").value = hsname;
		}
	}
	window.close();
	return false;
}