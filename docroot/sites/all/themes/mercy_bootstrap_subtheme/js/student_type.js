/* Sets student type based on radio selection
* Author: Jon Garcia jgarcia@mercy.edu
*/
(function ($) {
	$(document).ready(function(){
  		$('body').on('click','.form-inline-block-radios',function(){
		  	var radio_button = "#edit-submitted-are-you-transferring-credits-from-another-college-1";
		  	var radio_button2 = "#edit-submitted-are-you-transferring-credits-from-another-college-2";
		  	$(radio_button).click(function(){
		  		$("input[name='submitted[student_type]']").val('Transfer');
		 	});
		  	$(radio_button2).click(function(){
		  		$("input[name='submitted[student_type]']").val('Freshman');
			});
		});
	});
})(jQuery);
