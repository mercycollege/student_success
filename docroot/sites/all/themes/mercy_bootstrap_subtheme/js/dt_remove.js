// Removes "data-toggle" attribute from link to make them clickable when expanded
(function ($) {
	$(document).ready(function () {
	  DropdownToggle();
	});
	function DropdownToggle() {
		$('.dropdown-toggle').removeAttr("data-toggle");
	}

})(jQuery);