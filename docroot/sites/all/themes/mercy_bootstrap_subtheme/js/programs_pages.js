/* Program pages side bar, same page menu.
* Author: Jon Garcia jgarcia@mercy.edu
*/
(function ($) {
	$(document).ready(function(){

	$("#programs-pages #show-overview").click(function(){		
		$('#programs-pages .top-middle, #programs-pages .top-right, .overview-tab').show();
		$('.how-to-apply-group, .courses-group, .faculty-group, .faqs-group, .program-info-tab, .program-outcomes, .learning-objectives').hide();
		$("#show-overview").addClass('active');
		$("#show-how-to-apply, #show-curriculum, #show-faqs, #show-program-info, #show-faculty, #show-program-outcomes").removeClass('active');
	});

    $("#programs-pages #show-program-outcomes").click(function(){
        $('.program-outcomes, .learning-objectives').show();
        $('.courses-group, .how-to-apply-group, #programs-pages .top-middle, #programs-pages .top-right, .faculty-group, .faqs-group, .program-info-group,').hide();
        $("#show-program-outcomes").addClass('active');
        $("#show-program-info, #show-how-to-apply, #show-curriculum, #show-overview, #show-faqs, #show-faculty").removeClass('active');
    });

	$("#programs-pages #show-curriculum").click(function(){
		$('.courses-group').show();
		$('.how-to-apply-group, #programs-pages .top-middle, #programs-pages .top-right, .faculty-group, .faqs-group, .program-info-group, .program-outcomes, .learning-objectives').hide();
		$("#show-curriculum").addClass('active');
		$("#show-how-to-apply, #show-overview, #show-program-outcomes, #show-faqs, #show-program-info, #show-faculty").removeClass('active');
	});

	$("#programs-pages #show-faqs").click(function(){		
		$('.faqs-group').show();
		$('.how-to-apply-group, .courses-group, #programs-pages .top-middle, .faculty-group, #programs-pages .top-right, .program-info-group, .program-outcomes, .learning-objectives').hide();
		$("#show-faqs").addClass('active');
		$("#show-how-to-apply, #show-overview, #show-program-outcomes, #show-curriculum, #show-program-info, #show-faculty, #show-learning-objectives").removeClass('active');
	});

	$("#programs-pages #show-program-info").click(function(){		
		$('#programs-pages .top-middle, #programs-pages .top-right, .program-info-tab').show();
		$('.how-to-apply-group, .courses-group, .faqs-group, .overview-tab, .faculty-group, .program-outcomes, .learning-objectives').hide();
		$("#show-program-info").addClass('active');
		$("#show-how-to-apply, #show-curriculum, #show-overview, #show-program-outcomes, #show-faqs, #show-faculty, #show-learning-objectives").removeClass('active');
	});

	$("#programs-pages #show-faculty").click(function(){		
		$('.faculty-group').show();
		$('.how-to-apply-group, .courses-group, #programs-pages .top-middle, #programs-pages .top-right, .faqs-group, .program-info-group, .program-outcomes, .learning-objectives').hide();
		$("#show-faculty").addClass('active');
		$("#show-program-outcomes, #show-overview, #show-curriculum, #show-faqs, #show-program-info, #show-learning-objectives").removeClass('active');
	});

	$("#programs-pages #show-how-to-apply").click(function(){
		$('.how-to-apply-group').show();
		$('.faculty-group, .courses-group, #programs-pages .top-middle, #programs-pages .top-right, .faqs-group, .program-info-group, .program-outcomes, .learning-objectives').hide();
		$("#show-how-to-apply").addClass('active');
		$("#show-faculty, #show-overview, #show-program-outcomes, #show-curriculum, #show-faqs, #show-program-info, #show-learning-objectives").removeClass('active');
	});
});

})(jQuery);