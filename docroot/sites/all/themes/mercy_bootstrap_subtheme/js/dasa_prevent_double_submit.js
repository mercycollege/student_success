/**
 * @file
 * Prevent multiple submissions on the DASA form.
 */

(function ($) {

  "use strict";

  Drupal.behaviors.dasaForm = {
    attach: function (context, settings) {
      var $formSelector = $("#webform-client-form-17671", context);

      $formSelector.once('hideSubmit', function () {
        var $form = $(this);

        $form.find('.form-submit').on('click', function (e) {
          var $el = $(this);
          $el.after('<input type="hidden" name="' + $el.attr('name') + '" value="' + $el.attr('value') + '" />');
          return true;
        });

        $form.on('submit',function (e) {
          if (!e.isPropagationStopped()) {
            var $el = $('.form-submit', $(this));
            $el.attr('disabled', 'disabled');
            $el.append('<div class="ajax-progress ajax-progress-throbber"><i class="glyphicon glyphicon-refresh glyphicon-spin"></i></div>');
            return true;
          }
        });
      });

      // Keep chain-ability.
      return this;
    }
  };
}(jQuery));
