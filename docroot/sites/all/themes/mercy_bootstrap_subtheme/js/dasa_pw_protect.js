/*
* Adds front end password protection to the dasa workshop registration form. 
* Author: Jon Garcia jgarcia@mercy.edu
*/

(function ($) {
	//crete new form element for password field.
	labelElm = $("<label>").text('Password required for Sara Schenirer sessions. Please enter it below.');
	parentDiv = $('<div class="form-item webform-component webform-component-textfield"></div>');
	newElem = $("<input type='password' value='' />");
	newElem.attr("name", "dasaPW");
    newElem.attr("id", "pwConfirm");
    newElem.attr("name", "dasaPW");
    newElem.attr("placeholder", "Sara Schenirer Password");
    newElem.attr("class", "webform webform-hints-field form-control form-alone");

	var checkPW = function() { //checks for password match while user is typing it.
	    (newElem).keyup(function() {
	    	$("#edit-webform-ajax-submit-17671").prop("disabled", true);
	    	newElemVal = newElem.val()
	    	if (newElemVal === "DASA1836") {
	    		newElem.removeClass("error");
	    		parentDiv.addClass("has-success");
	    		$("#edit-webform-ajax-submit-17671").removeAttr("disabled");
	    	}
	    	else {
	    		newElem.addClass("error");
	    	}
		});
	}
	var addElement = function() { //add element to form upon selection of a sara workshop.
		$("#edit-webform-ajax-submit-17671" ).prop("disabled", true);
		$("[id^=edit-submitted-section-b]").after(parentDiv);
	    $(parentDiv).append(labelElm); 
	    $(labelElm).after(newElem); 
      	$(parentDiv).addClass("has-warning");
   		setTimeout(function() {
        $(parentDiv).removeClass('has-warning');
		}, 500)

	}
	$(document).ready(function () {
		$('body').on("click", ".webform-client-form-17671", function() { //binds elements recreated after ajax call to script
			$( document ).ajaxComplete(function(event, xhr, settings) { //this prevents the element being added  to the form on every click. useful when backend validation kicks in.
				var confir = xhr.responseText;
				if (confir.indexOf("alert-danger") >! 0) {
					newElem.attr('value', '');
			      	$(parentDiv).removeClass("has-success");
					if (typeof instanceVar != 'undefined') {
						addElement();
		    			checkPW();
					}
				}
			});
			$("[id^=edit-submitted-section]").change(function() {
				sarah = $("[id^=edit-submitted-section-a]").val();
				sarah2 = $("[id^=edit-submitted-section-b]").val();
				newElem.attr('value', '');
				newElem.remove();
				labelElm.remove();
				$("#edit-webform-ajax-submit-17671").removeAttr("disabled");
				if ((sarah.indexOf('Sara_Schenirer') > 0) || (sarah2.indexOf('Sara_Schenirer') > 0)) { //Finds Sarah in selected option values.
					instanceVar = true; //This var is created when sara is selected, and deleted when she's unselected. Helpful to be able to set up the element after ajax call based on var status
					addElement();
				    checkPW();
				}
				else {
					delete instanceVar;
				}
			});
		});
	});
})(jQuery);
