/*
* Author: Jon Garcia jgarcia@mercy.edu
*/
// This is a simple fading effect image rotator that applies only to the slideshow panel block content type (Fieldable panels).
(function ($) {
	$(document).ready(function(){
		var divs = $( "div.fancy-slide-show" ).children( ".field-image, .field-type-image" ).hide(),
		    i = 0;
			(function cycle() {
			    divs.eq(i).fadeIn(400)
			              .delay(5000)
			              .fadeOut(400, cycle);

			    i = ++i % divs.length;
			})
		();
	});

})(jQuery);