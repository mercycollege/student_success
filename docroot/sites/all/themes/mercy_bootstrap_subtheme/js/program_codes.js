/**
 * @file
 * Gets program codes from json objects and inserts them into form.
 *
 * @author
 * Jon Garcia jgarcia@mercy.edu
*/
(function($) {
  $(document).ready(function() {
    // Form is submitted via ajax call, if form validates kicks in before
    // submitting, form elements will be reloaded with new elements.
    // The following line takes care of this by attaching the event to the
    // selected elements.
    $('body').on('click', '.webform-client-form-18135', function() {
      // Selects element starting with text after = sign & reads every change.
      $("[id^=edit-submitted-program-of-interest]").change(function() {
        var json_obj = $("[id^=edit-submitted-program-of-interest]").val();
        if (json_obj != "") {
          $.getJSON("/sites/default/files/json/program/" + json_obj + ".json", function(data) {
            // This prevents the element being added to the form on every click.
            // Useful when backend validation kicks in.
            $(document).ajaxStop(function () {
              var $eventCampus = $("[id^=edit-submitted-campus]");
              if ($eventCampus.length) {
                var items = [];
                var select = $eventCampus;
                var options = select.prop('options');
                $('option', select).remove();
                options[options.length] = new Option('- Campus (required) -', '', true, true);

                $.each(data['campuses'], function(key, value) {
                  items.push(key + value);
                  var safe_key = value.replace(/ /g, "_");
                  options[options.length] = new Option(value, safe_key);
                });
                $("input[name='submitted[program_code]']").val(data['code']);
              }
            });
          });
        }
      });
    });
  });
})(jQuery);
