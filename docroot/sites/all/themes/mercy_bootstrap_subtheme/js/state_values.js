/* There're exist two fields to state, an international and and national. This script Adds state selection to another hidden field to be used as definitive state value.
* Author: Jon Garcia jgarcia@mercy.edu
*/
(function ($) {
  $(document).ready(function(){
  	$(this).change(function() {
		var state2 = $("#edit-submitted-personal-information-fieldset-preferred-mailing-address-other-state").val();
		var int_state2 = $("#edit-submitted-personal-information-fieldset-preferred-mailing-address-other-foreignstate").val();
		var state3 = $("#edit-submitted-family-information-fieldset-parent1-parental-1-state").val();
		var int_state3 = $("#edit-submitted-family-information-fieldset-parent1-other-foreignstateparent1").val();
		var state4 = $("#edit-submitted-family-information-fieldset-parent2-parental-2-state").val();
		var int_state4 = $("#edit-submitted-family-information-fieldset-parent2-other-foreignstateparent2").val();
		var g_state2 = $("#edit-submitted-preferred-mailing-other-state").val();
		var g_int_state2 = $("#edit-submitted-preferred-mailing-other-stateprovince").val();

		if ($("#edit-submitted-personal-information-fieldset-preferred-mailing-address-other-state").is(":disabled")) {
			$("input[name='submitted[personal_information_fieldset][preferred_mailing_address][state_value2]']").val(int_state2);
		} else {
			$("input[name='submitted[personal_information_fieldset][preferred_mailing_address][state_value2]']").val(state2);
		}

		if ($("#edit-submitted-family-information-fieldset-parent1-parental-1-state").is(":disabled")) {
			$("input[name='submitted[family_information_fieldset][parent1][state_value3]']").val(int_state3);
		} else {
			$("input[name='submitted[family_information_fieldset][parent1][state_value3]']").val(state3);
		}


		if ($("#edit-submitted-family-information-fieldset-parent2-parental-2-state").is(":disabled")) {
			$("input[name='submitted[family_information_fieldset][parent2][state_value4]']").val(int_state4);
		} else {
			$("input[name='submitted[family_information_fieldset][parent2][state_value4]']").val(state4);
		}

		if ($("#edit-submitted-preferred-mailing-other-state").is(":disabled")) {
			$("input[name='submitted[preferred_mailing][state_value2]']").val(g_int_state2);
		} else {
			$("input[name='submitted[preferred_mailing][state_value2]']").val(g_state2);
		}
	})

  });
})(jQuery);