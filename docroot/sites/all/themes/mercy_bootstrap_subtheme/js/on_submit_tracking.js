/* Executes tracking js after form submission.
* Author: Jon Garcia jgarcia@mercy.edu
* note for myself, calendar is the only one that will be undefined when not in that js is not in that node type, the rest will be an empty string.
*/
(function ($) {
	$(document).ready (function() {
		var program = $(".node-type-programs .title-box-purple ul li").text();
		var calendar = $(".node-type-calendar .pane-node-title").text().trim();
		var cal_date = $(".node-type-calendar .field-date-of-event .date-display-single").attr('content');
		var landing = $(".node-type-landing-page .page-header").text();

		if (program != '') {
			u1 = "program_" + program.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-');
		}
		else if ((calendar != 'undefined') && (landing == '')) {
				date = cal_date.trim();
				u1 = "rsvp_" + calendar.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-') + "-" + date;
		}
		else if (landing != '') {
			u1 = "landing_" + landing;
		}
   		$( document ).ajaxComplete(function(event, xhr, settings) {
			$(document).ajaxStop(function() {
       			var confir = xhr.responseText;
	   			if (confir.indexOf("form-confirmation") > 0) {
	   				iFrameWrite( confir );
					ga('send', 'event', 'RFI_RSVP', 'Form Submission', u1);
					facebookTrack();
					window._fbq = window._fbq || [];
					window._fbq.push(['track', '6027153649284', {'value':'0.00','currency':'USD'}]);
	   			}
   			});
   		});
	});
	var iFrameWrite = function (confir) {
		//console.log(u1);
		//Activity ID: 2127014
		//Creation Date: 03/12/2015
		var axel = Math.random() + "";
		var a = axel * 10000000000000;
		iframe = '<iframe src="https://4652774.fls.doubleclick.net/activityi;src=4652774;type=ip1570;cat=mercy0;u1=' + u1 + ';ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>';
		$('body').prepend(iframe);
	}

		var facebookTrack =  function() {
			(function() {
				var _fbq = window._fbq || (window._fbq = []);
				if (!_fbq.loaded) {
					var fbds = document.createElement('script');
					fbds.async = true;
					fbds.src = '//connect.facebook.net/en_US/fbds.js';
					var s = document.getElementsByTagName('script')[0];
					s.parentNode.insertBefore(fbds, s);
					_fbq.loaded = true;
				}
			})();
		}
})(jQuery);