/**
 * @file
 * Gets proper crm codes from json objects and inserts them into form.
 *
 * @author
 * Jon Garcia jgarcia@mercy.edu
 */
(function($) {
  $(document).ready(function() {
    // Form is submitted via ajax call, if form validates kicks in before
    // submitting, form elements will be reloaded with new elements.
    // The following line takes care of this by attaching the event to the
    // selected elements.
    $('body').on('click', '.webform-client-form-18135', function() {
      // Selects element starting with text after = sign & reads every change.
      $("[id^=edit-submitted-come-visit-us]").change(function() {
        $("input[name='submitted[event_taslima_crm_code]']").val("");
        var json_obj = $("[id^=edit-submitted-come-visit-us]").val();
        if ((json_obj === "I_already_visited_campus") || (json_obj === "I_am_not_interested_in_visiting_at_this_time") || (json_obj === "I_will_visit_at_a_later_date") || (json_obj === "")) {
          return;
        }
        $.getJSON("/sites/default/files/json/calendar/" + json_obj + ".json", function(data) {
          // This prevents the element being added to the form on every click.
          // Useful when backend validation kicks in.
          $(document).ajaxStop(function() {
            var $eventCampus = $("[id^=edit-submitted-event-campus]");
            if ($eventCampus.length) {
              var items = [];
              var select = $eventCampus;
              var options = select.prop('options');
              $('option', select).remove();
              options[options.length] = new Option('- Event Campus (required) -', '', true, true);
              var selectedOption = '';

              $.each(data, function(key, crm) {
                items.push(key + crm);
                var value = key.replace(/_/g, " ");
                options[options.length] = new Option(value, key);
              });
              select.val(selectedOption);
              select.change(function() {
                var jsonIndex = select.val();
                $("input[name='submitted[event_taslima_crm_code]']").val(data[jsonIndex]);
              });
            }
          });
        });
      });
    });
  });
})(jQuery);
