<?php

/*
 * Class Definition of Facebook Feeds Fetcher
 */
class FeedsFacebookFetcher extends FeedsHTTPFetcher{
    
  /*
   * Overrides sourceForm for FeedsHTTPFetcher
   */
  public function sourceForm($source_config) {
      $form = array();
      if(isset($source_config['source'])){
          $source = explode('?',$source_config['source']);
          $source_config['source'] = array($source[0]);
      }
      $form['source'] = array(
          '#type' => 'textfield',
          '#title' => t('URL'),
          '#description' => t('Enter a feed URL.'),
          '#default_value' => isset($source_config['source']) ? $source_config['source'] : '',
          '#maxlength' => NULL,
          '#required' => TRUE,
      );    
      $form['application_id'] = array(
          '#type' => 'textfield',
          '#title' => 'Application ID',
          '#description' => 'Provide Application ID',
          '#default_value' => isset($source_config['application_id']) ? $source_config['application_id'] : '',
          );
      
      $form['application_secret'] = array(
          '#type' => 'textfield',
          '#title' => 'Application Secret',
          '#description' => 'Provide Application secret',
          '#default_value' => isset($source_config['application_secret']) ? $source_config['application_secret'] : '',
      );
      return $form;
  }
  
  /*
   * Additional Validation of sourceFormValidate
   */
  public function sourceFormValidate(&$values) {
      $values['application_id'] = trim($values['application_id']);
      $values['application_secret'] = trim($values['application_secret']);
      $values['source'] = trim($values['source']);
      $access_token = _feeds_facebook_access_token($values['application_id'], $values['application_secret']);
      if($access_token){
          $values['source'] = $values['source'].'?access_token='.$access_token . '&limit=10';
      }
      else{
          form_set_error('Application credentials are not valid.');
      }
      parent::sourceFormValidate($values);
  }

}
