<?php

/**
 * @file
 *   Contains Entity API hooks.
 */

/**
 * Implements hook_entity_info().
 *
 * Defines the tables of this module's data model to Drupal core and Entity API
 * (from contrib):
 * - instagram_social_feeds is defined as entity.
 * - instagram_social_feed_photos is defined as an entity.
 *
 * Both Drupal core and Entity API keys and values are defined here.
 */
function instagram_social_feed_entity_info() {
  $return = array(
    'instagram_social_feeds' => array(
      'module' => 'instagram_social_feed',
      'label' => t('Instagram feed'),
      'plural label' => t('Instagram feeds'),
      'description' => t('An entity type used to store the definitions of an Instagram feed.'),
      'base table' => 'instagram_social_feeds',
      'revision table' => null,
      'entity keys' => array(
        'id' => 'id',
      ),
      'fieldable' => FALSE,
      'view modes' => array(
        'full' => array(
          'label' => t('Full content'),
          'custom settings' => FALSE,
        ),
      ),
      'controller class' => 'EntityAPIController',
      'entity class' => 'Entity',
      'label callback' => 'entity_class_label',
      'uri callback' => 'entity_class_uri',
      //'views controller class' => '',
      //'rules controller class' => '',
    ),
    'instagram_social_feed_photos' => array(
      'module' => 'instagram_social_feed',
      'label' => t('Instagram photo'),
      'plural label' => t('Instagram photos'),
      'description' => t('An entity type used to store Instagram photos.'),
      'base table' => 'instagram_social_feed_photos',
      'revision table' => null,
      'entity keys' => array(
        'id' => 'id',
      ),
      'fieldable' => FALSE,
      'view modes' => array(
        'full' => array(
          'label' => t('Full content'),
          'custom settings' => FALSE,
        ),
      ),
      'controller class' => 'EntityAPIController',
      'entity class' => 'Entity',
      'label callback' => 'entity_class_label',
      'uri callback' => 'entity_class_uri',
      //'views controller class' => '',
      //'rules controller class' => '',
    ),
  );
  return $return;
}

/**
 * Implements hook_views_data().
 */
function instagram_social_feed_views_data() {
  $data = array();

  $data['instagram_social_feed_photos'] = array(
    'table' => array(
      'group' => 'Content',
    ),
    'created_time' => array(
      'title' => t('Created time'),
      'help' =>  t('@entity "@field" field.', array('@entity' => t('Instagram photo'), '@field' => t('Created time'))),
      'field' => array(
        'handler' => 'views_handler_field_date',
      ),
      'sort' => array(
        'handler' => 'views_handler_sort_date',
      ),
    ),
    'thumbnail' => array(
      'title' => t('Thumbnail'),
      'help' =>  t('@entity "@field" field.', array('@entity' => t('Instagram photo'), '@field' => t('Thumbnail'))),
      'field' => array(
        'handler' => 'views_handler_field_url',
      ),
    ),
    'low_resolution' => array(
      'title' => t('Low resolution'),
      'help' =>  t('@entity "@field" field.', array('@entity' => t('Instagram photo'), '@field' => t('Low resolution'))),
      'field' => array(
        'handler' => 'views_handler_field_url',
      ),
    ),
    'standard_resolution' => array(
      'title' => t('Standard resolution'),
      'help' =>  t('@entity "@field" field.', array('@entity' => t('Instagram photo'), '@field' => t('Standard resolution'))),
      'field' => array(
        'handler' => 'views_handler_field_url',
      ),
    ),
    'caption' => array(
      'title' => t('Caption'),
      'help' =>  t('@entity "@field" field.', array('@entity' => t('Instagram photo'), '@field' => t('Caption'))),
      'field' => array(
        'handler' => 'views_handler_field',
      ),
    ),
    'instagram_user' => array(
      'title' => t('Instagram user'),
      'help' =>  t('@entity "@field" field.', array('@entity' => t('Instagram photo'), '@field' => t('Instagram user'))),
      'field' => array(
        'handler' => 'views_handler_field',
      ),
    ),
    'approve' => array(
      'title' => t('Approve'),
      'help' =>  t('@entity "@filter filter.', array('@entity' => t('Instagram photo'), '@filter' => t('Approve'))),
      'field' => array(
        'handler' => 'views_handler_field_numeric',
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_numeric',
      ),
    ),
    'instagram_link' => array(
      'title' => t('Instagram link'),
      'help' =>  t('@entity "@filter filter.', array('@entity' => t('Instagram photo'), '@filter' => t('Instagram link'))),
      'field' => array(
        'handler' => 'views_handler_field',
      ),
    ),
  );

  return $data;
}
