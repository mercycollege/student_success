/**
 * @file
 * Check if video has been deleted using YouTube Iframe API.
 *
 * This code is outside of a Drupal behavior so its callback
 * functions can be found by the YouTube API.
*/

// Load the IFrame Player API code asynchronously.
var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

// This function creates an <iframe> (and YouTube player)
// after the API code downloads.
var player;

function onYouTubeIframeAPIReady() {

  if (typeof Drupal.settings.mercy_system !== 'undefined') {
    if (typeof Drupal.settings.mercy_system.youtubeVideoID !== 'undefined') {
      player = new YT.Player('block-system-main', {
        width: '640',
        height: '480',
        videoId: Drupal.settings.mercy_system.youtubeVideoID,
        playerVars: { 'autoplay': 1, 'controls': 0 },
        events: {
          'onReady': onPlayerReady,
          'onStateChange': onPlayerStateChange
        }
      });
    }
  }
}

// The API will call this function when the video player is ready.
function onPlayerReady(event) {
  event.target.playVideo();
  player.mute();
}

// The API calls this function when the player's state changes.
// The function indicates that when playing a video (state=1),
// the player should play for six seconds and then stop.
var didPlay = false;
function onPlayerStateChange(event) {
  if (event.data == YT.PlayerState.UNSTARTED) {
    // Send AJAX request to signal this video has been deleted.
    if (didPlay === false) {
      if (typeof Drupal.settings.mercy_system !== 'undefined') {
        if (typeof Drupal.settings.mercy_system.youtubeNID !== 'undefined') {
          jQuery.get("/mercy_system/ajax/youtube_deleted/" + Drupal.settings.mercy_system.youtubeNID);
        }
      }
    }
  }
  if (event.data == YT.PlayerState.PLAYING) {
    didPlay = true;
  }
}
