/**
 * @file
 * This file will retrieve the utm info saved as cookie via PHP and add it as a
 * value to the utm field of rsvp and RFI forms  * The value is also being
 * loaded via php, but because php obtain the value after the page is rendered,
 * it cannot attach it to the form until the next refresh or after navigating
 * to other pages.
 *
 * @author Jon Garcia jgarcia@mercy.edu
 */
(function($) {
  $(document).ready(function () {
    var json = JSON.parse($.cookie("utm_info"));
    if (json != null) {
      var to_array = $.map(json, function (value, index) {
        return [value];
      });
      var to_string = to_array.join(" ");
      to_string = to_string.replace(/\+/g, " ");
      var referrer = $.cookie('utm_referrer');
      var medium = $.cookie('utm_medium');
      var UTM = '';
      if (referrer != null) {
        UTM = to_string + " " + $.cookie('utm_referrer');
      }
      else {
        UTM = to_string;
      }
      $('body').on("click", ".webform-client-form-18135", function () {
        $("input[name='submitted[leadsource]']").val(medium);
        $("input[name='submitted[utm_info]']").val(UTM);
      });
    }
  });
})(jQuery);
