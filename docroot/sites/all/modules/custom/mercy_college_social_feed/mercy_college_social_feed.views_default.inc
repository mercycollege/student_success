<?php
/**
 * @file
 * mercy_college_social_feed.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function mercy_college_social_feed_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'instagram_feed';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'instagram_social_feed_photos';
  $view->human_name = 'Instagram Feed';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Instagram Feed';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '30';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Instagram photo: Instagram_link */
  $handler->display->display_options['fields']['instagram_link']['id'] = 'instagram_link';
  $handler->display->display_options['fields']['instagram_link']['table'] = 'instagram_social_feed_photos';
  $handler->display->display_options['fields']['instagram_link']['field'] = 'instagram_link';
  $handler->display->display_options['fields']['instagram_link']['label'] = '';
  $handler->display->display_options['fields']['instagram_link']['exclude'] = TRUE;
  $handler->display->display_options['fields']['instagram_link']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['instagram_link']['display_as_link'] = FALSE;
  /* Field: Instagram photo: Thumbnail */
  $handler->display->display_options['fields']['thumbnail']['id'] = 'thumbnail';
  $handler->display->display_options['fields']['thumbnail']['table'] = 'instagram_social_feed_photos';
  $handler->display->display_options['fields']['thumbnail']['field'] = 'thumbnail';
  $handler->display->display_options['fields']['thumbnail']['label'] = '';
  $handler->display->display_options['fields']['thumbnail']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['thumbnail']['alter']['text'] = '<img src="[thumbnail]" alt="Instagram photo" />';
  $handler->display->display_options['fields']['thumbnail']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['thumbnail']['alter']['path'] = '[instagram_link]';
  $handler->display->display_options['fields']['thumbnail']['alter']['target'] = '_blank';
  $handler->display->display_options['fields']['thumbnail']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['thumbnail']['display_as_link'] = FALSE;
  /* Field: Instagram photo: Instagram_user */
  $handler->display->display_options['fields']['instagram_user']['id'] = 'instagram_user';
  $handler->display->display_options['fields']['instagram_user']['table'] = 'instagram_social_feed_photos';
  $handler->display->display_options['fields']['instagram_user']['field'] = 'instagram_user';
  $handler->display->display_options['fields']['instagram_user']['label'] = '';
  $handler->display->display_options['fields']['instagram_user']['alter']['text'] = 'http://instagram.com/[instagram_user]';
  $handler->display->display_options['fields']['instagram_user']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['instagram_user']['alter']['path'] = 'http://instagram.com/[instagram_user]';
  $handler->display->display_options['fields']['instagram_user']['alter']['target'] = '_blank';
  $handler->display->display_options['fields']['instagram_user']['element_label_colon'] = FALSE;
  /* Sort criterion: Instagram photo: Created_time */
  $handler->display->display_options['sorts']['created_time']['id'] = 'created_time';
  $handler->display->display_options['sorts']['created_time']['table'] = 'instagram_social_feed_photos';
  $handler->display->display_options['sorts']['created_time']['field'] = 'created_time';
  $handler->display->display_options['sorts']['created_time']['order'] = 'DESC';

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $handler->display->display_options['defaults']['use_ajax'] = FALSE;
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'mini';
  $handler->display->display_options['pager']['options']['items_per_page'] = '3';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Instagram photo: Instagram_link */
  $handler->display->display_options['fields']['instagram_link']['id'] = 'instagram_link';
  $handler->display->display_options['fields']['instagram_link']['table'] = 'instagram_social_feed_photos';
  $handler->display->display_options['fields']['instagram_link']['field'] = 'instagram_link';
  $handler->display->display_options['fields']['instagram_link']['label'] = '';
  $handler->display->display_options['fields']['instagram_link']['exclude'] = TRUE;
  $handler->display->display_options['fields']['instagram_link']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['instagram_link']['display_as_link'] = FALSE;
  /* Field: Instagram photo: Thumbnail */
  $handler->display->display_options['fields']['thumbnail']['id'] = 'thumbnail';
  $handler->display->display_options['fields']['thumbnail']['table'] = 'instagram_social_feed_photos';
  $handler->display->display_options['fields']['thumbnail']['field'] = 'thumbnail';
  $handler->display->display_options['fields']['thumbnail']['label'] = '';
  $handler->display->display_options['fields']['thumbnail']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['thumbnail']['alter']['text'] = '<img src="[thumbnail]" alt="Instagram photo" />';
  $handler->display->display_options['fields']['thumbnail']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['thumbnail']['alter']['path'] = '[instagram_link]';
  $handler->display->display_options['fields']['thumbnail']['alter']['target'] = '_blank';
  $handler->display->display_options['fields']['thumbnail']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['thumbnail']['display_as_link'] = FALSE;
  /* Field: Instagram photo: Created_time */
  $handler->display->display_options['fields']['created_time']['id'] = 'created_time';
  $handler->display->display_options['fields']['created_time']['table'] = 'instagram_social_feed_photos';
  $handler->display->display_options['fields']['created_time']['field'] = 'created_time';
  $handler->display->display_options['fields']['created_time']['label'] = '';
  $handler->display->display_options['fields']['created_time']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['created_time']['date_format'] = 'time ago';
  $handler->display->display_options['fields']['created_time']['second_date_format'] = 'long';
  /* Field: Instagram photo: Instagram_user */
  $handler->display->display_options['fields']['instagram_user']['id'] = 'instagram_user';
  $handler->display->display_options['fields']['instagram_user']['table'] = 'instagram_social_feed_photos';
  $handler->display->display_options['fields']['instagram_user']['field'] = 'instagram_user';
  $handler->display->display_options['fields']['instagram_user']['label'] = '';
  $handler->display->display_options['fields']['instagram_user']['alter']['text'] = 'http://instagram.com/[instagram_user]';
  $handler->display->display_options['fields']['instagram_user']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['instagram_user']['alter']['path'] = 'http://instagram.com/[instagram_user]';
  $handler->display->display_options['fields']['instagram_user']['alter']['target'] = '_blank';
  $handler->display->display_options['fields']['instagram_user']['element_label_colon'] = FALSE;
  /* Field: Instagram photo: Caption */
  $handler->display->display_options['fields']['caption']['id'] = 'caption';
  $handler->display->display_options['fields']['caption']['table'] = 'instagram_social_feed_photos';
  $handler->display->display_options['fields']['caption']['field'] = 'caption';
  $handler->display->display_options['fields']['caption']['label'] = '';
  $handler->display->display_options['fields']['caption']['alter']['max_length'] = '120';
  $handler->display->display_options['fields']['caption']['alter']['trim'] = TRUE;
  $handler->display->display_options['fields']['caption']['element_label_colon'] = FALSE;
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Instagram photo: Approve */
  $handler->display->display_options['filters']['approve']['id'] = 'approve';
  $handler->display->display_options['filters']['approve']['table'] = 'instagram_social_feed_photos';
  $handler->display->display_options['filters']['approve']['field'] = 'approve';
  $handler->display->display_options['filters']['approve']['value']['value'] = '1';
  $export['instagram_feed'] = $view;

  $view = new view();
  $view->name = 'mc_facebook_feed';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'MC : Facebook Feed';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'mini';
  $handler->display->display_options['pager']['options']['items_per_page'] = '3';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Link */
  $handler->display->display_options['fields']['field_links']['id'] = 'field_links';
  $handler->display->display_options['fields']['field_links']['table'] = 'field_data_field_links';
  $handler->display->display_options['fields']['field_links']['field'] = 'field_links';
  $handler->display->display_options['fields']['field_links']['label'] = '';
  $handler->display->display_options['fields']['field_links']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_links']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_links']['click_sort_column'] = 'url';
  $handler->display->display_options['fields']['field_links']['type'] = 'link_plain';
  $handler->display->display_options['fields']['field_links']['settings'] = array(
    'empty_fields_handler' => '',
    'empty_fields_emptyfieldtext_empty_text' => '',
    'field_formatter_class' => '',
    'conditions' => array(
      0 => array(
        'condition' => '',
      ),
    ),
    'linked_field' => array(
      'linked' => 0,
      'destination' => '',
      'advanced' => array(
        'title' => '',
        'target' => '',
        'class' => '',
        'rel' => '',
        'text' => '',
      ),
    ),
  );
  /* Field: Content: Post date */
  $handler->display->display_options['fields']['created']['id'] = 'created';
  $handler->display->display_options['fields']['created']['table'] = 'node';
  $handler->display->display_options['fields']['created']['field'] = 'created';
  $handler->display->display_options['fields']['created']['label'] = '';
  $handler->display->display_options['fields']['created']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['created']['date_format'] = 'time ago';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: Image URL */
  $handler->display->display_options['fields']['field_image_url']['id'] = 'field_image_url';
  $handler->display->display_options['fields']['field_image_url']['table'] = 'field_data_field_image_url';
  $handler->display->display_options['fields']['field_image_url']['field'] = 'field_image_url';
  $handler->display->display_options['fields']['field_image_url']['label'] = '';
  $handler->display->display_options['fields']['field_image_url']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_image_url']['alter']['text'] = '<img width="80px" src="[field_image_url]" />';
  $handler->display->display_options['fields']['field_image_url']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_image_url']['alter']['path'] = '[field_links]';
  $handler->display->display_options['fields']['field_image_url']['alter']['target'] = '_blank';
  $handler->display->display_options['fields']['field_image_url']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_image_url']['click_sort_column'] = 'url';
  $handler->display->display_options['fields']['field_image_url']['type'] = 'link_plain';
  $handler->display->display_options['fields']['field_image_url']['settings'] = array(
    'empty_fields_handler' => '',
    'empty_fields_emptyfieldtext_empty_text' => '',
    'field_formatter_class' => '',
    'conditions' => array(
      0 => array(
        'condition' => '',
      ),
    ),
    'linked_field' => array(
      'linked' => 0,
      'destination' => '',
      'advanced' => array(
        'title' => '',
        'target' => '',
        'class' => '',
        'rel' => '',
        'text' => '',
      ),
    ),
  );
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['body']['alter']['path'] = '[field_links]';
  $handler->display->display_options['fields']['body']['alter']['target'] = '_blank';
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['type'] = 'text_plain';
  $handler->display->display_options['fields']['body']['settings'] = array(
    'empty_fields_handler' => '',
    'empty_fields_emptyfieldtext_empty_text' => '',
    'field_formatter_class' => '',
    'conditions' => array(
      0 => array(
        'condition' => '',
      ),
    ),
    'linked_field' => array(
      'linked' => 0,
      'destination' => '',
      'advanced' => array(
        'title' => '',
        'target' => '',
        'class' => '',
        'rel' => '',
        'text' => '',
      ),
    ),
  );
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'facebook_status_item' => 'facebook_status_item',
  );

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block_1');
  $export['mc_facebook_feed'] = $view;

  $view = new view();
  $view->name = 'mc_twitter_feed';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'twitter';
  $view->human_name = 'MC : Twitter Feed';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'mini';
  $handler->display->display_options['pager']['options']['items_per_page'] = '3';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['content'] = '<a href="http://twitter.com/mercycollege" target="_blank">@mercycollege</a>';
  $handler->display->display_options['header']['area']['format'] = 'full_html';
  /* Field: Twitter: Twitter status message ID */
  $handler->display->display_options['fields']['twitter_id']['id'] = 'twitter_id';
  $handler->display->display_options['fields']['twitter_id']['table'] = 'twitter';
  $handler->display->display_options['fields']['twitter_id']['field'] = 'twitter_id';
  $handler->display->display_options['fields']['twitter_id']['label'] = '';
  $handler->display->display_options['fields']['twitter_id']['exclude'] = TRUE;
  $handler->display->display_options['fields']['twitter_id']['element_label_colon'] = FALSE;
  /* Field: Twitter: Created time */
  $handler->display->display_options['fields']['created_time']['id'] = 'created_time';
  $handler->display->display_options['fields']['created_time']['table'] = 'twitter';
  $handler->display->display_options['fields']['created_time']['field'] = 'created_time';
  $handler->display->display_options['fields']['created_time']['label'] = '';
  $handler->display->display_options['fields']['created_time']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['created_time']['date_format'] = 'time ago';
  /* Field: Twitter: Message text */
  $handler->display->display_options['fields']['text']['id'] = 'text';
  $handler->display->display_options['fields']['text']['table'] = 'twitter';
  $handler->display->display_options['fields']['text']['field'] = 'text';
  $handler->display->display_options['fields']['text']['label'] = '';
  $handler->display->display_options['fields']['text']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['text']['link_urls'] = 1;
  $handler->display->display_options['fields']['text']['link_usernames'] = 1;
  $handler->display->display_options['fields']['text']['link_hashtags'] = 1;
  $handler->display->display_options['fields']['text']['link_attributes'] = 1;
  /* Field: Twitter: Web Intents */
  $handler->display->display_options['fields']['web_intents']['id'] = 'web_intents';
  $handler->display->display_options['fields']['web_intents']['table'] = 'twitter';
  $handler->display->display_options['fields']['web_intents']['field'] = 'web_intents';
  $handler->display->display_options['fields']['web_intents']['label'] = '';
  $handler->display->display_options['fields']['web_intents']['element_label_colon'] = FALSE;
  /* Sort criterion: Twitter: Created time */
  $handler->display->display_options['sorts']['created_time']['id'] = 'created_time';
  $handler->display->display_options['sorts']['created_time']['table'] = 'twitter';
  $handler->display->display_options['sorts']['created_time']['field'] = 'created_time';
  $handler->display->display_options['sorts']['created_time']['order'] = 'DESC';

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block_1');
  $export['mc_twitter_feed'] = $view;

  $view = new view();
  $view->name = 'mc_youtube_feed';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'MC : YouTube Feed';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['pure_distinct'] = TRUE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'mini';
  $handler->display->display_options['pager']['options']['items_per_page'] = '3';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Published Timestamp */
  $handler->display->display_options['fields']['field_published_timestamp']['id'] = 'field_published_timestamp';
  $handler->display->display_options['fields']['field_published_timestamp']['table'] = 'field_data_field_published_timestamp';
  $handler->display->display_options['fields']['field_published_timestamp']['field'] = 'field_published_timestamp';
  $handler->display->display_options['fields']['field_published_timestamp']['label'] = '';
  $handler->display->display_options['fields']['field_published_timestamp']['element_type'] = '0';
  $handler->display->display_options['fields']['field_published_timestamp']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_published_timestamp']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_published_timestamp']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_published_timestamp']['type'] = 'format_interval';
  $handler->display->display_options['fields']['field_published_timestamp']['settings'] = array(
    'interval' => '2',
    'interval_display' => 'time ago',
    'empty_fields_handler' => '',
    'empty_fields_emptyfieldtext_empty_text' => '',
    'field_formatter_class' => '',
    'conditions' => array(
      0 => array(
        'condition' => '',
      ),
    ),
    'linked_field' => array(
      'linked' => 0,
      'destination' => '',
      'advanced' => array(
        'title' => '',
        'target' => '',
        'class' => '',
        'rel' => '',
        'text' => '',
      ),
    ),
  );
  $handler->display->display_options['fields']['field_published_timestamp']['field_api_classes'] = TRUE;
  /* Field: Content: Video URL */
  $handler->display->display_options['fields']['field_video_url']['id'] = 'field_video_url';
  $handler->display->display_options['fields']['field_video_url']['table'] = 'field_data_field_video_url';
  $handler->display->display_options['fields']['field_video_url']['field'] = 'field_video_url';
  $handler->display->display_options['fields']['field_video_url']['group_type'] = 'count_distinct';
  $handler->display->display_options['fields']['field_video_url']['label'] = '';
  $handler->display->display_options['fields']['field_video_url']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_video_url']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_video_url']['click_sort_column'] = 'url';
  $handler->display->display_options['fields']['field_video_url']['type'] = 'link_plain';
  $handler->display->display_options['fields']['field_video_url']['settings'] = array(
    'field_formatter_class' => '',
    'linked_field' => array(
      'linked' => 0,
      'destination' => '',
      'advanced' => array(
        'title' => '',
        'target' => '',
        'class' => '',
        'rel' => '',
        'text' => '',
      ),
    ),
  );
  $handler->display->display_options['fields']['field_video_url']['group_column'] = 'entity_id';
  /* Field: Content: Image URL */
  $handler->display->display_options['fields']['field_image_url']['id'] = 'field_image_url';
  $handler->display->display_options['fields']['field_image_url']['table'] = 'field_data_field_image_url';
  $handler->display->display_options['fields']['field_image_url']['field'] = 'field_image_url';
  $handler->display->display_options['fields']['field_image_url']['group_type'] = 'count_distinct';
  $handler->display->display_options['fields']['field_image_url']['label'] = '';
  $handler->display->display_options['fields']['field_image_url']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_image_url']['alter']['text'] = '<a class="fancybox" href="http://www.youtube.com/watch?v=[field_video_url]"><img width="120px" src="[field_image_url-url]"/></a>';
  $handler->display->display_options['fields']['field_image_url']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_image_url']['click_sort_column'] = 'url';
  $handler->display->display_options['fields']['field_image_url']['type'] = 'link_plain';
  $handler->display->display_options['fields']['field_image_url']['settings'] = array(
    'empty_fields_handler' => '',
    'empty_fields_emptyfieldtext_empty_text' => '',
    'field_formatter_class' => '',
    'conditions' => array(
      0 => array(
        'condition' => '',
      ),
    ),
    'linked_field' => array(
      'linked' => 0,
      'destination' => '',
      'advanced' => array(
        'title' => '',
        'target' => '',
        'class' => '',
        'rel' => '',
        'text' => '',
      ),
    ),
  );
  $handler->display->display_options['fields']['field_image_url']['group_column'] = 'entity_id';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['path'] = 'http://www.youtube.com/watch?v=[field_video_url] ';
  $handler->display->display_options['fields']['title']['alter']['target'] = '_blank';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['group_type'] = 'count_distinct';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['exclude'] = TRUE;
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['type'] = 'text_plain';
  /* Sort criterion: Content: Published Timestamp (field_published_timestamp) */
  $handler->display->display_options['sorts']['field_published_timestamp_value']['id'] = 'field_published_timestamp_value';
  $handler->display->display_options['sorts']['field_published_timestamp_value']['table'] = 'field_data_field_published_timestamp';
  $handler->display->display_options['sorts']['field_published_timestamp_value']['field'] = 'field_published_timestamp_value';
  $handler->display->display_options['sorts']['field_published_timestamp_value']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['group_type'] = 'count_distinct';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'min' => '',
    'max' => '',
    'value' => '',
    'youtube_media_item' => 'youtube_media_item',
  );

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block_1');
  $export['mc_youtube_feed'] = $view;

  return $export;
}
