<?php
/**
 * @file
 * mercy_college_social_feed.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function mercy_college_social_feed_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'facebook_feed';
  $feeds_importer->config = array(
    'name' => 'Facebook Feed',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'FeedsFacebookFetcher',
      'config' => array(
        'auto_detect_feeds' => FALSE,
        'use_pubsubhubbub' => FALSE,
        'designated_hub' => '',
        'request_timeout' => NULL,
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsFacebookParser',
      'config' => array(),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => 0,
        'authorize' => 1,
        'mappings' => array(
          0 => array(
            'source' => 'message',
            'target' => 'body',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'post_id',
            'target' => 'title',
            'unique' => 1,
          ),
          2 => array(
            'source' => 'created_time',
            'target' => 'created',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'picture',
            'target' => 'field_image_url:url',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'link',
            'target' => 'field_links:url',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'post_id',
            'target' => 'field_post_id',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '1',
        'input_format' => 'plain_text',
        'skip_hash_check' => 0,
        'bundle' => 'facebook_status_item',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '0',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['facebook_feed'] = $feeds_importer;

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'youtube_feed_2_1';
  $feeds_importer->config = array(
    'name' => 'YouTube Feed 2.1',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'FeedsHTTPFetcher',
      'config' => array(
        'auto_detect_feeds' => FALSE,
        'use_pubsubhubbub' => FALSE,
        'designated_hub' => '',
        'request_timeout' => NULL,
        'auto_scheme' => 'http',
        'accept_invalid_cert' => FALSE,
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsJSONPathParser',
      'config' => array(
        'context' => '$.items[*]',
        'sources' => array(
          'jsonpath_parser:3' => 'id',
          'jsonpath_parser:0' => 'snippet.title',
          'jsonpath_parser:1' => 'snippet.description',
          'jsonpath_parser:6' => 'contentDetails.videoId',
          'jsonpath_parser:2' => 'http://www.youtube.com/watch?v={field_video_url:title}',
          'jsonpath_parser:4' => 'snippet.thumbnails.default.url',
          'jsonpath_parser:5' => 'snippet.publishedAt',
          'jsonpath_parser:7' => 'snippet.publishedAt',
        ),
        'debug' => array(
          'options' => array(
            'context' => 0,
            'jsonpath_parser:3' => 0,
            'jsonpath_parser:0' => 0,
            'jsonpath_parser:1' => 0,
            'jsonpath_parser:6' => 0,
            'jsonpath_parser:2' => 0,
            'jsonpath_parser:4' => 0,
            'jsonpath_parser:5' => 0,
            'jsonpath_parser:7' => 0,
          ),
        ),
        'allow_override' => 0,
        'convert_four_byte' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => 0,
        'authorize' => 1,
        'mappings' => array(
          0 => array(
            'source' => 'jsonpath_parser:3',
            'target' => 'guid',
            'unique' => 1,
          ),
          1 => array(
            'source' => 'jsonpath_parser:0',
            'target' => 'title',
            'unique' => 0,
          ),
          2 => array(
            'source' => 'jsonpath_parser:1',
            'target' => 'body',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'jsonpath_parser:6',
            'target' => 'field_video_url:title',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'jsonpath_parser:2',
            'target' => 'field_video_url:url',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'jsonpath_parser:4',
            'target' => 'field_image_url:url',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'jsonpath_parser:5',
            'target' => 'created',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'jsonpath_parser:7',
            'target' => 'field_published_timestamp:start',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '0',
        'update_non_existent' => 'skip',
        'input_format' => 'plain_text',
        'skip_hash_check' => 0,
        'bundle' => 'youtube_media_item',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '1800',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['youtube_feed_2_1'] = $feeds_importer;

  return $export;
}
