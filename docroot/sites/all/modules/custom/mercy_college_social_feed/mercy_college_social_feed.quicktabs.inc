<?php
/**
 * @file
 * mercy_college_social_feed.quicktabs.inc
 */

/**
 * Implements hook_quicktabs_default_quicktabs().
 */
function mercy_college_social_feed_quicktabs_default_quicktabs() {
  $export = array();

  $quicktabs = new stdClass();
  $quicktabs->disabled = FALSE; /* Edit this to true to make a default quicktabs disabled initially */
  $quicktabs->api_version = 1;
  $quicktabs->machine_name = 'social_feed';
  $quicktabs->ajax = 0;
  $quicktabs->hide_empty_tabs = FALSE;
  $quicktabs->default_tab = 0;
  $quicktabs->title = 'Social Feed Tabs';
  $quicktabs->tabs = array(
    0 => array(
      'vid' => 'instagram_feed',
      'display' => 'block',
      'args' => '',
      'title' => 'Instagram',
      'weight' => '-100',
      'type' => 'view',
    ),
    1 => array(
      'vid' => 'mc_twitter_feed',
      'display' => 'block_1',
      'args' => '',
      'title' => 'Twitter',
      'weight' => '-99',
      'type' => 'view',
    ),
    2 => array(
      'vid' => 'mc_youtube_feed',
      'display' => 'block_1',
      'args' => '',
      'title' => 'YouTube',
      'weight' => '-98',
      'type' => 'view',
    ),
    3 => array(
      'vid' => 'mc_facebook_feed',
      'display' => 'block_1',
      'args' => '',
      'title' => 'Facebook',
      'weight' => '-97',
      'type' => 'view',
    ),
  );
  $quicktabs->renderer = 'quicktabs';
  $quicktabs->style = 'nostyle';
  $quicktabs->options = array();

  // Translatables
  // Included for use with string extractors like potx.
  t('Facebook');
  t('Instagram');
  t('Social Feed Tabs');
  t('Twitter');
  t('YouTube');

  $export['social_feed'] = $quicktabs;

  return $export;
}
