<?php

/** 
* Implementation of hook_views_data
*/

function  mercy_events_view_views_data() {
   error_log("Loading views");
   $data['arTable']['table']['group'] = t('Arduino Data');
   $data['arTable']['table']['base'] = array(
      'field' => 'id',
      'title' => t('arduino'),
      'help' => t('Autoincremented ID field, to enable views integration'),
      'database' => 'arduino',
      'weight' => -10,
   );

   $data['arTable']['value'] = array(
      'title' => t('Analog Input Value'),
      'help' => t('Analog Input Value'),
      'field' => array(
         'handler' => 'views_handler_field',
         ' click sortable' => TRUE,
      ),
      'sort' => array(
         'handler' => 'views_handler_sort',
      ),
      'filter' => array(
         'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
         'handler' => 'views_handler_argument_string',
      ),
    );

   $data['arTable']['serialNum'] = array(
      'title' => t('Serial Number'),
      'help' => t('Serial Number'),
      'field' => array(
         'handler' => 'views_handler_field',
         ' click sortable' => TRUE,
      ),
      'sort' => array(
         'handler' => 'views_handler_sort',
      ),
      'filter' => array(
         'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
         'handler' => 'views_handler_argument_string',
      ),
    );	

   $data['arTable']['date'] = array(
      'title' => t('Date'),
      'help' => t('Date'),
      'field' => array(
         'handler' => 'views_handler_field',
         ' click sortable' => TRUE,
      ),
      'sort' => array(
         'handler' => 'views_handler_sort',
      ),
      'filter' => array(
         'handler' => 'date_views_filter_handler',
      ),
      'argument' => array(
         'handler' => 'views_handler_argument_string',
      ),
    );

   $data['arTable']['IP'] = array(
      'title' => t('Senders IP Address'),
      'help' => t('Senders IP Address'),
      'field' => array(
         'handler' => 'views_handler_field',
         ' click sortable' => TRUE,
      ),
      'sort' => array(
         'handler' => 'views_handler_sort',
      ),
      'filter' => array(
         'handler' => 'views_handler_filter_string',
      ),
      'argument' => array(
         'handler' => 'views_handler_argument_string',
      ),
    );
   $data['arTable']['datetime'] = array(
      'title' => t('Datetime'),
      'help' => t('Datetime'),
      'field' => array(
         'handler' => 'views_handler_field',
         ' click sortable' => TRUE,
      ),
      'sort' => array(
         'handler' => 'views_handler_sort',
      ),
      'filter' => array(
         'handler' => 'date_views_filter_handler',
      ),
      'argument' => array(
         'handler' => 'views_handler_argument_string',
      ),
    );

   return $data;

}