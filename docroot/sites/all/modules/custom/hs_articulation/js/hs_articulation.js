/**
 * Created by shanford on 4/13/15.
 */
(function($, Drupal){
    //The function name is prototyped as part fo the Drupal.ajax namespace, adding to the commands:
    Drupal.ajax.prototype.commands.hs_articulationDateCallback = function(ajax, response, status)
    {
        $('#edit-dob label').text('Date of Birthing *');
    };

}(jQuery, Drupal));
