<?php

function hs_articulation_install() {

}

function hs_articulation_uninstall() {

}


function hs_articulation_schema() {
  $schema['hs_articulation_table'] = array (
    'description' => t('hs_articulation Registration Table'),
    'fields' => array(
      'registrationGUID' => array(
        'description' => t('GUID for HS Articulation'),
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'school' => array(
        'description' => t('Which school offers the course(s)'),
        'type' => 'varchar',
        'length' => 100,
        'not null' => TRUE,
      ),
      'grade' => array(
        'description' => t('Grade of the applicant'),
        'type' => 'varchar',
        'length' => 25,
        'not null' => TRUE,
      ),
      'courses' => array(
        'description' => t('Which course(s) are selected'),
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ),
      'crn' => array(
        'description' => t('Course CRN numbers comma separated'),
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ),
      'first_name' => array(
        'description' => t('Applicant First Name'),
        'type' => 'varchar',
        'length' => 100,
        'not null' => TRUE,
      ),
      'last_name' => array(
        'description' => t('Applicant Last Name'),
        'type' => 'varchar',
        'length' => 100,
        'not null' => TRUE,
      ),
      'ssn' => array(
        'description' => t('Encrypted Applicant SSN'),
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ),
      'email' => array(
        'description' => t('Applicant Email'),
        'type' => 'varchar',
        'length' => 100,
        'not null' => TRUE,
      ),
      'address' => array(
        'description' => t('Applicant Address'),
        'type' => 'varchar',
        'length' => 100,
        'not null' => TRUE,
      ),
      'address_2' => array(
        'description' => t('Applicant Address 2'),
        'type' => 'varchar',
        'length' => 100,
        'not null' => FALSE,
      ),
      'city' => array(
        'description' => t('Applicant City'),
        'type' => 'varchar',
        'length' => 100,
        'not null' => TRUE,
      ),
      'state' => array(
        'description' => t('Applicant State'),
        'type' => 'varchar',
        'length' => 2,
        'not null' => TRUE,
      ),
      'zip' => array(
        'description' => t('Applicant Zip Code'),
        'type' => 'varchar',
        'length' => 12,
        'not null' => TRUE,
      ),
      'phone' => array(
        'description' => t('Applicant Phone#'),
        'type' => 'varchar',
        'length' => 100,
        'not null' => TRUE,
      ),
      'dob' => array(
        'description' => t('Applicant Date of Birth'),
        'type' => 'varchar',
        'length' => 10,
        'not null' => TRUE,
      ),
      'gender' => array(
        'description' => t('Applicant Sex'),
        'type' => 'varchar',
        'length' => 10,
        'not null' => TRUE,
      ),
      'ethnicity' => array(
        'description' => t('Applicant Ethnicity'),
        'type' => 'varchar',
        'length' => 40,
        'not null' => FALSE,
      ),
      'race' => array(
        'description' => t('Applicant Race'),
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ),
      'term' => array(
        'description' => t('The term/date for this registration period'),
        'type' => 'varchar',
        'length' => 40,
        'not null' => TRUE,
      ),
      'createfromIP' => array(
        'description' => t('The IP Address of the user for this application request.'),
        'type' => 'varchar',
        'length' => 20,
        'not null' => FALSE,
      ),
      'createDate' => array(
        'description' => t('The date the record was created.'),
        'type' => 'varchar',
        'length' => 20,
        'not null' => TRUE,
      ),
      'lastModifiedDate' => array(
        'description' => t('The date the record was last updated using UNIX TIMESTAMP.'),
        'type' => 'varchar',
        'length' => 20,
        'not null' => TRUE,
        'default' => 0,
      ),
      'lasteditfromIP' => array(
        'description' => t('The IP the last edit originated from.'),
        'type' => 'varchar',
        'length' => 20,
        'not null' => FALSE,
      ),
      'amtPaid' => array(
        'description' => t('The amount, if any the applicant paid.'),
        'type' => 'varchar',
        'length' => 25,
        'not null' => FALSE,
      ),
      'cashnetStatus' => array(
        'description' => t('The status, if any, returned from CashNET.'),
        'type' => 'varchar',
        'length' => 100,
        'not null' => FALSE,
      ),
      'bannerStatus' => array(
        'description' => t('The status of the applicants registration in Banner.'),
        'type' => 'varchar',
        'length' => 100,
        'not null' => FALSE,
      ),
      'registrationCost' => array(
        'description' => t('The total cost of the course(s)'),
        'type' => 'varchar',
        'length' => 25,
        'not null' => FALSE,
      ),
      'registrationStatus' => array(
        'description' => t('The status of the applicants registration status.'),
        'type' => 'varchar',
        'length' => 100,
        'not null' => FALSE,
      ),
    ),
    'primary key' => array('registrationGUID'),
  );

  $schema['hs_articulation_cashnet_table'] = array (
    'description' => t('hs_articulation CashNET Notification Table'),
    'fields' => array(
      'id' => array(
        'description' => t('Auto-increment ID for CashNET Notification Table'),
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'request' => array(
        'description' => t('Which school offers the course(s)'),
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ),
      'notifyDate' => array(
        'description' => t('Date of the transaction'),
        'mysql_type' => 'datetime',
        'not null' => TRUE,
      ),
      'fromIP' => array(
        'description' => t('IP Address where request originated'),
        'type' => 'varchar',
        'length' => 20,
        'not null' => TRUE,
      ),
      'referer' => array(
        'description' => t('URL originating request'),
        'type' => 'varchar',
        'length' => 100,
        'not null' => TRUE,
      ),
      'appGUID' => array(
        'description' => t('id of user record in Registration Table'),
        'type' => 'varchar',
        'length' => 100,
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('id'),
  );
  return $schema;
}