<?php

/**
 * @file
 * Views integration for the hs_articulation custom module
 */
function hs_articulation_views_data() {
  $table = array(
    'hs_articulation_table' => array(
      'table' => array(
        'group' => 'hsa',
        'base' => array(
          'field' => 'registrationGUID',
          'title' => 'HS Articulation Table',
          'help' => 'HS Articulation Custom Database table'
        )
      ),
      'registrationGUID' => array(
        'title' => t('HS Articulation Record Id'),
        'field' => array(
          'click sortable' => TRUE,
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_numeric'
        ),
        'sort' => array(
          'handler' => 'views_handler_sort'
        )
      ),
      'school' => array(
        'title' => t('HSA School'),
        'field' => array(
          'click sortable' => TRUE,
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_string'
        ),
        'sort' => array(
          'handler' => 'views_handler_sort'
        )
      ),
      'term' => array(
        'title' => t('HSA Term'),
        'field' => array(
          'click sortable' => TRUE,
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_string'
        ),
        'sort' => array(
          'handler' => 'views_handler_sort'
        )
      ),
      'grade' => array(
        'title' => t('HSA Grade'),
        'field' => array(
          'click sortable' => TRUE,
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_string'
        ),
        'sort' => array(
          'handler' => 'views_handler_sort'
        )
      ),
      'courses' => array(
        'title' => t('HSA Courses'),
        'field' => array(
          'click sortable' => TRUE,
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_string'
        ),
        'sort' => array(
          'handler' => 'views_handler_sort'
        )
      ),
      'first_name' => array(
        'title' => t('HSA First Name'),
        'field' => array(
          'click sortable' => TRUE,
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_string'
        ),
        'sort' => array(
          'handler' => 'views_handler_sort'
        )
      ),
      'last_name' => array(
        'title' => t('HSA Last Name'),
        'field' => array(
          'click sortable' => TRUE,
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_string'
        ),
        'sort' => array(
          'handler' => 'views_handler_sort'
        )
      ),
      'email' => array(
        'title' => t('HSA Email'),
        'field' => array(
          'click sortable' => TRUE,
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_string'
        ),
        'sort' => array(
          'handler' => 'views_handler_sort'
        )
      ),
      'phone' => array(
        'title' => t('HSA Phone'),
        'field' => array(
          'click sortable' => TRUE,
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_string'
        ),
        'sort' => array(
          'handler' => 'views_handler_sort'
        )
      ),
      'lastModifiedDate' => array(
        'title' => t('HSA Date Last Edited'),
        'field' => array(
          'click sortable' => TRUE,
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_string'
        ),
        'sort' => array(
          'handler' => 'views_handler_sort'
        )
      ),
      'amtPaid' => array(
        'title' => t('HSA Amount Paid'),
        'field' => array(
          'click sortable' => TRUE,
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_string'
        ),
        'sort' => array(
          'handler' => 'views_handler_sort'
        )
      ),
      'cashnetStatus' => array(
        'title' => t('HSA CashNET Status'),
        'field' => array(
          'click sortable' => TRUE,
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_string'
        ),
        'sort' => array(
          'handler' => 'views_handler_sort'
        )
      ),
      'bannerStatus' => array(
        'title' => t('HSA Banner Status'),
        'field' => array(
          'click sortable' => TRUE,
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_string'
        ),
        'sort' => array(
          'handler' => 'views_handler_sort'
        )
      ),
      'registrationStatus' => array(
        'title' => t('HSA Registration Status'),
        'field' => array(
          'click sortable' => TRUE,
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_string'
        ),
        'sort' => array(
          'handler' => 'views_handler_sort'
        )
      )
    )
  );
  return $table;
}