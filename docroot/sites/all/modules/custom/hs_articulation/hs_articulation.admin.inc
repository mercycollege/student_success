<?php
// $Id$

/**
 * @file
 * Admin page callback for the High School Articulation module
 */


/**
 * Builds and returns the articulation settings form.
 */
function hs_articulation_admin_settings() {
  $form['hs_articulation_begin_date'] = array(
    '#type' => 'date',
    '#title' => t('Begin HS Articulation Date'),
    '#default_value' => variable_get('hs_articulation_begin_date', ''),
    '#description' => t('The date to begin accepting applications'),
    '#required' => TRUE,
  );

  $form['hs_articulation_end_date'] = array(
    '#type' => 'date',
    '#title' => t('End HS Articulation Date'),
    '#default_value' => variable_get('hs_articulation_end_date', ''),
    '#description' => t('The last date for accepting applications'),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}