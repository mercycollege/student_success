<?php
  
// $Id$

/**
 * @file
 * Admin page callback for the High School Articulation module
 */


/**
 * Builds and returns the articulation settings form.
 */
function hsuftapi_library_admin_settings() {
  
  $form['hsuftapi'] = array(
    '#type' => 'fieldset',
    '#title' => 'HS UFT REST API',
  );
  
  $form['hsuftapi']['hsuftapi_endpoint'] = array(
    '#type' => 'textfield',
    '#title' => t('Endpoint URL'),
    '#default_value' => variable_get('hsuftapi_endpoint', ''),
    '#required' => TRUE,
  );
  
  $form['hsuftapi']['hsuftapi_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API Key'),
    '#default_value' => variable_get('hsuftapi_key', ''),
    '#description' => t('The API Key provided for the HS UFT REST API'),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}