<?php
// $Id$

/**
 * @file
 * Admin page callback for the UFT module
 */

/*
function uft_admin_process_form($form, &$form_state) {

  $output = drupal_get_form('uft_form');

  return $form;
}

function uft_admin_student_modify() {

  $output = t('Add a new student submission:');
  $output .= t('Or click on the Modify or Delete links to edit an existing submission');

  $form = drupal_get_form('uft_form');

  //display existing submissions
  $sql = "SELECT * FROM {uft_table}";
  $result = db_query($sql);

  $header = array(
    'uftGUID',
    'School',
    'Courses',
    'Existing Credits',
    'First Name',
    'Last Name',
    'SSN',
    'Email',
    'Address',
    'Address',
    'City',
    'State',
    'Zip',
    'Phone',
    'Date of Birth',
    'Gender',
    'Ethnicity',
    'Race'
  );
  $rows = array();

  while ($data = $result->fetchObject()) {
    $rows[] = array(
      l('Edit' . $data->uftGUID, '/admin/settings/uft/modify', array(
        'query' => array(
          'mod' => $data->uftGUID
        )
      )),
      $data->uftGUID,
      $data->school,
      $data->courses,
      $data->existing_credits,
      $data->first_name,
      $data->last_name,
      $data->ssn,
      $data->email,
      $data->address,
      $data->address_2,
      $data->city,
      $data->state,
      $data->zip,
      $data->phone,
      $data->dob,
      $data->gender,
      $data->ethnicity,
      $data->race,
      l('Delete ' . $data->uftGUID, 'admin/uft/modify', array(
        'query' => array(
          'del' => $data->uftGUID
        )
      )),
    );
  }

  //Output the table with paging
  $output .= theme_table(
    array(
      'header' => $header,
      'rows' => $rows,
      'attributes' => array(),
      'sticky' => TRUE,
      'caption' => '',
      'colgroups' => array(),
      'empty' => t("There are no records to display")
    )
  );

  return $output;
}


function uft_admin_table() {
  $student['submission'] = array(
    '#theme'  => 'table',
    '#header' => $header,
    '#rows'   => $rows,
  );
  return $student;
}

function theme_uft_admin_process_form($variables) {
  // Get the form definition from $variables
  $form = $variables[$form];
  $output = '';
  $student_table = uft_admin_table();
  $output .= drupal_render($student_table);
  return $output;
}*/

/**
 * Implements hook_form()
 * @param $form
 * @param $form_state
 * @return array
 */

function uft_admin_view_records() {
  
  $table = array(
    '#theme' => 'table',
    '#header' => array(
      'uftGUID',
      'School',
      'Courses',
      'Existing Credits',
      'First Name',
      'Last Name',
      'SSN',
      'Email',
      'Address',
      'Address',
      'City',
      'State',
      'Zip',
      'Phone',
      'Date of Birth',
      'Gender',
      'Ethnicity',
      'Race',
    ),
    '#rows' => array(),
  );

  $sql = "SELECT * FROM {uft_table}";
  $result = db_query($sql);
  foreach($result as $row) {
    $table['#rows'][] = array(
      $row->uftGUID,
      $row->school,
      $row->courses,
      $row->existing_credits,
      $row->first_name,
      $row->last_name,
      $row->ssn,
      $row->email,
      $row->address,
      $row->address_2,
      $row->city,
      $row->state,
      $row->zip,
      $row->phone,
      $row->dob,
      $row->gender,
      $row->ethnicity,
      $row->race,
    );
  }

  return $table;

}
