<?php
/**
 * Implements hood_views_data_alter().
 *
 * @param $data
 *  Information about Views' tables and fields
 */
function uft_views_data_alter(&$data) {
  // Add the Title form field to
  $data ['node']['title_edit'] = array(
    'field' => array(
      'title' => t('Title form field'),
      'help' => t('Edit the node title'),
      'handler' => 'uft_field_handler_title_edit',
    ),
  );
}
