<?php
	function universal_rsvp_rfi_conditionals(){

	$form['#node']->webform['conditionals'] = array(
          array(
            'nid' => '18135',
            'rgid' => '0',
            'andor' => 'or',
            'weight' => '-1',
            'rules' => array(
              array(
                'nid' => '18135',
                'rgid' => '0',
                'rid' => '0',
                'source_type' => 'component',
                'source' => '10',
                'operator' => 'equal',
                'value' => 'I_already_visited_campus',
              ),
              array(
                'nid' => '18135',
                'rgid' => '0',
                'rid' => '1',
                'source_type' => 'component',
                'source' => '10',
                'operator' => 'equal',
                'value' => 'I_am_not_interested_in_visiting_at_this_time',
              ),
              array(
                'nid' => '18135',
                'rgid' => '0',
                'rid' => '2',
                'source_type' => 'component',
                'source' => '10',
                'operator' => 'equal',
                'value' => 'I_will_visit_at_a_later_date',
              ),
              array(
                'nid' => '18135',
                'rgid' => '0',
                'rid' => '3',
                'source_type' => 'component',
                'source' => '10',
                'operator' => 'empty',
                'value' => 'I_will_visit_at_a_later_date',
              ),
            ),
            'actions' => array(
              array(
                'nid' => '18135',
                'rgid' => '0',
                'aid' => '0',
                'target_type' => 'component',
                'target' => '33',
                'invert' => '1',
                'action' => 'show',
                'argument' => '',
              ),
              array(
                'nid' => '18135',
                'rgid' => '0',
                'aid' => '1',
                'target_type' => 'component',
                'target' => '34',
                'invert' => '1',
                'action' => 'show',
                'argument' => '',
              ),
            ),
          ),
        );
       return $form['#node']->webform['conditionals'];
}