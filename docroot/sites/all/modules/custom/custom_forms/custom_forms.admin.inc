<?php

/**
* @file
* Admin include file to define form settings.
*/

/**
 * Menu callback for system settings form.
 */
function custom_forms_admin() {
  $form = array();

  $form['custom_forms_sandbox_mode'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Cashnet sandbox mode?'),
    '#default_value' => variable_get('custom_forms_sandbox_mode', FALSE),
    '#description' => t("When checked, form submissions can be made with test credit card data."),
  );

  return system_settings_form($form);
}
