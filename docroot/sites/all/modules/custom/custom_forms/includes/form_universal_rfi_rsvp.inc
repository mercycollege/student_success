<?php
/**
 * @file
 * Provides custom form functions for Mercy.edu's Universal RFI/RSVP form.
 */

/**
 * Modifies the Universal RSVP RFI Webform.
 *
 * @param array $form
 *   Form API structure.
 * @param array $form_state
 *   Form State structure.
 *
 * @return array
 *   Form API structure.
 */
function _custom_forms_universal_rsvp_rfi_form($form, &$form_state) {
  $form['#node']->webform['conditionals'] = universal_rsvp_rfi_conditionals();
  if (isset($_COOKIE['utm_info'])) {
    hide($form['submitted']['mrk_source']);
    hide($form['submitted']['how_did_you_hear_about_us']);
    $cookie_utm = json_decode($_COOKIE['utm_info'], TRUE);
    $leadsource = $_COOKIE['utm_medium'];
    if (isset($_COOKIE['utm_referrer'])) {
      $referrer = $_COOKIE['utm_referrer'];
    }
    else {
      $referrer = NULL;
    }
    $form['submitted']['leadsource']['#default_value'] = $leadsource;
    $form['submitted']['utm_info']['#default_value'] = implode(" ", array_filter($cookie_utm)) . " " . $referrer;
  }
  if (isset($form_state['page_id'])) {
    $obj_id = $form_state['page_id'];
    $node = node_load($obj_id);

    if ($node->type == 'calendar') {
      // If submission notification email is provided,
      // send email to entered address.
      if (isset($node->field_submission_notification[LANGUAGE_NONE][0]['email'])) {
        $form_state['build_info']['args'][0]->webform['emails'][1]['email'] = $node->field_submission_notification[LANGUAGE_NONE][0]['email'];
      }
      // Closes registration for events that have passed.
      $current_date = date('Y-m-d H:i:s');
      if ((isset($node->field_date_of_event[LANGUAGE_NONE][0]['value'])) && ($node->field_date_of_event[LANGUAGE_NONE][0]['value'] < $current_date)) {
        foreach ($form['submitted'] as $v => $k) {
          $form['submitted'][$v] = NULL;
        }
        $form['actions'] = NULL;

        $form['no_form'] = array(
          '#type' => 'markup',
          '#weight' => '',
          '#format' => NULL,
          '#markup' => '<hr><h3>Registration is closed</h3><p>It looks like this event has passed.</p>',
        );
        return $form;
      }
    }
    if ($node->type == 'landing_page') {
      $form['submitted']['top_markup'] = array(
        '#type' => 'markup',
        '#weight' => '',
        '#format' => NULL,
        '#markup' => '<h3>GET MORE INFORMATION!</h3><p>Call us toll-free at <a href="tel:877-637-2946">877-Mercy-GO</a>, visit our website at <a href="https://www.mercy.edu">www.mercy.edu</a> or complete the following form.</p><hr>',
      );

    }
    $form['submitted']['nid']['#value'] = $obj_id;
    if (isset($form['submitted']['submission_for'])) {
      $form['submitted']['submission_for']['#value'] = $node->title;
    }
    if (isset($form['submitted']['program_of_interest_options']['#options'])) {
      $option_array = array();
      foreach ($form['submitted']['program_of_interest_options']['#options'] as $key => $val) {
        $option_array[]['value'] = $val;
      }
      if ((!isset($node->field_program_option_list[LANGUAGE_NONE])) || ($node->field_program_option_list[LANGUAGE_NONE] != $option_array)) {
        $node->field_program_option_list[LANGUAGE_NONE] = $option_array;
        field_attach_update('node', $node);
      }
    }
    if (isset($form['submitted']['open_house_options']['#options'])) {
      $option_array1 = array();
      foreach ($form['submitted']['open_house_options']['#options'] as $key => $val) {
        $option_array1[]['value'] = $val;
      }
      if ((!isset($node->field_open_house_list[LANGUAGE_NONE])) || ($node->field_open_house_list[LANGUAGE_NONE] != $option_array1)) {
        $node->field_open_house_list[LANGUAGE_NONE] = $option_array1;
        field_attach_update('node', $node);
      }
    }
  }
  $form['submitted']['program_of_interest_options']['#type'] = "hidden";
  $form['submitted']['open_house_options']['#type'] = "hidden";
  $form['actions']['previous']['#type'] = "hidden";

  unset($form['submitted']['program_of_interest_options']);
  unset($form['submitted']['open_house_options']);

  array_unshift($form['#validate'], 'validate_rsvp');

  // Implement ajax manually to continue to hold the page/node object id.
  $form['webform_ajax_wrapper_id'] = array(
    '#type' => 'hidden',
  );
  if (isset($form_state['values']['webform_ajax_wrapper_id'])) {
    $form['webform_ajax_wrapper_id']['#value'] = $form_state['values']['webform_ajax_wrapper_id'];
  }
  elseif (isset($form['#node']->webform['webform_ajax_wrapper_id'])) {
    $form['webform_ajax_wrapper_id']['#value'] = $form['#node']->webform['webform_ajax_wrapper_id'];
  }
  else {
    // At last, generate a unique ID.
    $form['webform_ajax_wrapper_id']['#value'] = drupal_html_id('webform-ajax-wrapper-' . $form['#node']->nid);
  }

  $form['#prefix'] = '<div id="' . $form['webform_ajax_wrapper_id']['#value'] . '">' . (isset($form['#prefix']) ? $form['#prefix'] : '');
  $form['#suffix'] = (isset($form['#suffix']) ? $form['#suffix'] : '') . '</div>';

  foreach (array('next', 'submit') as $button) {
    if (isset($form['actions'][$button])) {
      $form['actions'][$button]['#ajax'] = array(
        'callback' => '_universal_webform_js_submit',
        'wrapper' => $form['webform_ajax_wrapper_id']['#value'],
        'method' => 'replace',
        'effect' => 'fade',
      );
      if (in_array($button, array('next', 'submit'))) {
        $form['actions'][$button]['#ajax']['event'] = 'click';
      }
      $submit_id = drupal_html_id('edit-webform-ajax-' . $button . '-18135');
      $form['actions'][$button]['#attributes']['id'] = $submit_id;
      $form['actions'][$button]['#id'] = $submit_id;
    }
  }

  return $form;
}

/**
 * Fills RSVP Form RFI Conditionals.
 *
 * @return array
 *   An array for filling $form['#node']->webform['conditionals'].
 */
function universal_rsvp_rfi_conditionals() {
  $conditionals = array(
    array(
      'nid' => '18135',
      'rgid' => '0',
      'andor' => 'or',
      'weight' => '-1',
      'rules' => array(
        array(
          'nid' => '18135',
          'rgid' => '0',
          'rid' => '0',
          'source_type' => 'component',
          'source' => '10',
          'operator' => 'equal',
          'value' => 'I_already_visited_campus',
        ),
        array(
          'nid' => '18135',
          'rgid' => '0',
          'rid' => '1',
          'source_type' => 'component',
          'source' => '10',
          'operator' => 'equal',
          'value' => 'I_am_not_interested_in_visiting_at_this_time',
        ),
        array(
          'nid' => '18135',
          'rgid' => '0',
          'rid' => '2',
          'source_type' => 'component',
          'source' => '10',
          'operator' => 'equal',
          'value' => 'I_will_visit_at_a_later_date',
        ),
        array(
          'nid' => '18135',
          'rgid' => '0',
          'rid' => '3',
          'source_type' => 'component',
          'source' => '10',
          'operator' => 'empty',
          'value' => 'I_will_visit_at_a_later_date',
        ),
      ),
      'actions' => array(
        array(
          'nid' => '18135',
          'rgid' => '0',
          'aid' => '0',
          'target_type' => 'component',
          'target' => '33',
          'invert' => '1',
          'action' => 'show',
          'argument' => '',
        ),
        array(
          'nid' => '18135',
          'rgid' => '0',
          'aid' => '1',
          'target_type' => 'component',
          'target' => '34',
          'invert' => '1',
          'action' => 'show',
          'argument' => '',
        ),
      ),
    ),
  );
  return $conditionals;
}

/**
 * Handles submission via ajax for the RFI RSVP form.
 *
 * @param array $form
 *   Drupal form structure.
 * @param array $form_state
 *   Drupal form state structure.
 *
 * @return array
 *   Drupal form structure.
 */
function _universal_webform_js_submit($form, &$form_state) {
  if (isset($form_state['page_id'])) {
    $node = node_load($form_state['page_id']);

    $form['submitted']['node_type']['#value'] = $node->type;
    $node_name = $node->title;
    if ($node->type == 'programs') {
      $form = node_programs_values($node, $form, $form_state, $node_name);
    }
    elseif ($node->type == 'calendar') {
      $form = node_calendar_values($node, $form, $form_state, $node_name);
    }

    elseif ($node->type == 'landing_page') {
      $form = node_landing_values($node, $form, $form_state, $node_name);
    }
  }
  // SID here is the submission id from the webform.
  $sid = $form_state['values']['details']['sid'];
  // If we have an sid then we know the form was properly submitted, otherwise,
  // we'll just return the existing $form array.
  if ($sid) {
    $wnode = node_load($form_state['values']['details']['nid']);
    $confirmation = array(
      '#type' => 'markup',
      '#markup' => check_markup($wnode->webform['confirmation'], $wnode->webform['confirmation_format'], '', TRUE),
    );
    return $confirmation;
  }
  else {
    return $form;
  }
}

/**
 * Validates the RFI RSVP form.
 *
 * @param array $form
 *   Drupal form structure.
 * @param array $form_state
 *   Form state structure.
 */
function validate_rsvp(&$form, &$form_state) {
  if (isset($form_state['page_id'])) {
    $node = node_load($form_state['page_id']);
    $node_name = $node->title;
    if ($node->type == 'calendar') {
      if ((isset($form_state['values']['submitted']['event_campus'])) && (empty($form_state['values']['submitted']['event_campus']))) {
        form_error($form['submitted']['event_campus'], "Event Campus is required.");
      }
      if ((isset($form_state['values']['submitted']['number_of_guests'])) && (empty($form_state['values']['submitted']['number_of_guests']))) {
        form_error($form['submitted']['number_of_guests'], "Number of Guests is required.");
      }
      $form = node_calendar_values($node, $form, $form_state, $node_name);
    }
  }
}

/**
 * Generates an RFI RSVP webform for programs nodes.
 *
 * @param object $node
 *   Program node.
 * @param array $form
 *   Form structure.
 * @param array $form_state
 *   Form state structure.
 * @param string $node_name
 *   Name of the program node.
 *
 * @return array
 *   Updated RFI RSVP webform.
 */
function node_programs_values($node, $form, &$form_state, $node_name) {
  global $spl_chars;
  global $repl_chars;

  $form = come_visit_us_value($form, $object = $node);

  if (isset($node->field_degree_type[LANGUAGE_NONE][0]['value'])) {
    $degree_type = $node->field_degree_type[LANGUAGE_NONE][0]['value'];
  }
  else {
    $degree_type = NULL;
  }
  if (isset($node->field_banner_program_name[LANGUAGE_NONE][0]['value'])) {
    $form['submitted']['program_code']['#value'] = $node->field_banner_program_name[LANGUAGE_NONE][0]['value'];
  }
  if (isset($node->field_admission_type[LANGUAGE_NONE][0]['value'])) {
    $form['submitted']['admission_type']['#value'] = $node->field_admission_type[LANGUAGE_NONE][0]['value'];
  }
  if (isset($node->field_admissions_source[LANGUAGE_NONE][0]['value'])) {
    $form['submitted']['adm_source']['#value'] = $node->field_admissions_source[LANGUAGE_NONE][0]['value'];
  }
  hide($form['submitted']['program_of_interest']);
  $locations = array();
  foreach ($node->field_location[LANGUAGE_NONE] as $location) {
    $locations[str_replace(" ", "_", $location['value'])] = $location['value'];
  }
  $form['submitted']['campus']['#options'] = array('' => "- Campus (Required) -") + $locations;
  $form['submitted']['campus']['#default_value'] = '';
  $form['submitted']['campus']['#empty_value'] = '';
  $form['submitted']['campus']['#empty_option'] = "- Campus (Required) -";

  // If program type is undergraduate and student transferring credits is
  // student type of transfer.
  if (isset($node->field_program_type[LANGUAGE_NONE][0]['value'])) {
    $program_type = $node->field_program_type[LANGUAGE_NONE][0]['value'];

    if ($program_type != "Undergraduate") {
      hide($form['submitted']['are_you_transferring_credits_from_another_college']);
    }
    if ($program_type != "Graduate") {
      $form['submitted']['student_type']['#default_value'] = 'Freshman';
    }
    if ($program_type == "Graduate") {
      $form['submitted']['student_type']['#value'] = 'Graduate';
    }
  }
  $form['submitted']['program_title']['#value'] = "$node_name $degree_type";
  return $form;
}

/**
 * Generates an RFI RSVP webform for calendar nodes.
 *
 * @param object $node
 *   Calendar node.
 * @param array $form
 *   Form structure.
 * @param array $form_state
 *   Form state structure.
 * @param string $node_name
 *   Name of the calendar node.
 *
 * @return array
 *   Updated RFI RSVP webform.
 */
function node_calendar_values($node, $form, &$form_state, $node_name) {
  global $spl_chars;
  global $repl_chars;

  unset($form['#node']->webform['conditionals']);

  hide($form['submitted']['come_visit_us']);
  $form = program_of_interest_value($form, $object = $node);
  $form['submitted']['come_visit_us']['#value'] = $node_name;

  $locations = array();
  foreach ($node->field_location[LANGUAGE_NONE] as $location) {
    $locations[str_replace(" ", "_", $location['value'])] = $location['value'];
  }
  $form['submitted']['event_campus']['#options'] = array('' => "- Event Campus (required) -") + $locations;
  $form['submitted']['event_campus']['#empty_value'] = '';
  $form['submitted']['event_campus']['#empty_option'] = "- Event Campus (required) -";

  if (isset($node->field_category[LANGUAGE_NONE][0]['value'])) {
    $form['submitted']['event_category']['#value'] = $node->field_category[LANGUAGE_NONE][0]['value'];
  }
  if (isset($node->field_open_house_sub_category[LANGUAGE_NONE][0]['value'])) {
    $form['submitted']['open_house_sub_category']['#value'] = $node->field_open_house_sub_category[LANGUAGE_NONE][0]['value'];
  }

  if ((isset($node->field_open_house_sub_category[LANGUAGE_NONE][0]['value'])) && ($node->field_open_house_sub_category[LANGUAGE_NONE][0]['value'] === "MBA Open House")) {
    hide($form['submitted']['program_of_interest']);
    $form['submitted']['student_type']['#value'] = 'Graduate';
    hide($form['submitted']['are_you_transferring_credits_from_another_college']);
  }
  if ((isset($node->field_open_house_sub_category[LANGUAGE_NONE][0]['value'])) && ($node->field_open_house_sub_category[LANGUAGE_NONE][0]['value'] === "Program Specific")) {
    hide($form['submitted']['program_of_interest']);
    if (isset($node->field_referenced_programs[LANGUAGE_NONE])) {
      $ref_node = node_load($node->field_referenced_programs[LANGUAGE_NONE][0]['target_id']);
      $program_title = $ref_node->title;
      if (isset($ref_node->field_degree_type[LANGUAGE_NONE][0]['value'])) {
        $program_degree = $ref_node->field_degree_type[LANGUAGE_NONE][0]['value'];
      }
      else {
        $program_degree = NULL;
      }
      $form['submitted']['program_title']['#value'] = $program_title;
      $form['submitted']['program_of_interest']['#value'] = "$program_title $program_degree";

      if (isset($ref_node->field_banner_program_name[LANGUAGE_NONE][0]['value'])) {
        $banner_name = $ref_node->field_banner_program_name[LANGUAGE_NONE][0]['value'];
        $form['submitted']['program_code']['#value'] = $banner_name;
      }
      else {
        $banner_name = NULL;
      }
      $program_type = $ref_node->field_program_type[LANGUAGE_NONE][0]['value'];
      if ($program_type != "Graduate") {
        $form['submitted']['student_type']['#default_value'] = 'Freshman';
      }
      if ($program_type == "Graduate") {
        $form['submitted']['student_type']['#value'] = 'Graduate';
        hide($form['submitted']['are_you_transferring_credits_from_another_college']);
      }
    }
  }

  if (isset($node->field_open_house_sub_category[LANGUAGE_NONE][0]['value'])) {
    if ($node->field_open_house_sub_category[LANGUAGE_NONE][0]['value'] === 'Graduate Open House') {
      $form['submitted']['student_type']['#value'] = 'Graduate';
      hide($form['submitted']['are_you_transferring_credits_from_another_college']);
    }
    elseif ($node->field_open_house_sub_category[LANGUAGE_NONE][0]['value'] === "Undergraduate Open House") {
      $form['submitted']['student_type']['#default_value'] = 'Freshman';
    }
  }
  if (isset($node->field_date_of_event[LANGUAGE_NONE][0]['value'])) {
    $form['submitted']['event_date']['#value'] = $node->field_date_of_event[LANGUAGE_NONE][0]['value'];
  }
  $form['submitted']['event_name']['#value'] = $node_name;

  return $form;
}

/**
 * Generates an RFI RSVP webform for Landing Page nodes.
 *
 * @param object $node
 *   Landing Page node.
 * @param array $form
 *   Form structure.
 * @param array $form_state
 *   Form state structure.
 * @param string $node_name
 *   Name of the Landing Page node.
 *
 * @return array
 *   Updated RFI RSVP webform.
 */
function node_landing_values($node, $form, $form_state, $node_name) {
  global $spl_chars;
  global $repl_chars;
  $ref_node = NULL;
  $form['submitted']['submission_for']['#value'] = $node_name . " Landing Page";
  $form = program_of_interest_value($form, $object = $node);
  $form = come_visit_us_value($form, $object = $node);

  if (($node->field_landing_page_category[LANGUAGE_NONE][0]['value']) === "Program Specific (Must Select a Program)") {
    hide($form['submitted']['program_of_interest']);

    // If the landing page is program specific,
    // grab the open house specific for that program.
    if (isset($node->field_referenced_programs[LANGUAGE_NONE])) {
      $ref_node = node_load($node->field_referenced_programs[LANGUAGE_NONE][0]['target_id']);
      $program_nid = $ref_node->nid;
      $host = $_SERVER['HTTP_HOST'];
      file_get_contents("http://{$host}/node/{$program_nid}");

      if (isset($ref_node->field_open_house_list[LANGUAGE_NONE][0]['value'])) {
        $form = come_visit_us_value($form, $object = $ref_node);
      }
      $program_title = $ref_node->title;
      if (isset($ref_node->field_degree_type[LANGUAGE_NONE][0]['value'])) {
        $program_degree = $ref_node->field_degree_type[LANGUAGE_NONE][0]['value'];
      }
      else {
        $program_degree = NULL;
      }
      $form['submitted']['program_of_interest']['#value'] = "$program_title $program_degree";
      $form['submitted']['program_title']['#value'] = $program_title;
      if (isset($ref_node->field_banner_program_name[LANGUAGE_NONE][0]['value'])) {
        $program_code = $ref_node->field_banner_program_name[LANGUAGE_NONE][0]['value'];
        $form['submitted']['program_code']['#value'] = $program_code;
      }

      $program_type = $ref_node->field_program_type[LANGUAGE_NONE][0]['value'];

      if ($program_type != "Graduate") {
        $form['submitted']['student_type']['#default_value'] = 'Freshman';
      }
      if ($program_type == "Graduate") {
        $form['submitted']['student_type']['#value'] = 'Graduate';
      }
    }
    else {
      $form['submitted']['program_of_interest']['#value'] = "";
    }
  }
  if (($node->field_landing_page_category[LANGUAGE_NONE][0]['value']) === "Graduate") {
    hide($form['submitted']['are_you_transferring_credits_from_another_college']);
    $form['submitted']['student_type']['#value'] = 'Graduate';
  }
  if (($node->field_landing_page_category[LANGUAGE_NONE][0]['value']) === "Undergraduate" || ($node->field_landing_page_category[LANGUAGE_NONE][0]['value']) === "Freshman") {
    $form['submitted']['student_type']['#default_value'] = 'Freshman';
  }
  if (($node->field_landing_page_category[LANGUAGE_NONE][0]['value']) === "Transfer") {
    $form['submitted']['student_type']['#value'] = 'Transfer';
    $form['submitted']['are_you_transferring_credits_from_another_college']['#value'] = "Yes";
    hide($form['submitted']['are_you_transferring_credits_from_another_college']);
  }
  if ((isset($ref_node->field_program_type[LANGUAGE_NONE][0]['value'])) && ($ref_node->field_program_type[LANGUAGE_NONE][0]['value'] === 'Graduate')) {
    hide($form['submitted']['are_you_transferring_credits_from_another_college']);
  }
  return $form;
}

/**
 * Fills in a Come Visit Us entry in the webform.
 *
 * @param array $form
 *   Form structure.
 * @param object $node
 *   Node with an open house list.
 *
 * @return array
 *   Updated form array.
 */
function come_visit_us_value($form, $node) {
  global $spl_chars;
  global $repl_chars;
  if (isset($node->field_open_house_list[LANGUAGE_NONE])) {
    $open_houses = array();
    foreach ($node->field_open_house_list[LANGUAGE_NONE] as $key) {
      $opt_key = str_replace($spl_chars, $repl_chars, $key['value']);
      $open_houses[$opt_key] = $key['value'];
    }
    $form['submitted']['come_visit_us']['#options'] = array('' => "- Come Visit Us -") + $open_houses;
    $form['submitted']['come_visit_us']['#default_value'] = '';
    $form['submitted']['come_visit_us']['#empty_value'] = '';
    $form['submitted']['come_visit_us']['#empty_option'] = "- Come Visit Us -";
  }
  return $form;
}

/**
 * Fills in a Program of Interest entry in the webform.
 *
 * @param array $form
 *   Form structure.
 * @param object $node
 *   Node with program option list.
 *
 * @return array
 *   Updated form array.
 */
function program_of_interest_value($form, $node) {
  global $spl_chars;
  global $repl_chars;
  if (isset($node->field_program_option_list[LANGUAGE_NONE])) {
    foreach ($node->field_program_option_list[LANGUAGE_NONE] as $key) {
      $opt_key = str_replace($spl_chars, $repl_chars, $key['value']);
      $programs[$opt_key] = $key['value'];
    }
    $form['submitted']['program_of_interest']['#options'] = array('' => "- Program of Interest -") + $programs;
    $form['submitted']['program_of_interest']['#default_value'] = '';
    $form['submitted']['program_of_interest']['#empty_value'] = '';
    $form['submitted']['program_of_interest']['#empty_option'] = "- Program of Interest -";
  }
  return $form;
}
