(function($) {

  Drupal.newsroom = {};

  Drupal.behaviors.newsroom_client = {
    attach: function(context, settings) {
      // load the articles
      if (!$('.newsroom-articles-wrapper').hasClass('processed')) {
        Drupal.newsroom.newsroom_articles(context, settings);
      }
      // load the fetaured article
      if (!$('.newsroom-featured-article-wrapper').hasClass('processed')) {
        Drupal.newsroom.newsroom_featured_article(context, settings);
      }
      // load the event ticker
      if (!$('.event-wrapper').hasClass('processed')) {
        Drupal.newsroom.event_ticker(context, settings);
      }
    }
  };

  Drupal.newsroom.newsroom_articles = function(context, settings) {
    if ($('.newsroom-articles:not(.processed)')) {
      $.ajax({
        dataType: "jsonp",
        url: settings.newsroom_blocks.newsroom_articles_api_url + '&callback=drupal',
        data: '',
        success: function(data) {
          var items = [];
          $.each(data, function(key, val) {
            items.push('<li id="node-' + key + '"><div class="img-container">' + val.image + '</div><div class="news-text"><h2 class="title"><a href="/node/' + val.nid + '">' + val.title + '</a></h2><p><span class="date-display-single">' + val.date + '</span><span class="description quote">' + val.body + '</span></p></div></li>');
          });
          // create the div
          $("<ul></ul>", {
            "class": "newsroom-articles flexslider slides",
            html: items.join("")
          }).appendTo('.newsroom-articles-wrapper').addClass('processed');
          // use the slideshow
          $('.newsroom-articles-wrapper').flexslider({
            animation: "fade"
          });
          $('.newsroom-articles-wrapper').addClass('processed');
        }
      });
    }
  }

  Drupal.newsroom.newsroom_featured_article = function(context, settings) {
    if ($('.newsroom-featured-article:not(.processed)')) {
      $.ajax({
        dataType: "jsonp",
        url: settings.newsroom_blocks.newsroom_featured_article_api_url + '?callback=drupal',
        data: '',
        success: function(data) {
          var items = [];
          $.each(data, function(key, val) {
            items.push("<img src='" + val.image + "' /><div class='news-text'><h2 class='title'>" + val.title + "</h2><p><span class='date'>" + val.date + "</span><a href='/node/" + val.nid + "' class='description'>" + val.body + "</a></p></div>");
          });
          // create the div
          $("<div></div>", {
            "class": "newsroom-featured-article",
            html: items.join("")
          }).appendTo('.newsroom-featured-article-wrapper').addClass('processed');
          $('.newsroom-featured-article-wrapper').addClass('processed');
        }
      });
    }
  }

  Drupal.newsroom.event_ticker = function(context, settings) {
    if ($('.event-wrapper:not(.processed)')) {
      $.ajax({
        dataType: "jsonp",
        url: settings.newsroom_blocks.event_ticker_api_url + '&callback=drupal',
        data: '',
        success: function(data) {
          var items = [];
          $.each(data, function(key, val) {
            // items.push('<li id="node-' + key + '"><div class="img-container">' + val.image + '</div><div class="news-text"><h2 class="title"><a href="/node/' + val.nid + '">' + val.title + '</a></h2><p><span class="date-display-single">' + val.date + '</span><span class="description quote">' + val.body + '</span></p></div></li>');
            items.push('<li class="views-row event"><div class="event-date"><span class="day">' + val.day + '</span><span class="month">' + val.month + '</span></div><a href="/node/' + val.nid + '">' + val.node_title + '</a></li>');
          });
          // create the div
          $("<ul></ul>", {
            "class": "events-ticker events",
            html: items.join("")
          }).appendTo('.event-wrapper').addClass('processed');
          $('.event-wrapper').addClass('processed');
          // run the owl careousel
          var owl = $(".events-ticker");
          owl.owlCarousel({
            items: 5, //5 items above 1000px browser width
            pagination: false,
            autoplay: true,
            stopOnHover: true,
            addClassActive: true,
            afterAction: setFirstLast
          });

          $(".customNavigation .btn.next").click(function() {
            owl.trigger('owl.next');
          })
          $(".customNavigation .btn.prev").click(function() {
            owl.trigger('owl.prev');
          })

          function setFirstLast() {
            $('.owl-item.active').removeClass('first-visible');
            $('.owl-item.active').removeClass('last-visible');
            $('.owl-item.active').first().addClass('first-visible');
            $('.owl-item.active').last().addClass('last-visible');
          }
        }
      });
    }
  }

})(jQuery);
