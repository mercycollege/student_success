<?php
/**
 * @file
 * newsroom_blocks.features.fe_block_boxes.inc
 */

/**
 * Implements hook_default_fe_block_boxes().
 */
function newsroom_blocks_default_fe_block_boxes() {
  $export = array();

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Newsroom Carousel';
  $fe_block_boxes->format = 'admin_html';
  $fe_block_boxes->machine_name = 'newsroom_carousel';
  $fe_block_boxes->body = '<div class="news-slider"><div id="news-slide" class="newsroom-articles-wrapper flexslider"></div></div>';

  $export['newsroom_carousel'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Newsroom Featured Article';
  $fe_block_boxes->format = 'admin_html';
  $fe_block_boxes->machine_name = 'nr_featured_article';
  $fe_block_boxes->body = '<div class="newsroom-featured-article-wrapper blk-single-news"></div>';

  $export['nr_featured_article'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Event Ticker Universal';
  $fe_block_boxes->format = 'admin_html';
  $fe_block_boxes->machine_name = 'event_ticker';
  $fe_block_boxes->body = '<div class="view view-events-subsections view-id-events_subsections view-display-id-event_horizon event-ticker container"><div class="ticker-overlay">Upcoming Events</div><div class="item-list event-wrapper"></div><div class="customNavigation"><a class="btn prev">Previous</a><a class="btn next">Next</a></div></div>';

  $export['event_ticker'] = $fe_block_boxes;


  return $export;
}
