<?php
/**
 * @file
 * news_section.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function news_section_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_image_separator|node|news_sections_articles|rss';
  $field_group->group_name = 'group_image_separator';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'news_sections_articles';
  $field_group->mode = 'rss';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Image Separator',
    'weight' => '7',
    'children' => array(
      0 => 'field_image',
    ),
    'format_type' => 'html-element',
    'format_settings' => array(
      'label' => 'Image Separator',
      'instance_settings' => array(
        'classes' => '',
        'element' => '<br>',
        'show_label' => '0',
        'label_element' => '',
        'attributes' => '',
        'required_fields' => 1,
      ),
      'formatter' => '',
    ),
  );
  $export['group_image_separator|node|news_sections_articles|rss'] = $field_group;

  return $export;
}
