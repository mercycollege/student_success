<?php
/**
 * @file
 * news_section.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function news_section_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'page_newsroom';
  $page->task = 'page';
  $page->admin_title = 'page-newsroom';
  $page->admin_description = '';
  $page->path = 'newsroom';
  $page->access = array();
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_page_newsroom_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'page_newsroom';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'three_four_adaptive';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      11 => NULL,
      12 => NULL,
      13 => NULL,
      21 => NULL,
      22 => NULL,
      23 => NULL,
      31 => NULL,
      32 => NULL,
      33 => NULL,
      41 => NULL,
      42 => NULL,
      43 => NULL,
      44 => NULL,
      51 => NULL,
      52 => NULL,
      53 => NULL,
      54 => NULL,
      61 => NULL,
      62 => NULL,
      63 => NULL,
      64 => NULL,
      71 => NULL,
      72 => NULL,
      73 => NULL,
      74 => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = 'a8e58637-287f-4e88-8fda-4fcfdc3e6856';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-76c0f6d8-e3f2-49ca-9774-642df317fe0a';
    $pane->panel = '11';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => '',
      'title' => '',
      'body' => 'All categories 
Mercy College in the news 
Athletics and recreation 
College news 
Mercy Online
School of Business
School of Education
School of Health and Natural Sciences
School of Liberal Arts
School of Social and Behavioral Sciences',
      'format' => 'admin_html',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '76c0f6d8-e3f2-49ca-9774-642df317fe0a';
    $display->content['new-76c0f6d8-e3f2-49ca-9774-642df317fe0a'] = $pane;
    $display->panels['11'][0] = 'new-76c0f6d8-e3f2-49ca-9774-642df317fe0a';
    $pane = new stdClass();
    $pane->pid = 'new-fb70a956-3164-40a3-99ec-139f88036ba9';
    $pane->panel = '12';
    $pane->type = 'views';
    $pane->subtype = 'featured_news';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 1,
      'nodes_per_page' => '5',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'default',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'fb70a956-3164-40a3-99ec-139f88036ba9';
    $display->content['new-fb70a956-3164-40a3-99ec-139f88036ba9'] = $pane;
    $display->panels['12'][0] = 'new-fb70a956-3164-40a3-99ec-139f88036ba9';
    $pane = new stdClass();
    $pane->pid = 'new-436461ef-12cf-4691-b41c-003b213b7ff2';
    $pane->panel = '21';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => '',
      'title' => 'Media inquires',
      'body' => 'Contact PR',
      'format' => 'admin_html',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => 'contact_us',
      'css_class' => 'contact_us',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '436461ef-12cf-4691-b41c-003b213b7ff2';
    $display->content['new-436461ef-12cf-4691-b41c-003b213b7ff2'] = $pane;
    $display->panels['21'][0] = 'new-436461ef-12cf-4691-b41c-003b213b7ff2';
    $pane = new stdClass();
    $pane->pid = 'new-4a4e97c9-935a-45d9-a83b-b79ade693e64';
    $pane->panel = '22';
    $pane->type = 'views';
    $pane->subtype = 'news_section';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 1,
      'nodes_per_page' => '5',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'default',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '4a4e97c9-935a-45d9-a83b-b79ade693e64';
    $display->content['new-4a4e97c9-935a-45d9-a83b-b79ade693e64'] = $pane;
    $display->panels['22'][0] = 'new-4a4e97c9-935a-45d9-a83b-b79ade693e64';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['page_newsroom'] = $page;

  return $pages;

}
