<?php
/**
 * @file
 * news_section.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function news_section_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['views--exp-news_section-page_1'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => '-exp-news_section-page_1',
    'icon' => 'a:3:{s:6:"bundle";s:9:"bootstrap";s:4:"icon";s:16:"glyphicon-search";s:8:"position";s:13:"content_after";}',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'mercy_bootstrap_subtheme' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'mercy_bootstrap_subtheme',
        'weight' => 2,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['views-featured_news-block'] = array(
    'cache' => -1,
    'css_class' => 'col-sm-9 pull-right',
    'custom' => 0,
    'delta' => 'featured_news-block',
    'icon' => 'a:2:{s:4:"icon";s:0:"";s:8:"position";s:12:"title_before";}',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'newsroom_test_part1',
    'roles' => array(),
    'themes' => array(
      'mercy_bootstrap_subtheme' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'mercy_bootstrap_subtheme',
        'weight' => 1,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['views-news_section-block'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'news_section-block',
    'icon' => NULL,
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'mercy_bootstrap_subtheme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'mercy_bootstrap_subtheme',
        'weight' => 98,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-news_section-block_1'] = array(
    'cache' => -1,
    'css_class' => 'newsmenu',
    'custom' => 0,
    'delta' => 'news_section-block_1',
    'icon' => 'a:2:{s:4:"icon";s:0:"";s:8:"position";s:12:"title_before";}',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'newsroom
newsroom/*',
    'roles' => array(),
    'themes' => array(
      'mercy_bootstrap_subtheme' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'mercy_bootstrap_subtheme',
        'weight' => 1,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  return $export;
}
