<?php
/**
 * @file
 * other_exportable_content_kits.features.node_export_features.inc
 */

/**
 * Implements hook_node_export_features_default().
 */
function other_exportable_content_kits_node_export_features_default() {
  $node_export = array(
  'code_string' => 'array(
  (object) array(
      \'vid\' => \'227\',
      \'uid\' => \'5\',
      \'title\' => \'test\',
      \'log\' => \'\',
      \'status\' => \'0\',
      \'comment\' => \'1\',
      \'promote\' => \'0\',
      \'sticky\' => \'0\',
      \'vuuid\' => \'b8afdd0f-704f-4bd2-848c-8e12f1881c3d\',
      \'nid\' => \'227\',
      \'type\' => \'news_articles\',
      \'language\' => \'en\',
      \'created\' => \'1400177786\',
      \'changed\' => \'1400177786\',
      \'tnid\' => \'0\',
      \'translate\' => \'0\',
      \'uuid\' => \'2aa1c19c-ae16-409c-8398-56d348b866b7\',
      \'revision_timestamp\' => \'1400177786\',
      \'revision_uid\' => \'5\',
      \'rdf_mapping\' => array(
        \'rdftype\' => array(
          \'sioc:Item\',
          \'foaf:Document\',
        ),
        \'title\' => array(
          \'predicates\' => array(
            \'dc:title\',
          ),
        ),
        \'created\' => array(
          \'predicates\' => array(
            \'dc:date\',
            \'dc:created\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
        \'changed\' => array(
          \'predicates\' => array(
            \'dc:modified\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
        \'body\' => array(
          \'predicates\' => array(
            \'content:encoded\',
          ),
        ),
        \'uid\' => array(
          \'predicates\' => array(
            \'sioc:has_creator\',
          ),
          \'type\' => \'rel\',
        ),
        \'name\' => array(
          \'predicates\' => array(
            \'foaf:name\',
          ),
        ),
        \'comment_count\' => array(
          \'predicates\' => array(
            \'sioc:num_replies\',
          ),
          \'datatype\' => \'xsd:integer\',
        ),
        \'last_activity\' => array(
          \'predicates\' => array(
            \'sioc:last_activity_date\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
      ),
      \'cid\' => \'0\',
      \'last_comment_timestamp\' => \'1400177786\',
      \'last_comment_name\' => NULL,
      \'last_comment_uid\' => \'5\',
      \'comment_count\' => \'0\',
      \'name\' => \'jgarcia\',
      \'picture\' => \'0\',
      \'data\' => \'a:3:{s:19:"ldap_authentication";a:1:{s:4:"init";a:3:{s:3:"sid";s:12:"mercydfdc001";s:2:"dn";s:67:"CN=axiomrunner,OU=VPN Users,OU=Domain Users,DC=mercy,DC=local";s:4:"mail";s:17:"webmaster@mercy.edu";}}s:7:"overlay";i:1;s:7:"contact";i:1;}\',
      \'workbench_access\' => array(
        \'admissions\' => \'admissions\',
      ),
      \'path\' => array(
        \'pid\' => \'247\',
        \'source\' => \'node/227\',
        \'alias\' => \'test\',
        \'language\' => \'en\',
      ),
      \'workbench_moderation\' => array(
        \'current\' => (object) array(
            \'hid\' => NULL,
            \'vid\' => \'227\',
            \'nid\' => \'227\',
            \'from_state\' => \'draft\',
            \'state\' => \'draft\',
            \'uid\' => \'5\',
            \'stamp\' => \'1400177786\',
            \'published\' => \'0\',
            \'current\' => 1,
            \'title\' => \'test\',
            \'timestamp\' => \'1400177786\',
          ),
        \'my_revision\' => \'' . "\0" . 'recursion_export_node_74\',
      ),
      \'menu\' => NULL,
      \'node_export_drupal_version\' => \'7\',
    ),
)',
);
  return $node_export;
}
