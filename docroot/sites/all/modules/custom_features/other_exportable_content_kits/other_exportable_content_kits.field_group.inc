<?php
/**
 * @file
 * other_exportable_content_kits.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function other_exportable_content_kits_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_hidden_title7|fieldable_panels_pane|pdf_uploader|form';
  $field_group->group_name = 'group_hidden_title7';
  $field_group->entity_type = 'fieldable_panels_pane';
  $field_group->bundle = 'pdf_uploader';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Hidden Title',
    'weight' => '0',
    'children' => array(
      0 => 'title',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Hidden Title',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => 'fieldable_panels_pane_pdf_uploader_form_group_hidden_title7',
        'classes' => 'group-hidden-title',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_hidden_title7|fieldable_panels_pane|pdf_uploader|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_hidden_title8|fieldable_panels_pane|blue_seal_title|form';
  $field_group->group_name = 'group_hidden_title8';
  $field_group->entity_type = 'fieldable_panels_pane';
  $field_group->bundle = 'blue_seal_title';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Hidden Title',
    'weight' => '0',
    'children' => array(
      0 => 'title',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Hidden Title',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => 'fieldable_panels_pane_blue_seal_title_form_group_hidden_title8',
        'classes' => 'group-hidden-title',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_hidden_title8|fieldable_panels_pane|blue_seal_title|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_hidden_title8|fieldable_panels_pane|vertical_video_content_kit|form';
  $field_group->group_name = 'group_hidden_title8';
  $field_group->entity_type = 'fieldable_panels_pane';
  $field_group->bundle = 'vertical_video_content_kit';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Hidden Title',
    'weight' => '0',
    'children' => array(
      0 => 'title',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Hidden Title',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => 'fieldable_panels_pane_vertical_video_content_kit_form_group_hidden_title8',
        'classes' => 'group-hidden-title',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_hidden_title8|fieldable_panels_pane|vertical_video_content_kit|form'] = $field_group;

  return $export;
}
