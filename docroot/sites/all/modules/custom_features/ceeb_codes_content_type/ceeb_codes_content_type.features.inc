<?php
/**
 * @file
 * ceeb_codes_content_type.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ceeb_codes_content_type_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function ceeb_codes_content_type_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function ceeb_codes_content_type_node_info() {
  $items = array(
    'schools_ceeb_data' => array(
      'name' => t('Schools CEEB Data'),
      'base' => 'node_content',
      'description' => t('High School and College Data'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
