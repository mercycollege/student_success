<?php
/**
 * @file
 * ceeb_codes_content_type.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function ceeb_codes_content_type_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'schools_data';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'CEEB Codes';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'School CEEB Code Lookup';
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'input_required';
  $handler->display->display_options['exposed_form']['options']['expose_sort_order'] = FALSE;
  $handler->display->display_options['exposed_form']['options']['text_input_required'] = 'Use any search field to find your school, then click Apply.';
  $handler->display->display_options['exposed_form']['options']['text_input_required_format'] = 'admin_html';
  $handler->display->display_options['pager']['type'] = 'mini';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'title' => 'title',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'title' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Field: Content: CEEB Code */
  $handler->display->display_options['fields']['field_ceeb_code']['id'] = 'field_ceeb_code';
  $handler->display->display_options['fields']['field_ceeb_code']['table'] = 'field_data_field_ceeb_code';
  $handler->display->display_options['fields']['field_ceeb_code']['field'] = 'field_ceeb_code';
  $handler->display->display_options['fields']['field_ceeb_code']['label'] = 'Code';
  $handler->display->display_options['fields']['field_ceeb_code']['element_class'] = 'hscode';
  $handler->display->display_options['fields']['field_ceeb_code']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_ceeb_code']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_ceeb_code']['settings'] = array(
    'empty_fields_handler' => '',
    'empty_fields_emptyfieldtext_empty_text' => '',
    'field_formatter_class' => '',
    'conditions' => array(
      0 => array(
        'condition' => '',
      ),
    ),
    'linked_field' => array(
      'linked' => 0,
      'destination' => '',
      'advanced' => array(
        'title' => '',
        'target' => '',
        'class' => '',
        'rel' => '',
        'text' => '',
      ),
    ),
  );
  /* Field: Global: PHP */
  $handler->display->display_options['fields']['php']['id'] = 'php';
  $handler->display->display_options['fields']['php']['table'] = 'views';
  $handler->display->display_options['fields']['php']['field'] = 'php';
  $handler->display->display_options['fields']['php']['label'] = '';
  $handler->display->display_options['fields']['php']['element_class'] = 'hsname';
  $handler->display->display_options['fields']['php']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['php']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['php']['use_php_setup'] = 0;
  $handler->display->display_options['fields']['php']['php_output'] = '<?php

if (isset($data->field_field_ceeb_code)) {
	$ceeb = ($data->field_field_ceeb_code[0][\'raw\'][\'value\']); 
}
else {

}
if (isset($data->field_field_school_name)) {
	$name = ($data->field_field_school_name[0][\'raw\'][\'value\']); 
}
else {
	$name = NULL;
}
if (isset($data->field_field_school_address1)) {
	$address = ($data->field_field_school_address1[0][\'raw\'][\'value\']); 
}
else {
	$address = NULL;
}
if (isset($data->field_field_school_address2)) {
	$address2 = ($data->field_field_school_address2[0][\'raw\'][\'value\']); 
}
else {
	$address2 = NULL;
}
if (isset($data->field_field_school_city)) {
	$city = ($data->field_field_school_city[0][\'raw\'][\'value\']); 
}
else {
	$city = NULL;
}
if (isset($data->field_field_school_state)) {
	$state = ($data->field_field_school_state[0][\'raw\'][\'value\']); 
}
else {
	$state = NULL;
}
if (isset($data->field_field_school_zip_code)) {
	$zip = ($data->field_field_school_zip_code[0][\'raw\'][\'value\']); 
}
else {
	$zip = NULL;
}

if (!empty($_GET[\'app\'])) {
	$app = $_GET[\'app\'];
}
else {
	($app=NULL);
}
if (!empty($_GET[\'schooltype\'])) {
	$school_type = $_GET[\'schooltype\'];
} 
else {
	($school_type=NULL);
}

if (!empty($_GET[\'pop\'])) {
	$pop = $_GET[\'pop\'];
} 
else {
	($pop=NULL);
}

$vals = "\'$ceeb\',\'$school_type\',\'$app\',\'$name\',\'$address\',\'$address2\',\'$city\',\'$state\',\'$zip\',\'$pop\'";
$js = \'"GetValues\' . "($vals)" . \';return false;"\';
$result = \'<a href="#"\' . \'onclick=\' . "$js" . \'>\' . "$name" . \'</a>\';
print $result;
?>
';
  $handler->display->display_options['fields']['php']['use_php_click_sortable'] = '0';
  $handler->display->display_options['fields']['php']['php_click_sortable'] = '';
  /* Field: Content: School Name */
  $handler->display->display_options['fields']['field_school_name']['id'] = 'field_school_name';
  $handler->display->display_options['fields']['field_school_name']['table'] = 'field_data_field_school_name';
  $handler->display->display_options['fields']['field_school_name']['field'] = 'field_school_name';
  $handler->display->display_options['fields']['field_school_name']['label'] = 'Name';
  $handler->display->display_options['fields']['field_school_name']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_school_name']['element_class'] = 'hsname1';
  $handler->display->display_options['fields']['field_school_name']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_school_name']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_school_name']['settings'] = array(
    'empty_fields_handler' => '',
    'empty_fields_emptyfieldtext_empty_text' => '',
    'field_formatter_class' => '',
    'conditions' => array(
      0 => array(
        'condition' => '',
      ),
    ),
    'linked_field' => array(
      'linked' => 0,
      'destination' => '',
      'advanced' => array(
        'title' => '',
        'target' => '',
        'class' => '',
        'rel' => '',
        'text' => '',
      ),
    ),
  );
  /* Field: Content: School Address1 */
  $handler->display->display_options['fields']['field_school_address1']['id'] = 'field_school_address1';
  $handler->display->display_options['fields']['field_school_address1']['table'] = 'field_data_field_school_address1';
  $handler->display->display_options['fields']['field_school_address1']['field'] = 'field_school_address1';
  $handler->display->display_options['fields']['field_school_address1']['label'] = 'Address';
  $handler->display->display_options['fields']['field_school_address1']['element_class'] = 'hsaddress1';
  $handler->display->display_options['fields']['field_school_address1']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_school_address1']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_school_address1']['settings'] = array(
    'empty_fields_handler' => '',
    'empty_fields_emptyfieldtext_empty_text' => '',
    'field_formatter_class' => '',
    'conditions' => array(
      0 => array(
        'condition' => '',
      ),
    ),
    'linked_field' => array(
      'linked' => 0,
      'destination' => '',
      'advanced' => array(
        'title' => '',
        'target' => '',
        'class' => '',
        'rel' => '',
        'text' => '',
      ),
    ),
  );
  /* Field: Content: School Address2 */
  $handler->display->display_options['fields']['field_school_address2']['id'] = 'field_school_address2';
  $handler->display->display_options['fields']['field_school_address2']['table'] = 'field_data_field_school_address2';
  $handler->display->display_options['fields']['field_school_address2']['field'] = 'field_school_address2';
  $handler->display->display_options['fields']['field_school_address2']['label'] = 'Address(cont.)';
  $handler->display->display_options['fields']['field_school_address2']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_school_address2']['element_class'] = 'hsaddress2';
  $handler->display->display_options['fields']['field_school_address2']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_school_address2']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_school_address2']['settings'] = array(
    'empty_fields_handler' => '',
    'empty_fields_emptyfieldtext_empty_text' => '',
    'field_formatter_class' => '',
    'conditions' => array(
      0 => array(
        'condition' => '',
      ),
    ),
    'linked_field' => array(
      'linked' => 0,
      'destination' => '',
      'advanced' => array(
        'title' => '',
        'target' => '',
        'class' => '',
        'rel' => '',
        'text' => '',
      ),
    ),
  );
  /* Field: Content: School City */
  $handler->display->display_options['fields']['field_school_city']['id'] = 'field_school_city';
  $handler->display->display_options['fields']['field_school_city']['table'] = 'field_data_field_school_city';
  $handler->display->display_options['fields']['field_school_city']['field'] = 'field_school_city';
  $handler->display->display_options['fields']['field_school_city']['label'] = 'City';
  $handler->display->display_options['fields']['field_school_city']['element_class'] = 'hscity';
  $handler->display->display_options['fields']['field_school_city']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_school_city']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_school_city']['settings'] = array(
    'empty_fields_handler' => '',
    'empty_fields_emptyfieldtext_empty_text' => '',
    'field_formatter_class' => '',
    'conditions' => array(
      0 => array(
        'condition' => '',
      ),
    ),
    'linked_field' => array(
      'linked' => 0,
      'destination' => '',
      'advanced' => array(
        'title' => '',
        'target' => '',
        'class' => '',
        'rel' => '',
        'text' => '',
      ),
    ),
  );
  /* Field: Content: School State */
  $handler->display->display_options['fields']['field_school_state']['id'] = 'field_school_state';
  $handler->display->display_options['fields']['field_school_state']['table'] = 'field_data_field_school_state';
  $handler->display->display_options['fields']['field_school_state']['field'] = 'field_school_state';
  $handler->display->display_options['fields']['field_school_state']['label'] = 'State';
  $handler->display->display_options['fields']['field_school_state']['element_type'] = '0';
  $handler->display->display_options['fields']['field_school_state']['element_class'] = 'hsstate';
  $handler->display->display_options['fields']['field_school_state']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_school_state']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_school_state']['type'] = 'text_default';
  $handler->display->display_options['fields']['field_school_state']['settings'] = array(
    'empty_fields_handler' => '',
    'empty_fields_emptyfieldtext_empty_text' => '',
    'field_formatter_class' => '',
    'conditions' => array(
      0 => array(
        'condition' => '',
      ),
    ),
    'linked_field' => array(
      'linked' => 0,
      'destination' => '',
      'advanced' => array(
        'title' => '',
        'target' => '',
        'class' => '',
        'rel' => '',
        'text' => '',
      ),
    ),
  );
  /* Field: Content: School Zip Code */
  $handler->display->display_options['fields']['field_school_zip_code']['id'] = 'field_school_zip_code';
  $handler->display->display_options['fields']['field_school_zip_code']['table'] = 'field_data_field_school_zip_code';
  $handler->display->display_options['fields']['field_school_zip_code']['field'] = 'field_school_zip_code';
  $handler->display->display_options['fields']['field_school_zip_code']['label'] = 'Zip';
  $handler->display->display_options['fields']['field_school_zip_code']['element_class'] = 'hszip';
  $handler->display->display_options['fields']['field_school_zip_code']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_school_zip_code']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_school_zip_code']['settings'] = array(
    'empty_fields_handler' => '',
    'empty_fields_emptyfieldtext_empty_text' => '',
    'field_formatter_class' => '',
    'conditions' => array(
      0 => array(
        'condition' => '',
      ),
    ),
    'linked_field' => array(
      'linked' => 0,
      'destination' => '',
      'advanced' => array(
        'title' => '',
        'target' => '',
        'class' => '',
        'rel' => '',
        'text' => '',
      ),
    ),
  );
  /* Sort criterion: Content: Title */
  $handler->display->display_options['sorts']['title']['id'] = 'title';
  $handler->display->display_options['sorts']['title']['table'] = 'node';
  $handler->display->display_options['sorts']['title']['field'] = 'title';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: School Name (field_school_name) */
  $handler->display->display_options['filters']['field_school_name_value']['id'] = 'field_school_name_value';
  $handler->display->display_options['filters']['field_school_name_value']['table'] = 'field_data_field_school_name';
  $handler->display->display_options['filters']['field_school_name_value']['field'] = 'field_school_name_value';
  $handler->display->display_options['filters']['field_school_name_value']['operator'] = 'allwords';
  $handler->display->display_options['filters']['field_school_name_value']['group'] = 1;
  $handler->display->display_options['filters']['field_school_name_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_school_name_value']['expose']['operator_id'] = 'field_school_name_value_op';
  $handler->display->display_options['filters']['field_school_name_value']['expose']['label'] = 'School Name';
  $handler->display->display_options['filters']['field_school_name_value']['expose']['operator'] = 'field_school_name_value_op';
  $handler->display->display_options['filters']['field_school_name_value']['expose']['identifier'] = 'field_school_name_value';
  $handler->display->display_options['filters']['field_school_name_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    27 => 0,
    5 => 0,
    7 => 0,
    21 => 0,
    22 => 0,
    23 => 0,
    24 => 0,
    26 => 0,
    28 => 0,
    29 => 0,
    30 => 0,
    31 => 0,
    32 => 0,
    33 => 0,
    34 => 0,
    35 => 0,
    36 => 0,
    37 => 0,
    38 => 0,
    39 => 0,
    40 => 0,
    41 => 0,
    42 => 0,
    43 => 0,
    44 => 0,
    45 => 0,
    46 => 0,
    47 => 0,
    48 => 0,
    49 => 0,
    50 => 0,
    51 => 0,
    52 => 0,
    53 => 0,
  );
  $handler->display->display_options['filters']['field_school_name_value']['expose']['autocomplete_filter'] = 1;
  $handler->display->display_options['filters']['field_school_name_value']['expose']['autocomplete_items'] = '10';
  $handler->display->display_options['filters']['field_school_name_value']['expose']['autocomplete_min_chars'] = '0';
  $handler->display->display_options['filters']['field_school_name_value']['expose']['autocomplete_field'] = 'field_school_name';
  $handler->display->display_options['filters']['field_school_name_value']['expose']['autocomplete_raw_suggestion'] = 1;
  $handler->display->display_options['filters']['field_school_name_value']['expose']['autocomplete_raw_dropdown'] = 1;
  $handler->display->display_options['filters']['field_school_name_value']['expose']['autocomplete_dependent'] = 0;
  /* Filter criterion: Content: School Address1 (field_school_address1) */
  $handler->display->display_options['filters']['field_school_address1_value']['id'] = 'field_school_address1_value';
  $handler->display->display_options['filters']['field_school_address1_value']['table'] = 'field_data_field_school_address1';
  $handler->display->display_options['filters']['field_school_address1_value']['field'] = 'field_school_address1_value';
  $handler->display->display_options['filters']['field_school_address1_value']['operator'] = 'contains';
  $handler->display->display_options['filters']['field_school_address1_value']['group'] = 1;
  $handler->display->display_options['filters']['field_school_address1_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_school_address1_value']['expose']['operator_id'] = 'field_school_address1_value_op';
  $handler->display->display_options['filters']['field_school_address1_value']['expose']['label'] = 'Address';
  $handler->display->display_options['filters']['field_school_address1_value']['expose']['operator'] = 'field_school_address1_value_op';
  $handler->display->display_options['filters']['field_school_address1_value']['expose']['identifier'] = 'field_school_address1_value';
  $handler->display->display_options['filters']['field_school_address1_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    27 => 0,
    5 => 0,
    7 => 0,
    21 => 0,
    22 => 0,
    23 => 0,
    24 => 0,
    26 => 0,
    28 => 0,
    29 => 0,
    30 => 0,
    31 => 0,
    32 => 0,
    33 => 0,
    34 => 0,
    35 => 0,
    36 => 0,
    37 => 0,
    38 => 0,
    39 => 0,
    40 => 0,
    41 => 0,
    42 => 0,
    43 => 0,
    44 => 0,
    45 => 0,
    46 => 0,
    47 => 0,
    48 => 0,
    49 => 0,
    50 => 0,
    51 => 0,
    52 => 0,
    53 => 0,
  );
  $handler->display->display_options['filters']['field_school_address1_value']['expose']['autocomplete_filter'] = 1;
  $handler->display->display_options['filters']['field_school_address1_value']['expose']['autocomplete_items'] = '10';
  $handler->display->display_options['filters']['field_school_address1_value']['expose']['autocomplete_min_chars'] = '0';
  $handler->display->display_options['filters']['field_school_address1_value']['expose']['autocomplete_field'] = 'field_school_address1';
  $handler->display->display_options['filters']['field_school_address1_value']['expose']['autocomplete_raw_suggestion'] = 1;
  $handler->display->display_options['filters']['field_school_address1_value']['expose']['autocomplete_raw_dropdown'] = 1;
  $handler->display->display_options['filters']['field_school_address1_value']['expose']['autocomplete_dependent'] = 0;
  /* Filter criterion: Content: School City (field_school_city) */
  $handler->display->display_options['filters']['field_school_city_value']['id'] = 'field_school_city_value';
  $handler->display->display_options['filters']['field_school_city_value']['table'] = 'field_data_field_school_city';
  $handler->display->display_options['filters']['field_school_city_value']['field'] = 'field_school_city_value';
  $handler->display->display_options['filters']['field_school_city_value']['operator'] = 'contains';
  $handler->display->display_options['filters']['field_school_city_value']['group'] = 1;
  $handler->display->display_options['filters']['field_school_city_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_school_city_value']['expose']['operator_id'] = 'field_school_city_value_op';
  $handler->display->display_options['filters']['field_school_city_value']['expose']['label'] = 'City';
  $handler->display->display_options['filters']['field_school_city_value']['expose']['operator'] = 'field_school_city_value_op';
  $handler->display->display_options['filters']['field_school_city_value']['expose']['identifier'] = 'field_school_city_value';
  $handler->display->display_options['filters']['field_school_city_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    27 => 0,
    5 => 0,
    7 => 0,
    21 => 0,
    22 => 0,
    23 => 0,
    24 => 0,
    26 => 0,
    28 => 0,
    29 => 0,
    30 => 0,
    31 => 0,
    32 => 0,
    33 => 0,
    34 => 0,
    35 => 0,
    36 => 0,
    37 => 0,
    38 => 0,
    39 => 0,
    40 => 0,
    41 => 0,
    42 => 0,
    43 => 0,
    44 => 0,
    45 => 0,
    46 => 0,
    47 => 0,
    48 => 0,
    49 => 0,
    50 => 0,
    51 => 0,
    52 => 0,
    53 => 0,
  );
  $handler->display->display_options['filters']['field_school_city_value']['expose']['autocomplete_filter'] = 1;
  $handler->display->display_options['filters']['field_school_city_value']['expose']['autocomplete_items'] = '10';
  $handler->display->display_options['filters']['field_school_city_value']['expose']['autocomplete_min_chars'] = '0';
  $handler->display->display_options['filters']['field_school_city_value']['expose']['autocomplete_field'] = 'field_school_city';
  $handler->display->display_options['filters']['field_school_city_value']['expose']['autocomplete_raw_suggestion'] = 1;
  $handler->display->display_options['filters']['field_school_city_value']['expose']['autocomplete_raw_dropdown'] = 1;
  $handler->display->display_options['filters']['field_school_city_value']['expose']['autocomplete_dependent'] = 0;
  /* Filter criterion: Content: School State (field_school_state) */
  $handler->display->display_options['filters']['field_school_state_value']['id'] = 'field_school_state_value';
  $handler->display->display_options['filters']['field_school_state_value']['table'] = 'field_data_field_school_state';
  $handler->display->display_options['filters']['field_school_state_value']['field'] = 'field_school_state_value';
  $handler->display->display_options['filters']['field_school_state_value']['group'] = 1;
  $handler->display->display_options['filters']['field_school_state_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_school_state_value']['expose']['operator_id'] = 'field_school_state_value_op';
  $handler->display->display_options['filters']['field_school_state_value']['expose']['label'] = 'State';
  $handler->display->display_options['filters']['field_school_state_value']['expose']['operator'] = 'field_school_state_value_op';
  $handler->display->display_options['filters']['field_school_state_value']['expose']['identifier'] = 'field_school_state_value';
  $handler->display->display_options['filters']['field_school_state_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    27 => 0,
    5 => 0,
    7 => 0,
    21 => 0,
    22 => 0,
    23 => 0,
    24 => 0,
    26 => 0,
    28 => 0,
    29 => 0,
    30 => 0,
    31 => 0,
    32 => 0,
    33 => 0,
    34 => 0,
    35 => 0,
    36 => 0,
    37 => 0,
    38 => 0,
    39 => 0,
    40 => 0,
    41 => 0,
    42 => 0,
    43 => 0,
    44 => 0,
    45 => 0,
    46 => 0,
    47 => 0,
    48 => 0,
    49 => 0,
    50 => 0,
    51 => 0,
    52 => 0,
    53 => 0,
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->display->display_options['path'] = 'school-lookup';
  $export['schools_data'] = $view;

  return $export;
}
