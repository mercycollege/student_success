<?php
/**
 * @file
 * landing_page_module.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function landing_page_module_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_lp_section_image|field_collection_item|field_landing_page_section|default';
  $field_group->group_name = 'group_lp_section_image';
  $field_group->entity_type = 'field_collection_item';
  $field_group->bundle = 'field_landing_page_section';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_section_class';
  $field_group->data = array(
    'label' => 'Section Image Class',
    'weight' => '6',
    'children' => array(
      0 => 'field_lp_section_images',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Section Image Class',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-lp-section-image',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_lp_section_image|field_collection_item|field_landing_page_section|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_lp_section_second_body|field_collection_item|field_landing_page_section|default';
  $field_group->group_name = 'group_lp_section_second_body';
  $field_group->entity_type = 'field_collection_item';
  $field_group->bundle = 'field_landing_page_section';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_section_class';
  $field_group->data = array(
    'label' => 'Section Second Body',
    'weight' => '8',
    'children' => array(
      0 => 'field_lp_section_second_body',
      1 => 'field_lp_section_image',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Section Second Body',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-lp-section-second-body',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_lp_section_second_body|field_collection_item|field_landing_page_section|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_section_class|field_collection_item|field_landing_page_section|default';
  $field_group->group_name = 'group_section_class';
  $field_group->entity_type = 'field_collection_item';
  $field_group->bundle = 'field_landing_page_section';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Section Class',
    'weight' => '0',
    'children' => array(
      0 => 'field_lp_section_title',
      1 => 'field_lp_section_sub_title',
      2 => 'field_lp_section_body',
      3 => 'field_lp_section_template',
      4 => 'group_lp_section_image',
      5 => 'group_section_image_class_ii',
      6 => 'group_lp_section_second_body',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Section Class',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'container',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_section_class|field_collection_item|field_landing_page_section|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_section_image_class_ii|field_collection_item|field_landing_page_section|default';
  $field_group->group_name = 'group_section_image_class_ii';
  $field_group->entity_type = 'field_collection_item';
  $field_group->bundle = 'field_landing_page_section';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_section_class';
  $field_group->data = array(
    'label' => 'Section Image Class II',
    'weight' => '4',
    'children' => array(
      0 => 'field_lp_section_images_ii',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Section Image Class II',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-section-image-class-ii',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_section_image_class_ii|field_collection_item|field_landing_page_section|default'] = $field_group;

  return $export;
}
