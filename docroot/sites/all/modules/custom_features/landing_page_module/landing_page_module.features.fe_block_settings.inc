<?php
/**
 * @file
 * landing_page_module.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function landing_page_module_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['panels_mini-mini_panel_landing_page_top'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'mini_panel_landing_page_top',
    'icon' => 'a:2:{s:4:"icon";s:0:"";s:8:"position";s:12:"title_before";}',
    'module' => 'panels_mini',
    'node_types' => array(
      0 => 'landing_page',
    ),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'mercy_bootstrap_subtheme' => array(
        'region' => 'slider',
        'status' => 1,
        'theme' => 'mercy_bootstrap_subtheme',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-landing_page_slider-block'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'landing_page_slider-block',
    'icon' => 'a:2:{s:4:"icon";s:0:"";s:8:"position";s:12:"title_before";}',
    'module' => 'views',
    'node_types' => array(
      0 => 'landing_page',
    ),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'mercy_bootstrap_subtheme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'mercy_bootstrap_subtheme',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  return $export;
}
