<?php
/**
 * @file
 * landing_page_module.panels_default.inc
 */

/**
 * Implements hook_default_panels_mini().
 */
function landing_page_module_default_panels_mini() {
  $export = array();

  $mini = new stdClass();
  $mini->disabled = FALSE; /* Edit this to true to make a default mini disabled initially */
  $mini->api_version = 1;
  $mini->name = 'mini_panel_landing_page_top';
  $mini->category = '';
  $mini->admin_title = 'Mini Panel Landing Page Top';
  $mini->admin_description = '';
  $mini->requiredcontexts = array();
  $mini->contexts = array();
  $mini->relationships = array();
  $display = new panels_display();
  $display->layout = 'onecol_reset_clean';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => array(
        'content' => array(
          'prefix' => '<div class="container">
<div class="row">',
          'suffix' => '</div>
</div>',
        ),
        'theme' => 0,
      ),
    ),
    'middle' => array(
      'style' => 'wrapper_raw',
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '8a128dd8-1239-4508-87a3-c716f60d6062';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-588660b2-2ab7-4940-9337-1c44cc873d80';
    $pane->panel = 'middle';
    $pane->type = 'views';
    $pane->subtype = 'landing_page_slider';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block',
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'landing-slider',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '588660b2-2ab7-4940-9337-1c44cc873d80';
    $display->content['new-588660b2-2ab7-4940-9337-1c44cc873d80'] = $pane;
    $display->panels['middle'][0] = 'new-588660b2-2ab7-4940-9337-1c44cc873d80';
    $pane = new stdClass();
    $pane->pid = 'new-071c4cb4-71ff-46b9-ba17-4f1299b7dffc';
    $pane->panel = 'middle';
    $pane->type = 'block';
    $pane->subtype = 'webform-client-block-18135';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'landing-page-form',
    );
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '071c4cb4-71ff-46b9-ba17-4f1299b7dffc';
    $display->content['new-071c4cb4-71ff-46b9-ba17-4f1299b7dffc'] = $pane;
    $display->panels['middle'][1] = 'new-071c4cb4-71ff-46b9-ba17-4f1299b7dffc';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-588660b2-2ab7-4940-9337-1c44cc873d80';
  $mini->display = $display;
  $export['mini_panel_landing_page_top'] = $mini;

  return $export;
}
