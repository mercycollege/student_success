<?php
/**
 * @file
 * landing_page_module.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function landing_page_module_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'landing_page_slider';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Landing Page Slider';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'flexslider';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Relationship: Content: Landing Page Header (field_landing_page_header) */
  $handler->display->display_options['relationships']['field_landing_page_header_value']['id'] = 'field_landing_page_header_value';
  $handler->display->display_options['relationships']['field_landing_page_header_value']['table'] = 'field_data_field_landing_page_header';
  $handler->display->display_options['relationships']['field_landing_page_header_value']['field'] = 'field_landing_page_header_value';
  $handler->display->display_options['relationships']['field_landing_page_header_value']['label'] = 'Header';
  $handler->display->display_options['relationships']['field_landing_page_header_value']['delta'] = '-1';
  /* Field: Field collection item: Description */
  $handler->display->display_options['fields']['field_landing_page_description']['id'] = 'field_landing_page_description';
  $handler->display->display_options['fields']['field_landing_page_description']['table'] = 'field_data_field_landing_page_description';
  $handler->display->display_options['fields']['field_landing_page_description']['field'] = 'field_landing_page_description';
  $handler->display->display_options['fields']['field_landing_page_description']['relationship'] = 'field_landing_page_header_value';
  $handler->display->display_options['fields']['field_landing_page_description']['label'] = '';
  $handler->display->display_options['fields']['field_landing_page_description']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_landing_page_description']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_landing_page_description']['settings'] = array(
    'empty_fields_handler' => '',
    'empty_fields_emptyfieldtext_empty_text' => '',
    'field_formatter_class' => '',
    'conditions' => array(
      0 => array(
        'condition' => '',
      ),
    ),
    'linked_field' => array(
      'linked' => 0,
      'destination' => '',
      'advanced' => array(
        'title' => '',
        'target' => '',
        'class' => '',
        'rel' => '',
        'text' => '',
      ),
    ),
  );
  /* Field: Field collection item: Image */
  $handler->display->display_options['fields']['field_landing_page_image']['id'] = 'field_landing_page_image';
  $handler->display->display_options['fields']['field_landing_page_image']['table'] = 'field_data_field_landing_page_image';
  $handler->display->display_options['fields']['field_landing_page_image']['field'] = 'field_landing_page_image';
  $handler->display->display_options['fields']['field_landing_page_image']['relationship'] = 'field_landing_page_header_value';
  $handler->display->display_options['fields']['field_landing_page_image']['label'] = '';
  $handler->display->display_options['fields']['field_landing_page_image']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_landing_page_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_landing_page_image']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_landing_page_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_landing_page_image']['settings'] = array(
    'image_style' => 'landing_slider',
    'image_link' => '',
    'empty_fields_handler' => '',
    'empty_fields_emptyfieldtext_empty_text' => '',
    'field_formatter_class' => '',
    'conditions' => array(
      0 => array(
        'condition' => '',
      ),
    ),
    'linked_field' => array(
      'linked' => 0,
      'destination' => '',
      'advanced' => array(
        'title' => '',
        'target' => '',
        'class' => '',
        'rel' => '',
        'text' => '',
      ),
    ),
  );
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '<div class ="landing-header">
	<div class="landing-page-header-image">[field_landing_page_image]</div>
        <div class="landing-page-header-description"><p>[field_landing_page_description]</p></div>
</div>';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Contextual filter: Content: Nid */
  $handler->display->display_options['arguments']['nid']['id'] = 'nid';
  $handler->display->display_options['arguments']['nid']['table'] = 'node';
  $handler->display->display_options['arguments']['nid']['field'] = 'nid';
  $handler->display->display_options['arguments']['nid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['nid']['default_argument_type'] = 'node';
  $handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '25';
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'landing_page' => 'landing_page',
  );

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $export['landing_page_slider'] = $view;

  return $export;
}
