<?php
/**
 * @file
 * landing_page_module.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function landing_page_module_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'clean_markup_blocks--panels_mini-mini_panel_landing_page_top';
  $strongarm->value = array(
    'block_wrapper' => 'div',
    'additional_block_classes' => '',
    'additional_block_attributes' => '',
    'enable_inner_div' => FALSE,
    'title_wrapper' => 'h2',
    'title_hide' => FALSE,
    'content_wrapper' => 'none',
    'block_html_id_as_class' => TRUE,
  );
  $export['clean_markup_blocks--panels_mini-mini_panel_landing_page_top'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'clean_markup_blocks--views-landing_page_slider-block';
  $strongarm->value = array(
    'block_wrapper' => 'div',
    'additional_block_classes' => '',
    'additional_block_attributes' => '',
    'enable_inner_div' => FALSE,
    'title_wrapper' => 'h2',
    'title_hide' => FALSE,
    'content_wrapper' => 'none',
    'block_html_id_as_class' => TRUE,
  );
  $export['clean_markup_blocks--views-landing_page_slider-block'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_anonymous_landing_page';
  $strongarm->value = 0;
  $export['comment_anonymous_landing_page'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_landing_page';
  $strongarm->value = 0;
  $export['comment_default_mode_landing_page'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_per_page_landing_page';
  $strongarm->value = '50';
  $export['comment_default_per_page_landing_page'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_landing_page';
  $strongarm->value = 0;
  $export['comment_form_location_landing_page'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_landing_page';
  $strongarm->value = '1';
  $export['comment_landing_page'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_landing_page';
  $strongarm->value = '0';
  $export['comment_preview_landing_page'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_landing_page';
  $strongarm->value = 0;
  $export['comment_subject_field_landing_page'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_fieldable_panels_pane__landing_page';
  $strongarm->value = array(
    'view_modes' => array(
      'full' => array(
        'custom_settings' => FALSE,
      ),
      'token' => array(
        'custom_settings' => FALSE,
      ),
    ),
    'extra_fields' => array(
      'form' => array(
        'title' => array(
          'weight' => '-5',
        ),
      ),
      'display' => array(
        'title' => array(
          'default' => array(
            'weight' => '-5',
            'visible' => TRUE,
          ),
        ),
      ),
    ),
  );
  $export['field_bundle_settings_fieldable_panels_pane__landing_page'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_field_collection_item__field_landing_page_header';
  $strongarm->value = array(
    'view_modes' => array(
      'full' => array(
        'custom_settings' => FALSE,
      ),
      'token' => array(
        'custom_settings' => FALSE,
      ),
    ),
    'extra_fields' => array(
      'form' => array(
        'redirect' => array(
          'weight' => '30',
        ),
      ),
      'display' => array(),
    ),
  );
  $export['field_bundle_settings_field_collection_item__field_landing_page_header'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_field_collection_item__field_landing_page_section';
  $strongarm->value = array(
    'view_modes' => array(
      'full' => array(
        'custom_settings' => FALSE,
      ),
      'token' => array(
        'custom_settings' => FALSE,
      ),
    ),
    'extra_fields' => array(
      'form' => array(
        'redirect' => array(
          'weight' => '0',
        ),
      ),
      'display' => array(),
    ),
  );
  $export['field_bundle_settings_field_collection_item__field_landing_page_section'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_node__landing_page';
  $strongarm->value = array(
    'view_modes' => array(
      'teaser' => array(
        'custom_settings' => TRUE,
      ),
      'full' => array(
        'custom_settings' => FALSE,
      ),
      'rss' => array(
        'custom_settings' => FALSE,
      ),
      'search_index' => array(
        'custom_settings' => FALSE,
      ),
      'search_result' => array(
        'custom_settings' => FALSE,
      ),
      'token' => array(
        'custom_settings' => FALSE,
      ),
    ),
    'extra_fields' => array(
      'form' => array(
        'path' => array(
          'weight' => '2',
        ),
        'redirect' => array(
          'weight' => '1',
        ),
        'workbench_access' => array(
          'weight' => '0',
        ),
      ),
      'display' => array(),
    ),
  );
  $export['field_bundle_settings_node__landing_page'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'language_content_type_landing_page';
  $strongarm->value = '0';
  $export['language_content_type_landing_page'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_landing_page';
  $strongarm->value = array();
  $export['menu_options_landing_page'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_landing_page';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_landing_page'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_landing_page';
  $strongarm->value = array();
  $export['node_options_landing_page'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_landing_page';
  $strongarm->value = '1';
  $export['node_preview_landing_page'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_landing_page';
  $strongarm->value = 0;
  $export['node_submitted_landing_page'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'panelizer_defaults_fieldable_panels_pane_landing_page';
  $strongarm->value = array(
    'status' => 0,
    'view modes' => array(
      'default' => array(
        'status' => 0,
        'default' => 0,
        'choice' => 0,
      ),
    ),
  );
  $export['panelizer_defaults_fieldable_panels_pane_landing_page'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'panelizer_defaults_node_landing_page';
  $strongarm->value = array(
    'status' => 1,
    'view modes' => array(
      'page_manager' => array(
        'status' => 0,
        'default' => 1,
        'choice' => 1,
      ),
      'default' => array(
        'status' => 1,
        'default' => 1,
        'choice' => 0,
      ),
      'teaser' => array(
        'status' => 0,
        'default' => 0,
        'choice' => 0,
      ),
    ),
  );
  $export['panelizer_defaults_node_landing_page'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'panelizer_node:landing_page_allowed_layouts';
  $strongarm->value = 'O:22:"panels_allowed_layouts":4:{s:9:"allow_new";b:1;s:11:"module_name";s:27:"panelizer_node:landing_page";s:23:"allowed_layout_settings";a:71:{s:8:"flexible";b:0;s:29:"flexible:3_column_equal_sizes";b:0;s:33:"flexible:flex_three_column_layout";b:0;s:18:"onecol_reset_clean";b:1;s:6:"myriad";b:0;s:12:"onecol_clean";b:0;s:8:"six_pack";b:0;s:17:"threecol_25_50_25";b:0;s:6:"onecol";b:1;s:14:"twocol_stacked";b:0;s:6:"twocol";b:0;s:13:"twocol_bricks";b:0;s:25:"threecol_25_50_25_stacked";b:0;s:17:"threecol_33_34_33";b:0;s:25:"threecol_33_34_33_stacked";b:0;s:8:"bartlett";b:0;s:6:"boxton";b:0;s:16:"bartlett_flipped";b:0;s:25:"threecol_42_29_29_stacked";b:0;s:5:"selby";b:0;s:20:"bricks_25_75_stacked";b:0;s:19:"four_three_adaptive";b:0;s:7:"brenham";b:0;s:25:"threecol_15_70_15_stacked";b:0;s:4:"webb";b:0;s:25:"threecol_44_35_21_stacked";b:0;s:6:"whelan";b:0;s:4:"pond";b:0;s:12:"sutro_double";b:0;s:15:"brenham_flipped";b:0;s:20:"twocol_63_37_stacked";b:0;s:13:"selby_flipped";b:0;s:8:"mccoppin";b:0;s:12:"webb_flipped";b:0;s:5:"geary";b:0;s:6:"bryant";b:0;s:6:"phelan";b:0;s:19:"three_four_adaptive";b:0;s:20:"twocol_30_70_stacked";b:0;s:4:"burr";b:0;s:5:"brown";b:0;s:5:"rolph";b:0;s:16:"threecol_7_62_31";b:0;s:9:"sanderson";b:0;s:7:"hewston";b:0;s:20:"twocol_77_23_stacked";b:0;s:17:"sanderson_flipped";b:0;s:12:"burr_flipped";b:0;s:15:"moscone_flipped";b:0;s:6:"harris";b:0;s:15:"hewston_flipped";b:0;s:22:"bryant_flipped_flipped";b:0;s:14:"taylor_flipped";b:0;s:5:"sutro";b:0;s:25:"threecol_44_37_19_stacked";b:0;s:6:"taylor";b:0;s:20:"twocol_70_30_stacked";b:0;s:7:"moscone";b:0;s:16:"newsroom_article";b:0;s:15:"profiles_layout";b:0;s:34:"twocol_34_66_three_in_cols_flipped";b:0;s:17:"threecol_28_36_36";b:0;s:26:"twocol_34_66_three_in_cols";b:0;s:27:"twocol_3brick_30_70_stacked";b:0;s:13:"program_pages";b:0;s:32:"twocol_34_66_two_in_cols_flipped";b:0;s:24:"twocol_34_66_two_in_cols";b:0;s:24:"threecol_33_34_33_custom";b:0;s:21:"twocol_stacked_custom";b:0;s:20:"twocol_33_67_stacked";b:0;s:24:"threecol_25_50_25_custom";b:0;}s:10:"form_state";N;}';
  $export['panelizer_node:landing_page_allowed_layouts'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'panelizer_node:landing_page_allowed_layouts_default';
  $strongarm->value = 0;
  $export['panelizer_node:landing_page_allowed_layouts_default'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'panelizer_node:landing_page_allowed_types_default';
  $strongarm->value = 0;
  $export['panelizer_node:landing_page_allowed_types_default'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'panelizer_node:landing_page_default';
  $strongarm->value = array(
    'entity_form_field' => 'entity_form_field',
    'token' => 'token',
    'block' => 'block',
    'custom' => 'custom',
    'entity_field_extra' => 'entity_field_extra',
    'entity_field' => 'entity_field',
    'entity_view' => 'entity_view',
    'fieldable_panels_pane' => 'fieldable_panels_pane',
    'flag_link' => 'flag_link',
    'panels_mini' => 'panels_mini',
    'views_panes' => 'views_panes',
    'views' => 'views',
    'other' => 'other',
  );
  $export['panelizer_node:landing_page_default'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'save_continue_landing_page';
  $strongarm->value = 'Save and add fields';
  $export['save_continue_landing_page'] = $strongarm;

  return $export;
}
