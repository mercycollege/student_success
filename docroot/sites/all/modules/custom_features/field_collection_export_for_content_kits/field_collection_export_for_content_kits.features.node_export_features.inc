<?php
/**
 * @file
 * field_collection_export_for_content_kits.features.node_export_features.inc
 */

/**
 * Implements hook_node_export_features_default().
 */
function field_collection_export_for_content_kits_node_export_features_default() {
  $node_export = array(
  'code_string' => 'array(
  (object) array(
      \'vid\' => \'17\',
      \'uid\' => \'2\',
      \'title\' => \'Node Export with Field Collections\',
      \'log\' => \'\',
      \'status\' => \'0\',
      \'comment\' => \'2\',
      \'promote\' => \'0\',
      \'sticky\' => \'0\',
      \'vuuid\' => \'f58996e5-c6df-4f4f-bccb-d1a38475e43e\',
      \'nid\' => \'17\',
      \'type\' => \'content_type_with_exportable_fie\',
      \'language\' => \'en\',
      \'created\' => \'1398780888\',
      \'changed\' => \'1398780888\',
      \'tnid\' => \'0\',
      \'translate\' => \'0\',
      \'uuid\' => \'bf494f74-3e50-43a7-b31e-b98c45ae9afd\',
      \'revision_timestamp\' => \'1398780888\',
      \'revision_uid\' => \'2\',
      \'field_box_kit_bevel_corners\' => array(),
      \'field_box_top_triangle\' => array(),
      \'field_plain_box\' => array(),
      \'field_video\' => array(),
      \'field_video_description\' => array(),
      \'field_fast_facts\' => array(),
      \'field_slide_show\' => array(),
      \'rdf_mapping\' => array(
        \'rdftype\' => array(
          \'sioc:Item\',
          \'foaf:Document\',
        ),
        \'title\' => array(
          \'predicates\' => array(
            \'dc:title\',
          ),
        ),
        \'created\' => array(
          \'predicates\' => array(
            \'dc:date\',
            \'dc:created\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
        \'changed\' => array(
          \'predicates\' => array(
            \'dc:modified\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
        \'body\' => array(
          \'predicates\' => array(
            \'content:encoded\',
          ),
        ),
        \'uid\' => array(
          \'predicates\' => array(
            \'sioc:has_creator\',
          ),
          \'type\' => \'rel\',
        ),
        \'name\' => array(
          \'predicates\' => array(
            \'foaf:name\',
          ),
        ),
        \'comment_count\' => array(
          \'predicates\' => array(
            \'sioc:num_replies\',
          ),
          \'datatype\' => \'xsd:integer\',
        ),
        \'last_activity\' => array(
          \'predicates\' => array(
            \'sioc:last_activity_date\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
      ),
      \'cid\' => \'0\',
      \'last_comment_timestamp\' => \'1398780888\',
      \'last_comment_name\' => NULL,
      \'last_comment_uid\' => \'2\',
      \'comment_count\' => \'0\',
      \'name\' => \'jgarcia\',
      \'picture\' => \'0\',
      \'data\' => \'a:2:{s:19:"ldap_authentication";a:1:{s:4:"init";a:3:{s:3:"sid";s:12:"mercydfdc001";s:2:"dn";s:67:"CN=axiomrunner,OU=VPN Users,OU=Domain Users,DC=mercy,DC=local";s:4:"mail";s:17:"webmaster@mercy.edu";}}s:7:"contact";i:1;}\',
      \'workbench_access\' => array(),
      \'path\' => array(
        \'pid\' => \'26\',
        \'source\' => \'node/17\',
        \'alias\' => \'node-export-field-collections\',
        \'language\' => \'en\',
      ),
      \'menu\' => NULL,
      \'node_export_drupal_version\' => \'7\',
      \'field_collection_deploy\' => array(),
    ),
)',
);
  return $node_export;
}
