<?php
/**
 * @file
 * field_collection_export_for_content_kits.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function field_collection_export_for_content_kits_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_hidden_title4|fieldable_panels_pane|video_content_kit|form';
  $field_group->group_name = 'group_hidden_title4';
  $field_group->entity_type = 'fieldable_panels_pane';
  $field_group->bundle = 'video_content_kit';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Hidden Tittle',
    'weight' => '0',
    'children' => array(
      0 => 'title',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Hidden Tittle',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => 'fieldable_panels_pane_video_content_kit_form_group_hidden_title4',
        'classes' => 'group-hidden-title',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_hidden_title4|fieldable_panels_pane|video_content_kit|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_hidden_title5|fieldable_panels_pane|fast_facts|form';
  $field_group->group_name = 'group_hidden_title5';
  $field_group->entity_type = 'fieldable_panels_pane';
  $field_group->bundle = 'fast_facts';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Hidden Title',
    'weight' => '0',
    'children' => array(
      0 => 'title',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Hidden Title',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => 'fieldable_panels_pane_fast_facts_form_group_hidden_title5',
        'classes' => 'group-hidden-title',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_hidden_title5|fieldable_panels_pane|fast_facts|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_hidden_title6|fieldable_panels_pane|slide_show|form';
  $field_group->group_name = 'group_hidden_title6';
  $field_group->entity_type = 'fieldable_panels_pane';
  $field_group->bundle = 'slide_show';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Hidden Title',
    'weight' => '0',
    'children' => array(
      0 => 'title',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Hidden Title',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => 'fieldable_panels_pane_slide_show_form_group_hidden_title6',
        'classes' => 'group-hidden-title',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_hidden_title6|fieldable_panels_pane|slide_show|form'] = $field_group;

  return $export;
}
