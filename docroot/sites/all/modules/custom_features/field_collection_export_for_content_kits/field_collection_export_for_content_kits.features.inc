<?php
/**
 * @file
 * field_collection_export_for_content_kits.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function field_collection_export_for_content_kits_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
