<?php
/**
 * @file
 * field_collection_export_for_content_kits.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function field_collection_export_for_content_kits_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_anonymous_content_type_with_exportable_fie';
  $strongarm->value = 0;
  $export['comment_anonymous_content_type_with_exportable_fie'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_content_type_with_exportable_fie';
  $strongarm->value = '2';
  $export['comment_content_type_with_exportable_fie'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_content_type_with_exportable_fie';
  $strongarm->value = 1;
  $export['comment_default_mode_content_type_with_exportable_fie'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_per_page_content_type_with_exportable_fie';
  $strongarm->value = '50';
  $export['comment_default_per_page_content_type_with_exportable_fie'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_content_type_with_exportable_fie';
  $strongarm->value = 1;
  $export['comment_form_location_content_type_with_exportable_fie'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_content_type_with_exportable_fie';
  $strongarm->value = '1';
  $export['comment_preview_content_type_with_exportable_fie'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_content_type_with_exportable_fie';
  $strongarm->value = 1;
  $export['comment_subject_field_content_type_with_exportable_fie'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_node__content_type_with_exportable_fie';
  $strongarm->value = array(
    'view_modes' => array(),
    'extra_fields' => array(
      'form' => array(
        'title' => array(
          'weight' => '-5',
        ),
        'path' => array(
          'weight' => '30',
        ),
        'redirect' => array(
          'weight' => '30',
        ),
        'workbench_access' => array(
          'weight' => '0',
        ),
      ),
      'display' => array(),
    ),
  );
  $export['field_bundle_settings_node__content_type_with_exportable_fie'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'language_content_type_content_type_with_exportable_fie';
  $strongarm->value = '0';
  $export['language_content_type_content_type_with_exportable_fie'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_content_type_with_exportable_fie';
  $strongarm->value = array(
    0 => 'main-menu',
  );
  $export['menu_options_content_type_with_exportable_fie'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_content_type_with_exportable_fie';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_content_type_with_exportable_fie'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_content_type_with_exportable_fie';
  $strongarm->value = array(
    0 => 'status',
    1 => 'promote',
  );
  $export['node_options_content_type_with_exportable_fie'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_content_type_with_exportable_fie';
  $strongarm->value = '1';
  $export['node_preview_content_type_with_exportable_fie'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_content_type_with_exportable_fie';
  $strongarm->value = 1;
  $export['node_submitted_content_type_with_exportable_fie'] = $strongarm;

  return $export;
}
