<?php
/**
 * @file
 * field_collection_export_for_content_kits.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function field_collection_export_for_content_kits_taxonomy_default_vocabularies() {
  return array(
    'play_button_position' => array(
      'name' => 'Play Button Position',
      'machine_name' => 'play_button_position',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 10,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
