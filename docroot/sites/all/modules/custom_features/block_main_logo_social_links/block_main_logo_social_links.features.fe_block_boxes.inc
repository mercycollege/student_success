<?php
/**
 * @file
 * block_main_logo_social_links.features.fe_block_boxes.inc
 */

/**
 * Implements hook_default_fe_block_boxes().
 */
function block_main_logo_social_links_default_fe_block_boxes() {
  $export = array();

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Main Logo And Social';
  $fe_block_boxes->format = 'admin_html';
  $fe_block_boxes->machine_name = 'main_logo_and_social';
  $fe_block_boxes->body = '<div class="top-logo">
<div class="main_logo main-logo">
<a href="http://mercy.edu"><img alt="Mercy College" src="/sites/default/files/default_images/mercy_stacked_small.png" /></a>
</div>
<div id="social-icons">
<div class="main_social"><a href="https://www.instagram.com/mercycollege/" target="_blank"><img src="/sites/default/files/default_images/social_ig.png" /></a></div>
<div class="main_social"><a href="http://www.youtube.com/user/1800mercyny" target="_blank"><img src="/sites/default/files/default_images/social_youtube.png" /></a></div>
<div class="main_social"><a href="https://twitter.com/mercycollege" target="_blank"><img src="/sites/default/files/default_images/social_twitter.png" /></a></div>
<div class="main_social"><a href="https://www.facebook.com/mercycollegeny" target="_blank"><img src="/sites/default/files/default_images/social_fb.png" /></a></div>
</div>
</div>';

  $export['main_logo_and_social'] = $fe_block_boxes;

  return $export;
}
