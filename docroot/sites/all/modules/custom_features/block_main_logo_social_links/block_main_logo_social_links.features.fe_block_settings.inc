<?php
/**
 * @file
 * block_main_logo_social_links.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function block_main_logo_social_links_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['block-main_logo_and_social'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'icon' => 'a:2:{s:4:"icon";s:0:"";s:8:"position";s:12:"title_before";}',
    'machine_name' => 'main_logo_and_social',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bootstrap' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'bootstrap',
        'weight' => -15,
      ),
      'mercy_bootstrap_subtheme' => array(
        'region' => 'top-logo',
        'status' => 1,
        'theme' => 'mercy_bootstrap_subtheme',
        'weight' => -19,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  return $export;
}
