<?php
/**
 * @file
 * newsroom_endpoint.services.inc
 */

/**
 * Implements hook_default_services_endpoint().
 */
function newsroom_endpoint_default_services_endpoint() {
  $export = array();

  $endpoint = new stdClass();
  $endpoint->disabled = FALSE; /* Edit this to true to make a default endpoint disabled initially */
  $endpoint->api_version = 3;
  $endpoint->name = 'service_endpoint';
  $endpoint->server = 'rest_server';
  $endpoint->path = 'api/v1';
  $endpoint->authentication = array();
  $endpoint->server_settings = array(
    'formatters' => array(
      'jsonp' => TRUE,
      'bencode' => FALSE,
      'json' => FALSE,
      'php' => FALSE,
      'xml' => FALSE,
    ),
    'parsers' => array(
      'application/json' => TRUE,
      'application/vnd.php.serialized' => FALSE,
      'application/x-www-form-urlencoded' => FALSE,
      'application/xml' => FALSE,
      'multipart/form-data' => FALSE,
      'text/xml' => FALSE,
    ),
  );
  $endpoint->resources = array(
    'articles' => array(
      'alias' => 'articles',
      'operations' => array(
        'index' => array(
          'enabled' => '1',
        ),
      ),
    ),
    'articles-global-engagement' => array(
      'operations' => array(
        'index' => array(
          'enabled' => '1',
        ),
      ),
    ),
    'event-ticker' => array(
      'alias' => 'event-ticker',
      'operations' => array(
        'index' => array(
          'enabled' => '1',
        ),
      ),
    ),
    'featured-article' => array(
      'alias' => 'featured-article',
      'operations' => array(
        'index' => array(
          'enabled' => '1',
        ),
      ),
    ),
  );
  $endpoint->debug = 0;
  $export['service_endpoint'] = $endpoint;

  return $export;
}
