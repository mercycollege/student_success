<?php
/**
 * @file
 * newsroom_endpoint.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function newsroom_endpoint_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'date_format_m_d_y';
  $strongarm->value = 'M d, Y';
  $export['date_format_m_d_y'] = $strongarm;

  return $export;
}
