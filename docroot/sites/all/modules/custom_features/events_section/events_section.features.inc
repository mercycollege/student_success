<?php
/**
 * @file
 * events_section.features.inc
 */

/**
 * Implements hook_views_api().
 */
function events_section_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
