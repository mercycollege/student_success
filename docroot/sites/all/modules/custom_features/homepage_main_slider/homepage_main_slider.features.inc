<?php
/**
 * @file
 * homepage_main_slider.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function homepage_main_slider_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "flexslider" && $api == "flexslider_default_preset") {
    return array("version" => "1");
  }
  if ($module == "panels_mini" && $api == "panels_default") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function homepage_main_slider_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function homepage_main_slider_image_default_styles() {
  $styles = array();

  // Exported image style: 200x90.
  $styles['200x90'] = array(
    'label' => '200x90',
    'effects' => array(),
  );

  // Exported image style: 300x115.
  $styles['300x115'] = array(
    'label' => '300x115',
    'effects' => array(
      17 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 300,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
      18 => array(
        'name' => 'image_crop',
        'data' => array(
          'width' => 300,
          'height' => 115,
          'anchor' => 'center-center',
        ),
        'weight' => 2,
      ),
    ),
  );

  return $styles;
}
