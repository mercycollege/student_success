<?php
/**
 * @file
 * homepage_main_slider.panels_default.inc
 */

/**
 * Implements hook_default_panels_mini().
 */
function homepage_main_slider_default_panels_mini() {
  $export = array();

  $mini = new stdClass();
  $mini->disabled = FALSE; /* Edit this to true to make a default mini disabled initially */
  $mini->api_version = 1;
  $mini->name = 'mini_panel_top_slider';
  $mini->category = '';
  $mini->admin_title = 'Mini Panel Top Slider';
  $mini->admin_description = 'A mini panel to house top slider.';
  $mini->requiredcontexts = array();
  $mini->contexts = array();
  $mini->relationships = array();
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '232418a9-0f7e-49eb-807b-8585aa2a44a7';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-5b32adff-5b2e-4ced-aecc-f3605f0e6fed';
    $pane->panel = 'middle';
    $pane->type = 'views';
    $pane->subtype = 'main_page_top_slider';
    $pane->shown = FALSE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '0',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block',
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'main-site',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '5b32adff-5b2e-4ced-aecc-f3605f0e6fed';
    $display->content['new-5b32adff-5b2e-4ced-aecc-f3605f0e6fed'] = $pane;
    $display->panels['middle'][0] = 'new-5b32adff-5b2e-4ced-aecc-f3605f0e6fed';
    $pane = new stdClass();
    $pane->pid = 'new-afb01ec7-ea53-45e4-8b6c-600f645b1747';
    $pane->panel = 'middle';
    $pane->type = 'views';
    $pane->subtype = 'main_page_top_slider';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '0',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block_1',
      'override_title' => 1,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'afb01ec7-ea53-45e4-8b6c-600f645b1747';
    $display->content['new-afb01ec7-ea53-45e4-8b6c-600f645b1747'] = $pane;
    $display->panels['middle'][1] = 'new-afb01ec7-ea53-45e4-8b6c-600f645b1747';
    $pane = new stdClass();
    $pane->pid = 'new-f9f83fd1-5e18-4abd-b4c0-5233cc3a6df5';
    $pane->panel = 'middle';
    $pane->type = 'views';
    $pane->subtype = 'main_page_top_slider';
    $pane->shown = FALSE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '0',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block_2',
      'override_title' => 1,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = 'f9f83fd1-5e18-4abd-b4c0-5233cc3a6df5';
    $display->content['new-f9f83fd1-5e18-4abd-b4c0-5233cc3a6df5'] = $pane;
    $display->panels['middle'][2] = 'new-f9f83fd1-5e18-4abd-b4c0-5233cc3a6df5';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $mini->display = $display;
  $export['mini_panel_top_slider'] = $mini;

  return $export;
}
