<?php
/**
 * @file
 * events_ical.features.inc
 */

/**
 * Implements hook_views_api().
 */
function events_ical_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
