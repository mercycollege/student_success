<?php
/**
 * @file
 * programs_and_degrees.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function programs_and_degrees_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "js_injector" && $api == "js_injector_rules") {
    return array("version" => "1");
  }
  if ($module == "panelizer" && $api == "panelizer") {
    return array("version" => "1");
  }
  if ($module == "panels_mini" && $api == "panels_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function programs_and_degrees_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function programs_and_degrees_node_info() {
  $items = array(
    'programs' => array(
      'name' => t('Degrees and Programs'),
      'base' => 'node_content',
      'description' => t('This form adds new programs offered at Mercy College.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'programs_faq' => array(
      'name' => t('Programs FAQ'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
