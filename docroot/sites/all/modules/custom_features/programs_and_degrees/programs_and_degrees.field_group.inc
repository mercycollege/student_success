<?php
/**
 * @file
 * programs_and_degrees.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function programs_and_degrees_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_admission_codes|node|programs|form';
  $field_group->group_name = 'group_admission_codes';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'programs';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_lefttabs';
  $field_group->data = array(
    'label' => 'Admission Codes',
    'weight' => '11',
    'children' => array(
      0 => 'field_program_code',
      1 => 'field_admission_type',
      2 => 'field_admissions_source',
      3 => 'field_banner_program_name',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-admission-codes field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_admission_codes|node|programs|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_courseinfo|node|programs|form';
  $field_group->group_name = 'group_courseinfo';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'programs';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_lefttabs';
  $field_group->data = array(
    'label' => 'Curriculum',
    'weight' => '8',
    'children' => array(
      0 => 'field_course_group',
      1 => 'field_courses_note',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'Curriculum',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $export['group_courseinfo|node|programs|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_external_page|node|programs|form';
  $field_group->group_name = 'group_external_page';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'programs';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_lefttabs';
  $field_group->data = array(
    'label' => 'External Page',
    'weight' => '10',
    'children' => array(
      0 => 'field_links',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-external-page field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_external_page|node|programs|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_faculty|node|programs|form';
  $field_group->group_name = 'group_faculty';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'programs';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_lefttabs';
  $field_group->data = array(
    'label' => 'Faculty',
    'weight' => '9',
    'children' => array(
      0 => 'field_faculty_tab',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_faculty|node|programs|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_howtoapply|node|programs|form';
  $field_group->group_name = 'group_howtoapply';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'programs';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_lefttabs';
  $field_group->data = array(
    'label' => 'How to Apply',
    'weight' => '7',
    'children' => array(
      0 => 'field_how_to_apply',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-howtoapply field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_howtoapply|node|programs|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_lefttabs|node|programs|form';
  $field_group->group_name = 'group_lefttabs';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'programs';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Left Tabs',
    'weight' => '5',
    'children' => array(
      0 => 'group_programs_top_section',
      1 => 'group_programinfo',
      2 => 'group_courseinfo',
      3 => 'group_overview',
      4 => 'group_faculty',
      5 => 'group_howtoapply',
      6 => 'group_external_page',
      7 => 'group_admission_codes',
    ),
    'format_type' => 'tabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => 'group-lefttabs field-group-tabs',
      ),
    ),
  );
  $export['group_lefttabs|node|programs|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_overview|node|programs|form';
  $field_group->group_name = 'group_overview';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'programs';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_lefttabs';
  $field_group->data = array(
    'label' => 'Overview',
    'weight' => '5',
    'children' => array(
      0 => 'field_program_head',
      1 => 'field_general_information',
      2 => 'field_accolades',
      3 => 'field_career_opportunities',
      4 => 'field_program_fast_facts',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_overview|node|programs|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_programinfo|node|programs|form';
  $field_group->group_name = 'group_programinfo';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'programs';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_lefttabs';
  $field_group->data = array(
    'label' => 'Program Information',
    'weight' => '6',
    'children' => array(
      0 => 'field_requirements',
      1 => 'field_admission_requirements',
      2 => 'field_accreditation',
      3 => 'field_purpose',
      4 => 'field_objectives',
      5 => 'field_program_outcomes',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_programinfo|node|programs|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_programs_top_section|node|programs|form';
  $field_group->group_name = 'group_programs_top_section';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'programs';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_lefttabs';
  $field_group->data = array(
    'label' => 'Top Section Information',
    'weight' => '4',
    'children' => array(
      0 => 'field_degree_type',
      1 => 'field_location',
      2 => 'field_credits',
      3 => 'field_department',
      4 => 'field_program_type',
      5 => 'field_degree_photo',
      6 => 'field_short_title',
      7 => 'field_slider_description',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'label' => 'Top Section Information',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $export['group_programs_top_section|node|programs|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_total_credits|field_collection_item|field_course_group|default';
  $field_group->group_name = 'group_total_credits';
  $field_group->entity_type = 'field_collection_item';
  $field_group->bundle = 'field_course_group';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Total Credits',
    'weight' => '1',
    'children' => array(
      0 => 'field_total_credits',
      1 => 'field_min_number_credits',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Total Credits',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'total-credits',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_total_credits|field_collection_item|field_course_group|default'] = $field_group;

  return $export;
}
