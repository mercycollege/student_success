<?php
/**
 * @file
 * programs_and_degrees.panelizer.inc
 */

/**
 * Implements hook_panelizer_defaults().
 */
function programs_and_degrees_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:programs:default:default';
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'programs';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = 'programs-pages';
  $panelizer->css = '';
  $panelizer->pipeline = 'ipe';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'default';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = FALSE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = 'program_pages';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => array(),
      'top' => array(),
      'left' => array(),
      'bottom' => array(),
      'header' => NULL,
      'top_middle' => NULL,
      'top_right' => NULL,
      'footer' => NULL,
      'bottom_middle' => NULL,
      'bottom_right' => NULL,
      'overview_middle' => NULL,
      'overview_right' => NULL,
      'program_info_middle' => NULL,
      'program_info_right' => NULL,
      'right' => NULL,
      'courses_group' => NULL,
      'program_information' => NULL,
      'faqs' => NULL,
      'faculty' => NULL,
      'how_to_apply' => NULL,
    ),
    'style' => 'wrapper_element',
    'top' => array(
      'style' => 'wrapper_element',
    ),
    'left' => array(
      'style' => 'wrapper_element',
    ),
    'right' => array(
      'style' => 'default',
    ),
    'bottom' => array(
      'style' => 'wrapper_element',
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = 'd8c84ed1-b9e6-4a11-b04d-d792d87d05fb';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-b2f1ae78-1e60-4ea5-bfe1-1c79ca46b046';
    $pane->panel = 'courses_group';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_course_group';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'title',
      'formatter' => 'field_collection_fields',
      'delta_limit' => '0',
      'delta_offset' => '0',
      'delta_reversed' => 0,
      'formatter_settings' => array(
        'view_mode' => 'full',
      ),
      'context' => 'panelizer',
      'override_title' => 1,
      'override_title_text' => '%node:title %node:field_degree_type Curriculum',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'programs-bevel-corners curriculum-tab programs-margin-bottom',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'b2f1ae78-1e60-4ea5-bfe1-1c79ca46b046';
    $display->content['new-b2f1ae78-1e60-4ea5-bfe1-1c79ca46b046'] = $pane;
    $display->panels['courses_group'][0] = 'new-b2f1ae78-1e60-4ea5-bfe1-1c79ca46b046';
    $pane = new stdClass();
    $pane->pid = 'new-4ffa5196-614c-4e4b-bb20-d39439edb6f1';
    $pane->panel = 'courses_group';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_courses_note';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'title',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'panelizer',
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'content-box curriculum-tab programs-margin-bottom',
    );
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '4ffa5196-614c-4e4b-bb20-d39439edb6f1';
    $display->content['new-4ffa5196-614c-4e4b-bb20-d39439edb6f1'] = $pane;
    $display->panels['courses_group'][1] = 'new-4ffa5196-614c-4e4b-bb20-d39439edb6f1';
    $pane = new stdClass();
    $pane->pid = 'new-84ac60b1-ae21-4a25-991f-f5879d9b313e';
    $pane->panel = 'faculty';
    $pane->type = 'views';
    $pane->subtype = 'faculty_profiles';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 1,
      'nodes_per_page' => '10',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block_1',
      'context' => array(
        0 => '',
      ),
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'faculty-tab',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '84ac60b1-ae21-4a25-991f-f5879d9b313e';
    $display->content['new-84ac60b1-ae21-4a25-991f-f5879d9b313e'] = $pane;
    $display->panels['faculty'][0] = 'new-84ac60b1-ae21-4a25-991f-f5879d9b313e';
    $pane = new stdClass();
    $pane->pid = 'new-c5637ab9-72ab-43d3-9d04-06437a5c673f';
    $pane->panel = 'faqs';
    $pane->type = 'views';
    $pane->subtype = 'programs_faqs';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 1,
      'nodes_per_page' => '6',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'page',
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'faqs-tab faqs-left',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'c5637ab9-72ab-43d3-9d04-06437a5c673f';
    $display->content['new-c5637ab9-72ab-43d3-9d04-06437a5c673f'] = $pane;
    $display->panels['faqs'][0] = 'new-c5637ab9-72ab-43d3-9d04-06437a5c673f';
    $pane = new stdClass();
    $pane->pid = 'new-944d170f-1d29-45b1-9504-193811544285';
    $pane->panel = 'faqs';
    $pane->type = 'views';
    $pane->subtype = 'programs_faqs';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 1,
      'nodes_per_page' => '3',
      'pager_id' => '0',
      'offset' => '3',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'page_1',
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'faqs-tab faqs-right',
    );
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '944d170f-1d29-45b1-9504-193811544285';
    $display->content['new-944d170f-1d29-45b1-9504-193811544285'] = $pane;
    $display->panels['faqs'][1] = 'new-944d170f-1d29-45b1-9504-193811544285';
    $pane = new stdClass();
    $pane->pid = 'new-5039f902-29a4-40d8-b737-2da577e897aa';
    $pane->panel = 'how_to_apply';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_how_to_apply';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'title',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'panelizer',
      'override_title' => 1,
      'override_title_text' => 'How To Apply to the %node:title %node:field_degree_type',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'programs-bevel-corners programs-margin-bottom',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '5039f902-29a4-40d8-b737-2da577e897aa';
    $display->content['new-5039f902-29a4-40d8-b737-2da577e897aa'] = $pane;
    $display->panels['how_to_apply'][0] = 'new-5039f902-29a4-40d8-b737-2da577e897aa';
    $pane = new stdClass();
    $pane->pid = 'new-cfa2a5bd-a831-44d9-8329-2f94ebfaffc3';
    $pane->panel = 'left';
    $pane->type = 'views';
    $pane->subtype = 'degrees_programs';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '0',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block_4',
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'style' => 'default',
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'program-pages-menu programs-margin-bottom',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'cfa2a5bd-a831-44d9-8329-2f94ebfaffc3';
    $display->content['new-cfa2a5bd-a831-44d9-8329-2f94ebfaffc3'] = $pane;
    $display->panels['left'][0] = 'new-cfa2a5bd-a831-44d9-8329-2f94ebfaffc3';
    $pane = new stdClass();
    $pane->pid = 'new-1efcce9d-9015-4438-8cd3-98797b4cf04c';
    $pane->panel = 'left';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => '',
      'title' => '',
      'body' => 'Get More Info',
      'format' => 'admin_html',
      'substitute' => 0,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'form-get-more-info',
    );
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '1efcce9d-9015-4438-8cd3-98797b4cf04c';
    $display->content['new-1efcce9d-9015-4438-8cd3-98797b4cf04c'] = $pane;
    $display->panels['left'][1] = 'new-1efcce9d-9015-4438-8cd3-98797b4cf04c';
    $pane = new stdClass();
    $pane->pid = 'new-dd6cd13a-e23a-4d4e-943b-f4059f0c2d33';
    $pane->panel = 'left';
    $pane->type = 'block';
    $pane->subtype = 'webform-client-block-18135';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'content-box-purple programs-margin-bottom',
    );
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = 'dd6cd13a-e23a-4d4e-943b-f4059f0c2d33';
    $display->content['new-dd6cd13a-e23a-4d4e-943b-f4059f0c2d33'] = $pane;
    $display->panels['left'][2] = 'new-dd6cd13a-e23a-4d4e-943b-f4059f0c2d33';
    $pane = new stdClass();
    $pane->pid = 'new-3329fc8c-69a5-4571-b7cd-bcc23fcb8471';
    $pane->panel = 'overview_middle';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_general_information';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'title',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'panelizer',
      'override_title' => 1,
      'override_title_text' => '%node:title Overview',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'programs-bevel-corners programs-margin-bottom',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '3329fc8c-69a5-4571-b7cd-bcc23fcb8471';
    $display->content['new-3329fc8c-69a5-4571-b7cd-bcc23fcb8471'] = $pane;
    $display->panels['overview_middle'][0] = 'new-3329fc8c-69a5-4571-b7cd-bcc23fcb8471';
    $pane = new stdClass();
    $pane->pid = 'new-6b3ec270-f6a3-4ca7-aec9-41286355e69d';
    $pane->panel = 'overview_middle';
    $pane->type = 'views';
    $pane->subtype = 'programs_faqs';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '2',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'page_2',
      'override_title' => 1,
      'override_title_text' => '%node:title FAQS',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'programs-bevel-corners programs-margin-bottom',
    );
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '6b3ec270-f6a3-4ca7-aec9-41286355e69d';
    $display->content['new-6b3ec270-f6a3-4ca7-aec9-41286355e69d'] = $pane;
    $display->panels['overview_middle'][1] = 'new-6b3ec270-f6a3-4ca7-aec9-41286355e69d';
    $pane = new stdClass();
    $pane->pid = 'new-c628f2cb-63d1-44e5-a5cc-d50856965ee2';
    $pane->panel = 'overview_right';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_program_fast_facts';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'title',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'panelizer',
      'override_title' => 1,
      'override_title_text' => '%node:title Fast Facts',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'programs-bevel-corners programs-margin-bottom',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'c628f2cb-63d1-44e5-a5cc-d50856965ee2';
    $display->content['new-c628f2cb-63d1-44e5-a5cc-d50856965ee2'] = $pane;
    $display->panels['overview_right'][0] = 'new-c628f2cb-63d1-44e5-a5cc-d50856965ee2';
    $pane = new stdClass();
    $pane->pid = 'new-8c52240b-209c-4145-a8e3-638a071c8034';
    $pane->panel = 'overview_right';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_career_opportunities';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'title',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'panelizer',
      'override_title' => 1,
      'override_title_text' => 'Career Opportunities',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'programs-bevel-corners programs-margin-bottom',
    );
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '8c52240b-209c-4145-a8e3-638a071c8034';
    $display->content['new-8c52240b-209c-4145-a8e3-638a071c8034'] = $pane;
    $display->panels['overview_right'][1] = 'new-8c52240b-209c-4145-a8e3-638a071c8034';
    $pane = new stdClass();
    $pane->pid = 'new-5fcc81f3-cc09-448e-894a-2c40bd00b6a4';
    $pane->panel = 'overview_right';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_accolades';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'title',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'panelizer',
      'override_title' => 1,
      'override_title_text' => '%node:title Accolades',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'programs-bevel-corners programs-margin-bottom',
    );
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '5fcc81f3-cc09-448e-894a-2c40bd00b6a4';
    $display->content['new-5fcc81f3-cc09-448e-894a-2c40bd00b6a4'] = $pane;
    $display->panels['overview_right'][2] = 'new-5fcc81f3-cc09-448e-894a-2c40bd00b6a4';
    $pane = new stdClass();
    $pane->pid = 'new-3e17acfa-e3ad-452a-821a-a8af0a36f6e5';
    $pane->panel = 'program_info_middle';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_purpose';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'title',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'panelizer',
      'override_title' => 1,
      'override_title_text' => 'Purpose',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'programs-bevel-corners programs-margin-bottom',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '3e17acfa-e3ad-452a-821a-a8af0a36f6e5';
    $display->content['new-3e17acfa-e3ad-452a-821a-a8af0a36f6e5'] = $pane;
    $display->panels['program_info_middle'][0] = 'new-3e17acfa-e3ad-452a-821a-a8af0a36f6e5';
    $pane = new stdClass();
    $pane->pid = 'new-8517c45c-4e81-4ec5-972d-df1e42e05ae0';
    $pane->panel = 'program_info_middle';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_admission_requirements';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'title',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'panelizer',
      'override_title' => 1,
      'override_title_text' => 'Admission Requirements',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'programs-bevel-corners programs-margin-bottom',
    );
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '8517c45c-4e81-4ec5-972d-df1e42e05ae0';
    $display->content['new-8517c45c-4e81-4ec5-972d-df1e42e05ae0'] = $pane;
    $display->panels['program_info_middle'][1] = 'new-8517c45c-4e81-4ec5-972d-df1e42e05ae0';
    $pane = new stdClass();
    $pane->pid = 'new-24f0abfa-3df7-4aad-918f-b48632b65ee1';
    $pane->panel = 'program_info_middle';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_accreditation';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'title',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'panelizer',
      'override_title' => 1,
      'override_title_text' => 'Accreditations',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'programs-bevel-corners programs-margin-bottom',
    );
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '24f0abfa-3df7-4aad-918f-b48632b65ee1';
    $display->content['new-24f0abfa-3df7-4aad-918f-b48632b65ee1'] = $pane;
    $display->panels['program_info_middle'][2] = 'new-24f0abfa-3df7-4aad-918f-b48632b65ee1';
    $pane = new stdClass();
    $pane->pid = 'new-748f29d6-6efe-47f3-8668-4071f2fc259f';
    $pane->panel = 'program_info_right';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_requirements';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'title',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'panelizer',
      'override_title' => 1,
      'override_title_text' => 'Degree Requirements',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'programs-bevel-corners programs-margin-bottom',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '748f29d6-6efe-47f3-8668-4071f2fc259f';
    $display->content['new-748f29d6-6efe-47f3-8668-4071f2fc259f'] = $pane;
    $display->panels['program_info_right'][0] = 'new-748f29d6-6efe-47f3-8668-4071f2fc259f';
    $pane = new stdClass();
    $pane->pid = 'new-2f4dd2ad-c34e-4ae9-b268-93972374e242';
    $pane->panel = 'program_info_right';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_objectives';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'title',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'panelizer',
      'override_title' => 1,
      'override_title_text' => '%node:title Objectives',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'programs-bevel-corners programs-margin-bottom',
    );
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '2f4dd2ad-c34e-4ae9-b268-93972374e242';
    $display->content['new-2f4dd2ad-c34e-4ae9-b268-93972374e242'] = $pane;
    $display->panels['program_info_right'][1] = 'new-2f4dd2ad-c34e-4ae9-b268-93972374e242';
    $pane = new stdClass();
    $pane->pid = 'new-c82d3889-962a-4f41-b8e9-0059b7b1e4a5';
    $pane->panel = 'program_info_right';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_program_outcomes';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'title',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'panelizer',
      'override_title' => 1,
      'override_title_text' => 'Program Outcomes',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'programs-bevel-corners programs-margin-bottom',
    );
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = 'c82d3889-962a-4f41-b8e9-0059b7b1e4a5';
    $display->content['new-c82d3889-962a-4f41-b8e9-0059b7b1e4a5'] = $pane;
    $display->panels['program_info_right'][2] = 'new-c82d3889-962a-4f41-b8e9-0059b7b1e4a5';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:programs:default:default'] = $panelizer;

  return $export;
}
