<?php
/**
 * @file
 * programs_and_degrees.panels_default.inc
 */

/**
 * Implements hook_default_panels_mini().
 */
function programs_and_degrees_default_panels_mini() {
  $export = array();

  $mini = new stdClass();
  $mini->disabled = FALSE; /* Edit this to true to make a default mini disabled initially */
  $mini->api_version = 1;
  $mini->name = 'mini_panel_programs_top';
  $mini->category = '';
  $mini->admin_title = 'Mini Panel Programs Top';
  $mini->admin_description = 'A mini panel to house the programs top image section.';
  $mini->requiredcontexts = array();
  $mini->contexts = array();
  $mini->relationships = array();
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '232418a9-0f7e-49eb-807b-8585aa2a44a7';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-28f781b5-467d-4d99-b3cf-38dd89083674';
    $pane->panel = 'middle';
    $pane->type = 'views';
    $pane->subtype = 'programs_top_photo';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '5',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block',
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'academics',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '28f781b5-467d-4d99-b3cf-38dd89083674';
    $display->content['new-28f781b5-467d-4d99-b3cf-38dd89083674'] = $pane;
    $display->panels['middle'][0] = 'new-28f781b5-467d-4d99-b3cf-38dd89083674';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $mini->display = $display;
  $export['mini_panel_programs_top'] = $mini;

  return $export;
}
