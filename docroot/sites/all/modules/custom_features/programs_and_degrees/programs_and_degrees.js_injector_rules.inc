<?php
/**
 * @file
 * programs_and_degrees.js_injector_rules.inc
 */

/**
 * Implements hook_js_injector_rule().
 */
function programs_and_degrees_js_injector_rule() {
  $export = array();

  $rule = new stdClass();
  $rule->disabled = FALSE; /* Edit this to true to make a default rule disabled initially */
  $rule->api_version = 1;
  $rule->name = 'Degrees Programs Reorder';
  $rule->admin_description = 'Move get info form to slideout menu for mobile views';
  $rule->js = <<<'EOJS'
/**
 * Move a div with class moveDiv to div with class moveDivTarget,
 * for reordering containers on mobile that are not using the bootstrap grid.
 */

(function ($) {
  var divContent;

  var moveDivUp = function () {
    // Store innerhtml of original unit.
    divContent = $("#.panel-pane.pane-block.pane-webform-client-block-18135.content-box-purple.programs-margin-bottom.pane-webform").html();
    // Empty original photo unit.
    $(".panel-pane.pane-block.pane-webform-client-block-18135.content-box-purple.programs-margin-bottom.pane-webform").empty();
    $(".panel-pane.pane-custom.pane-1.form-get-more-info").addClass('hidden');
    // Create new unit at the top identical to original unit
    $("#moveDivInfoTarget").append(divContent);
  };

  var moveDivDown = function () {
    $(".panel-pane.pane-custom.pane-1.form-get-more-info").removeClass('hidden');
    var $divInfoTarget = $("#moveDivInfoTarget");
    divContent = $divInfoTarget.html();
    $divInfoTarget.empty();
    $(".panel-pane.pane-block.pane-webform-client-block-18135.content-box-purple.programs-margin-bottom.pane-webform").empty().append(divContent);
  };

  $(document).ready(function() {
    if ($(".navbar-toggle").css("display") != "none" ){
      // Move unit from bottom to top.
      moveDivUp();
    }
  });

  $(window).resize(function() {
    var $navbarToggle = $(".navbar-toggle");
    if ($navbarToggle.css("display") != "none" ) {
      if ($("#moveDivInfo").html() != "") {
        // If original container is not empty.
        moveDivUp();
      }
    }
    else {
      if ($navbarToggle.css("display") == "none" ){
        // If original container is empty.
        if ($("#moveDivInfo").html() === "") {
          moveDivDown();
        }
      }
    }
  });
})(jQuery);
EOJS;

  $rule->position = 'header';
  $rule->preprocess = 0;
  $rule->inline = 0;
  $rule->page_visibility = 0;
  $rule->page_visibility_pages = 'degrees-programs/*
*';
  $rule->noscript_regions = '';
  $rule->noscript = '';
  $export['Degrees Programs Reorder'] = $rule;

  $rule = new stdClass();
  $rule->disabled = FALSE; /* Edit this to true to make a default rule disabled initially */
  $rule->api_version = 1;
  $rule->name = 'Program Pages Reorder';
  $rule->admin_description = 'Program Pages Get Info form Reorder';
  $rule->js = <<<'EOJS'
/**
 * Move a div with class moveDiv to div with class moveDivTarget,
 * for reordering containers on mobile that are not using the bootstrap grid.
 */
(function ($) {
  var divContent;
  var moveDivUp = function() {
    var $webform = $("#webform-client-form-18135");
    $webform.unbind();
    $webform.off();
    // Store innerhtml of original unit.
    var $webformPane = $(".panel-pane.pane-block.pane-webform-client-block-18135.content-box-purple.programs-margin-bottom.pane-webform");
    divContent = $webformPane.html();

    // Empty original photo unit.
    $webformPane.empty();
    $(".panel-pane.pane-custom.pane-1.form-get-more-info").addClass('hidden');
    // Create new unit at the top identical to original unit.
    $("#moveDivInfoTarget").append(divContent);
  };

  var moveDivDown = function() {
    $(".panel-pane.pane-custom.pane-1.form-get-more-info").removeClass('hidden');
    var $divInfoTarget = $("#moveDivInfoTarget");
    divContent = $divInfoTarget.html();
    $divInfoTarget.empty();
    $(".panel-pane.pane-block.pane-webform-client-block-18135.content-box-purple.programs-margin-bottom.pane-webform").append(divContent);
  };

  /**
   * Load remote content after the main page loaded.
   */
  var reinitializeAjax = function() {
    var $webform = $("#webform-client-form-18135");
    $webform.unbind();
    $webform.off();

    $webform.on("submit", function(e) {
      var base = $("#edit-webform-ajax-next-18135").attr('id');
      var element_settings = {
        url: '\\/system\\/ajax',
        callback: 'webform_ajax_callback',
        form: $('#webform-client-form-18135'),
        method: "replaceWith",
        wrapper: "webform-ajax-wrapper-18135",
        event: 'submit',
        progress: {
          message: '',
          type: 'throbber'
        },
        submit:
        {_triggering_element_name: "op", _triggering_element_value:"Submit"}
      };

      Drupal.ajax[base] = new Drupal.ajax(base, this, element_settings);
      console.log(base + ": " + Drupal.ajax[base]);
      e.preventDefault();
    }).submit();
  };

  $(document).ready(function() {
    if ($(".navbar-toggle").css("display") != "none") {

     // Move unit from bottom to top.
     moveDivUp();
     reinitializeAjax();
     }
 });

  $(window).resize(function() {
    var $navbarToggle = $(".navbar-toggle");
    var moveDiv = $("#moveDivInfo").html();
    if ($navbarToggle.css("display") != "none" ){
      if (moveDiv != "") {
        // If original container is not empty.
          moveDivUp();
          reinitializeAjax();
      }
    }
    else {
      if ($navbarToggle.css("display") == "none" ){
        // If original container is empty.
        if (moveDiv === "" || moveDiv === null) {
          moveDivDown();
          reinitializeAjax();
        }
      }
    }
  });
})(jQuery);
EOJS;
  $rule->position = 'header';
  $rule->preprocess = 0;
  $rule->inline = 0;
  $rule->page_visibility = 1;
  $rule->page_visibility_pages = 'degrees-programs/*
';
  $rule->noscript_regions = '';
  $rule->noscript = '';
  $export['Program Pages Reorder'] = $rule;

  return $export;
}
