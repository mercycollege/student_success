<?php
/**
 * @file
 * main_site_features_module.panelizer.inc
 */

/**
 * Implements hook_panelizer_defaults().
 */
function main_site_features_module_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:main_page:default:default';
  $panelizer->title = 'Basic Page';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'main_page';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'ipe';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'default';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = FALSE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = '43006a4b-768b-4d38-a840-051d5de70031';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-93c6d72c-2ac7-4988-9303-15a8eca04333';
    $pane->panel = 'center';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:body';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'field_formatter_class' => '',
      ),
      'context' => 'panelizer',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '93c6d72c-2ac7-4988-9303-15a8eca04333';
    $display->content['new-93c6d72c-2ac7-4988-9303-15a8eca04333'] = $pane;
    $display->panels['center'][0] = 'new-93c6d72c-2ac7-4988-9303-15a8eca04333';
    $pane = new stdClass();
    $pane->pid = 'new-c2177dfc-ed2b-449d-bab1-a5d53d95472a';
    $pane->panel = 'center';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_slider_images';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'image',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'image_style' => '',
        'image_link' => '',
        'field_formatter_class' => '',
      ),
      'context' => 'panelizer',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'c2177dfc-ed2b-449d-bab1-a5d53d95472a';
    $display->content['new-c2177dfc-ed2b-449d-bab1-a5d53d95472a'] = $pane;
    $display->panels['center'][1] = 'new-c2177dfc-ed2b-449d-bab1-a5d53d95472a';
    $pane = new stdClass();
    $pane->pid = 'new-0b78651d-9371-4a7e-96a9-37f6c89ac39c';
    $pane->panel = 'center';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_slider_title';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'field_formatter_class' => '',
      ),
      'context' => 'panelizer',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '0b78651d-9371-4a7e-96a9-37f6c89ac39c';
    $display->content['new-0b78651d-9371-4a7e-96a9-37f6c89ac39c'] = $pane;
    $display->panels['center'][2] = 'new-0b78651d-9371-4a7e-96a9-37f6c89ac39c';
    $pane = new stdClass();
    $pane->pid = 'new-e2e2bf03-0187-4546-aea7-ac046dbb82cb';
    $pane->panel = 'center';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_slider_description';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'field_formatter_class' => '',
      ),
      'context' => 'panelizer',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = 'e2e2bf03-0187-4546-aea7-ac046dbb82cb';
    $display->content['new-e2e2bf03-0187-4546-aea7-ac046dbb82cb'] = $pane;
    $display->panels['center'][3] = 'new-e2e2bf03-0187-4546-aea7-ac046dbb82cb';
    $pane = new stdClass();
    $pane->pid = 'new-cb5404ab-eccc-4e06-8ccb-d4f69e963781';
    $pane->panel = 'center';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_slider_button';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'link_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'field_formatter_class' => '',
      ),
      'context' => 'panelizer',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 4;
    $pane->locks = array();
    $pane->uuid = 'cb5404ab-eccc-4e06-8ccb-d4f69e963781';
    $display->content['new-cb5404ab-eccc-4e06-8ccb-d4f69e963781'] = $pane;
    $display->panels['center'][4] = 'new-cb5404ab-eccc-4e06-8ccb-d4f69e963781';
    $pane = new stdClass();
    $pane->pid = 'new-2216ffaa-e4ad-4078-a3d6-75ae56b50331';
    $pane->panel = 'center';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_mp_left_column_title';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'field_formatter_class' => '',
      ),
      'context' => 'panelizer',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 5;
    $pane->locks = array();
    $pane->uuid = '2216ffaa-e4ad-4078-a3d6-75ae56b50331';
    $display->content['new-2216ffaa-e4ad-4078-a3d6-75ae56b50331'] = $pane;
    $display->panels['center'][5] = 'new-2216ffaa-e4ad-4078-a3d6-75ae56b50331';
    $pane = new stdClass();
    $pane->pid = 'new-dfef9990-8280-42bf-9702-e14b10479826';
    $pane->panel = 'center';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_mp_left_column_content';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'field_formatter_class' => '',
      ),
      'context' => 'panelizer',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 6;
    $pane->locks = array();
    $pane->uuid = 'dfef9990-8280-42bf-9702-e14b10479826';
    $display->content['new-dfef9990-8280-42bf-9702-e14b10479826'] = $pane;
    $display->panels['center'][6] = 'new-dfef9990-8280-42bf-9702-e14b10479826';
    $pane = new stdClass();
    $pane->pid = 'new-16ceb692-8f0a-4e39-94c3-ca390a37c821';
    $pane->panel = 'center';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_mp_center_colum_top_image';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'above',
      'formatter' => 'image',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'image_style' => '',
        'image_link' => '',
        'field_formatter_class' => '',
      ),
      'context' => 'panelizer',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 7;
    $pane->locks = array();
    $pane->uuid = '16ceb692-8f0a-4e39-94c3-ca390a37c821';
    $display->content['new-16ceb692-8f0a-4e39-94c3-ca390a37c821'] = $pane;
    $display->panels['center'][7] = 'new-16ceb692-8f0a-4e39-94c3-ca390a37c821';
    $pane = new stdClass();
    $pane->pid = 'new-48167b8a-7f66-4f2b-acb7-974d946801e4';
    $pane->panel = 'center';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_mp_body_title';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'above',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'field_formatter_class' => '',
      ),
      'context' => 'panelizer',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 8;
    $pane->locks = array();
    $pane->uuid = '48167b8a-7f66-4f2b-acb7-974d946801e4';
    $display->content['new-48167b8a-7f66-4f2b-acb7-974d946801e4'] = $pane;
    $display->panels['center'][8] = 'new-48167b8a-7f66-4f2b-acb7-974d946801e4';
    $pane = new stdClass();
    $pane->pid = 'new-27a0f206-687d-4073-b6a5-9f3157f1c6ce';
    $pane->panel = 'center';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_mp_right_column_top_image';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'above',
      'formatter' => 'image',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'image_style' => '',
        'image_link' => '',
        'field_formatter_class' => '',
      ),
      'context' => 'panelizer',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 9;
    $pane->locks = array();
    $pane->uuid = '27a0f206-687d-4073-b6a5-9f3157f1c6ce';
    $display->content['new-27a0f206-687d-4073-b6a5-9f3157f1c6ce'] = $pane;
    $display->panels['center'][9] = 'new-27a0f206-687d-4073-b6a5-9f3157f1c6ce';
    $pane = new stdClass();
    $pane->pid = 'new-61f6110c-0c8f-4fc9-ad0a-b72e6e8227c0';
    $pane->panel = 'center';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_mp_right_column_top_title';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'above',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'field_formatter_class' => '',
      ),
      'context' => 'panelizer',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 10;
    $pane->locks = array();
    $pane->uuid = '61f6110c-0c8f-4fc9-ad0a-b72e6e8227c0';
    $display->content['new-61f6110c-0c8f-4fc9-ad0a-b72e6e8227c0'] = $pane;
    $display->panels['center'][10] = 'new-61f6110c-0c8f-4fc9-ad0a-b72e6e8227c0';
    $pane = new stdClass();
    $pane->pid = 'new-8304662b-5d82-4794-82d4-63cbd25a5363';
    $pane->panel = 'center';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_mp_right_column_top_conten';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'above',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'field_formatter_class' => '',
      ),
      'context' => 'panelizer',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 11;
    $pane->locks = array();
    $pane->uuid = '8304662b-5d82-4794-82d4-63cbd25a5363';
    $display->content['new-8304662b-5d82-4794-82d4-63cbd25a5363'] = $pane;
    $display->panels['center'][11] = 'new-8304662b-5d82-4794-82d4-63cbd25a5363';
    $pane = new stdClass();
    $pane->pid = 'new-e9c10a02-933a-40e6-ab4c-0ed5e99ec11a';
    $pane->panel = 'center';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_mp_bg_picker_fields';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'field_collection_view',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'edit' => 'Edit',
        'delete' => 'Delete',
        'add' => 'Add',
        'description' => TRUE,
        'view_mode' => 'full',
        'field_formatter_class' => '',
      ),
      'context' => 'panelizer',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 12;
    $pane->locks = array();
    $pane->uuid = 'e9c10a02-933a-40e6-ab4c-0ed5e99ec11a';
    $display->content['new-e9c10a02-933a-40e6-ab4c-0ed5e99ec11a'] = $pane;
    $display->panels['center'][12] = 'new-e9c10a02-933a-40e6-ab4c-0ed5e99ec11a';
    $pane = new stdClass();
    $pane->pid = 'new-4292a6f1-2340-4fbf-8965-9b0a337cb975';
    $pane->panel = 'center';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_mp_top_right_column_style_';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'field_collection_view',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'edit' => 'Edit',
        'delete' => 'Delete',
        'add' => 'Add',
        'description' => TRUE,
        'view_mode' => 'full',
        'field_formatter_class' => '',
      ),
      'context' => 'panelizer',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 13;
    $pane->locks = array();
    $pane->uuid = '4292a6f1-2340-4fbf-8965-9b0a337cb975';
    $display->content['new-4292a6f1-2340-4fbf-8965-9b0a337cb975'] = $pane;
    $display->panels['center'][13] = 'new-4292a6f1-2340-4fbf-8965-9b0a337cb975';
    $pane = new stdClass();
    $pane->pid = 'new-8b5ea7a6-2c32-4075-b78d-7b0d31300925';
    $pane->panel = 'center';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_mp_top_left_column';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'above',
      'formatter' => 'field_collection_view',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'edit' => 'Edit',
        'delete' => 'Delete',
        'add' => 'Add',
        'description' => TRUE,
        'view_mode' => 'full',
        'field_formatter_class' => '',
      ),
      'context' => 'panelizer',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 14;
    $pane->locks = array();
    $pane->uuid = '8b5ea7a6-2c32-4075-b78d-7b0d31300925';
    $display->content['new-8b5ea7a6-2c32-4075-b78d-7b0d31300925'] = $pane;
    $display->panels['center'][14] = 'new-8b5ea7a6-2c32-4075-b78d-7b0d31300925';
    $pane = new stdClass();
    $pane->pid = 'new-660d53bd-bb49-4585-87d2-9f042d544781';
    $pane->panel = 'center';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_mp_left_middle_box';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'field_collection_view',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'edit' => 'Edit',
        'delete' => 'Delete',
        'add' => 'Add',
        'description' => TRUE,
        'view_mode' => 'full',
        'field_formatter_class' => '',
      ),
      'context' => 'panelizer',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 15;
    $pane->locks = array();
    $pane->uuid = '660d53bd-bb49-4585-87d2-9f042d544781';
    $display->content['new-660d53bd-bb49-4585-87d2-9f042d544781'] = $pane;
    $display->panels['center'][15] = 'new-660d53bd-bb49-4585-87d2-9f042d544781';
    $pane = new stdClass();
    $pane->pid = 'new-f4a2e167-ab95-4dd7-8380-35bcea1da01e';
    $pane->panel = 'center';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_mp_middle_center_box';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'above',
      'formatter' => 'field_collection_view',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'edit' => 'Edit',
        'delete' => 'Delete',
        'add' => 'Add',
        'description' => TRUE,
        'view_mode' => 'full',
        'field_formatter_class' => '',
      ),
      'context' => 'panelizer',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 16;
    $pane->locks = array();
    $pane->uuid = 'f4a2e167-ab95-4dd7-8380-35bcea1da01e';
    $display->content['new-f4a2e167-ab95-4dd7-8380-35bcea1da01e'] = $pane;
    $display->panels['center'][16] = 'new-f4a2e167-ab95-4dd7-8380-35bcea1da01e';
    $pane = new stdClass();
    $pane->pid = 'new-4a8abe25-5f70-4fa6-91b3-cd8a89813876';
    $pane->panel = 'center';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_mp_middle_right_box';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'above',
      'formatter' => 'field_collection_view',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'edit' => 'Edit',
        'delete' => 'Delete',
        'add' => 'Add',
        'description' => TRUE,
        'view_mode' => 'full',
        'field_formatter_class' => '',
      ),
      'context' => 'panelizer',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 17;
    $pane->locks = array();
    $pane->uuid = '4a8abe25-5f70-4fa6-91b3-cd8a89813876';
    $display->content['new-4a8abe25-5f70-4fa6-91b3-cd8a89813876'] = $pane;
    $display->panels['center'][17] = 'new-4a8abe25-5f70-4fa6-91b3-cd8a89813876';
    $pane = new stdClass();
    $pane->pid = 'new-5b92e4aa-34a1-497a-9375-640536a8d3dd';
    $pane->panel = 'center';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_mp_bottom_left_box';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'above',
      'formatter' => 'field_collection_view',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'edit' => 'Edit',
        'delete' => 'Delete',
        'add' => 'Add',
        'description' => TRUE,
        'view_mode' => 'full',
        'field_formatter_class' => '',
      ),
      'context' => 'panelizer',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 18;
    $pane->locks = array();
    $pane->uuid = '5b92e4aa-34a1-497a-9375-640536a8d3dd';
    $display->content['new-5b92e4aa-34a1-497a-9375-640536a8d3dd'] = $pane;
    $display->panels['center'][18] = 'new-5b92e4aa-34a1-497a-9375-640536a8d3dd';
    $pane = new stdClass();
    $pane->pid = 'new-68bf0088-6f72-43d8-afd7-5ebe14b387f7';
    $pane->panel = 'center';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_mp_bottom_center_box';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'above',
      'formatter' => 'field_collection_view',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'edit' => 'Edit',
        'delete' => 'Delete',
        'add' => 'Add',
        'description' => TRUE,
        'view_mode' => 'full',
        'field_formatter_class' => '',
      ),
      'context' => 'panelizer',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 19;
    $pane->locks = array();
    $pane->uuid = '68bf0088-6f72-43d8-afd7-5ebe14b387f7';
    $display->content['new-68bf0088-6f72-43d8-afd7-5ebe14b387f7'] = $pane;
    $display->panels['center'][19] = 'new-68bf0088-6f72-43d8-afd7-5ebe14b387f7';
    $pane = new stdClass();
    $pane->pid = 'new-64a87b4e-8dfb-4049-9be2-accb88a2e32a';
    $pane->panel = 'center';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_mp_bottom_right_box';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'above',
      'formatter' => 'field_collection_view',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'edit' => 'Edit',
        'delete' => 'Delete',
        'add' => 'Add',
        'description' => TRUE,
        'view_mode' => 'full',
        'field_formatter_class' => '',
      ),
      'context' => 'panelizer',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 20;
    $pane->locks = array();
    $pane->uuid = '64a87b4e-8dfb-4049-9be2-accb88a2e32a';
    $display->content['new-64a87b4e-8dfb-4049-9be2-accb88a2e32a'] = $pane;
    $display->panels['center'][20] = 'new-64a87b4e-8dfb-4049-9be2-accb88a2e32a';
    $pane = new stdClass();
    $pane->pid = 'new-4dbbc3a0-31ec-4e21-afde-f59577478397';
    $pane->panel = 'center';
    $pane->type = 'node_links';
    $pane->subtype = 'node_links';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => FALSE,
      'override_title_text' => '',
      'build_mode' => 'default',
      'identifier' => '',
      'link' => TRUE,
      'context' => 'panelizer',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_class' => 'link-wrapper',
    );
    $pane->extras = array();
    $pane->position = 21;
    $pane->locks = array();
    $pane->uuid = '4dbbc3a0-31ec-4e21-afde-f59577478397';
    $display->content['new-4dbbc3a0-31ec-4e21-afde-f59577478397'] = $pane;
    $display->panels['center'][21] = 'new-4dbbc3a0-31ec-4e21-afde-f59577478397';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-93c6d72c-2ac7-4988-9303-15a8eca04333';
  $panelizer->display = $display;
  $export['node:main_page:default:default'] = $panelizer;

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:page:default';
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'page';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'ipe';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = 'threecol_25_50_25_stacked';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
      'top' => NULL,
      'left' => NULL,
      'middle' => NULL,
      'right' => array(
        'region_title' => '',
        'modifier_class' => '',
        'collapsible' => 1,
        'collapsed' => 0,
        'hide_classes' => array(
          'hidden-xs' => 0,
          'hidden-sm' => 0,
          'hidden-md' => 0,
          'hidden-lg' => 0,
        ),
        'additional_classes' => array(
          'pull-left' => 0,
          'pull-right' => 0,
          'clearfix' => 0,
        ),
      ),
      'bottom' => NULL,
    ),
    'right' => array(
      'style' => 'bootstrap_styles:panel',
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = '2aa4942f-9d75-41cd-91c4-4b799ff01bc7';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-321a9ed9-2974-402f-a208-ef3351697d5f';
    $pane->panel = 'middle';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:body';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'title',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'panelizer',
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => '',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '321a9ed9-2974-402f-a208-ef3351697d5f';
    $display->content['new-321a9ed9-2974-402f-a208-ef3351697d5f'] = $pane;
    $display->panels['middle'][0] = 'new-321a9ed9-2974-402f-a208-ef3351697d5f';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:page:default'] = $panelizer;

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:page:default:default';
  $panelizer->title = '';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'page';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'ipe';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'default';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = FALSE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = 'threecol_33_34_33_custom';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'left' => NULL,
      'middle' => NULL,
      'right' => NULL,
      'bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'No Title';
  $display->uuid = '38e2aaa9-5fee-47d5-83e0-f2b859b40345';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-1ea11ca4-93bc-4ce5-aa83-86efc47b6684';
    $pane->panel = 'left';
    $pane->type = 'block';
    $pane->subtype = 'menu-menu-faculty-resources';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'role',
          'settings' => array(
            'rids' => array(
              0 => 46,
            ),
          ),
          'context' => 'logged-in-user',
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => array(
        'title' => array(
          'element' => 'div',
          'attributes' => array(
            'id' => '',
            'class' => 'title-bevel-corners-purple',
          ),
        ),
        'content' => array(
          'element' => 'div',
          'attributes' => array(
            'id' => '',
            'class' => 'content-bevel-corners-purple',
          ),
        ),
        'theme' => 0,
      ),
      'style' => 'wrapper_element',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '1ea11ca4-93bc-4ce5-aa83-86efc47b6684';
    $display->content['new-1ea11ca4-93bc-4ce5-aa83-86efc47b6684'] = $pane;
    $display->panels['left'][0] = 'new-1ea11ca4-93bc-4ce5-aa83-86efc47b6684';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:page:default:default'] = $panelizer;

  return $export;
}
