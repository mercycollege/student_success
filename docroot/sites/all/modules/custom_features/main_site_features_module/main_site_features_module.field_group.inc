<?php
/**
 * @file
 * main_site_features_module.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function main_site_features_module_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_box_bevel_corners|fieldable_panels_pane|fieldable_panels_pane|form';
  $field_group->group_name = 'group_box_bevel_corners';
  $field_group->entity_type = 'fieldable_panels_pane';
  $field_group->bundle = 'fieldable_panels_pane';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Box with Bevel Corners',
    'weight' => '3',
    'children' => array(
      0 => 'field_mp_bottom_left_box',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Box with Bevel Corners',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => 'fieldable_panels_pane_fieldable_panels_pane_form_group_box_bevel_corners',
        'classes' => 'well',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_box_bevel_corners|fieldable_panels_pane|fieldable_panels_pane|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_box_with_top_triangle|fieldable_panels_pane|fieldable_panels_pane|form';
  $field_group->group_name = 'group_box_with_top_triangle';
  $field_group->entity_type = 'fieldable_panels_pane';
  $field_group->bundle = 'fieldable_panels_pane';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Box with Top Triangle',
    'weight' => '8',
    'children' => array(
      0 => 'field_mp_bottom_center_box',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Box with Top Triangle',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => 'fieldable_panels_pane_fieldable_panels_pane_form_group_box_with_top_triangle',
        'classes' => 'well',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_box_with_top_triangle|fieldable_panels_pane|fieldable_panels_pane|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_location_campus_wrap|node|calendar|form';
  $field_group->group_name = 'group_location_campus_wrap';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'calendar';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Location',
    'weight' => '7',
    'children' => array(
      0 => 'field_location',
      1 => 'field_crm_code_dobbs_ferry',
      2 => 'field_crm_code_bronx',
      3 => 'field_crm_code_manhattan',
      4 => 'field_crm_code_yorktown_heights',
      5 => 'field_crm_code_online',
      6 => 'field_crm_code_off_campus',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Location',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-location-campus-wrap',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_location_campus_wrap|node|calendar|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_video_content_kit|fieldable_panels_pane|fieldable_panels_pane|form';
  $field_group->group_name = 'group_video_content_kit';
  $field_group->entity_type = 'fieldable_panels_pane';
  $field_group->bundle = 'fieldable_panels_pane';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Video Content Kit',
    'weight' => '1',
    'children' => array(
      0 => 'field_video',
      1 => 'field_video_description',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Video Content Kit',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => 'fieldable_panels_pane_fieldable_panels_pane_form_group_video_content_kit',
        'classes' => 'well',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_video_content_kit|fieldable_panels_pane|fieldable_panels_pane|form'] = $field_group;

  return $export;
}
