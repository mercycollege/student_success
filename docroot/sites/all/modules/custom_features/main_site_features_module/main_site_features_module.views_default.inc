<?php
/**
 * @file
 * main_site_features_module.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function main_site_features_module_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'main_page_top_slider';
  $view->description = 'This view is mandatory for all Main Pages';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Main Page Top Slider';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['disable_sql_rewrite'] = TRUE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'flexslider';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['hide_empty'] = TRUE;
  /* Relationship: Content: Top Slider (field_top_slider) */
  $handler->display->display_options['relationships']['field_top_slider_value']['id'] = 'field_top_slider_value';
  $handler->display->display_options['relationships']['field_top_slider_value']['table'] = 'field_data_field_top_slider';
  $handler->display->display_options['relationships']['field_top_slider_value']['field'] = 'field_top_slider_value';
  $handler->display->display_options['relationships']['field_top_slider_value']['delta'] = '-1';
  /* Field: Content: Alternate Title */
  $handler->display->display_options['fields']['field_alternate_title']['id'] = 'field_alternate_title';
  $handler->display->display_options['fields']['field_alternate_title']['table'] = 'field_data_field_alternate_title';
  $handler->display->display_options['fields']['field_alternate_title']['field'] = 'field_alternate_title';
  $handler->display->display_options['fields']['field_alternate_title']['label'] = '';
  $handler->display->display_options['fields']['field_alternate_title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_alternate_title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_alternate_title']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_alternate_title']['settings'] = array(
    'empty_fields_handler' => '',
    'empty_fields_emptyfieldtext_empty_text' => '',
    'field_formatter_class' => '',
    'linked_field' => array(
      'linked' => 0,
      'destination' => '',
      'advanced' => array(
        'title' => '',
        'target' => '',
        'class' => '',
        'rel' => '',
        'text' => '',
      ),
    ),
  );
  /* Field: Field: Image */
  $handler->display->display_options['fields']['field_image']['id'] = 'field_image';
  $handler->display->display_options['fields']['field_image']['table'] = 'field_data_field_image';
  $handler->display->display_options['fields']['field_image']['field'] = 'field_image';
  $handler->display->display_options['fields']['field_image']['relationship'] = 'field_top_slider_value';
  $handler->display->display_options['fields']['field_image']['label'] = '';
  $handler->display->display_options['fields']['field_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_image']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_image']['settings'] = array(
    'image_style' => 'top_slider',
    'image_link' => '',
    'field_formatter_class' => '',
  );
  /* Field: Field: Teaser Description */
  $handler->display->display_options['fields']['field_slider_description']['id'] = 'field_slider_description';
  $handler->display->display_options['fields']['field_slider_description']['table'] = 'field_data_field_slider_description';
  $handler->display->display_options['fields']['field_slider_description']['field'] = 'field_slider_description';
  $handler->display->display_options['fields']['field_slider_description']['relationship'] = 'field_top_slider_value';
  $handler->display->display_options['fields']['field_slider_description']['label'] = '';
  $handler->display->display_options['fields']['field_slider_description']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_slider_description']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_slider_description']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_slider_description']['settings'] = array(
    'field_formatter_class' => '',
  );
  /* Field: Field: Title */
  $handler->display->display_options['fields']['field_title']['id'] = 'field_title';
  $handler->display->display_options['fields']['field_title']['table'] = 'field_data_field_title';
  $handler->display->display_options['fields']['field_title']['field'] = 'field_title';
  $handler->display->display_options['fields']['field_title']['relationship'] = 'field_top_slider_value';
  $handler->display->display_options['fields']['field_title']['label'] = '';
  $handler->display->display_options['fields']['field_title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_title']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_title']['settings'] = array(
    'field_formatter_class' => '',
  );
  /* Field: Field: Video Link */
  $handler->display->display_options['fields']['field_video_link']['id'] = 'field_video_link';
  $handler->display->display_options['fields']['field_video_link']['table'] = 'field_data_field_video_link';
  $handler->display->display_options['fields']['field_video_link']['field'] = 'field_video_link';
  $handler->display->display_options['fields']['field_video_link']['relationship'] = 'field_top_slider_value';
  $handler->display->display_options['fields']['field_video_link']['label'] = '';
  $handler->display->display_options['fields']['field_video_link']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_video_link']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_video_link']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_video_link']['click_sort_column'] = 'url';
  $handler->display->display_options['fields']['field_video_link']['settings'] = array(
    'field_formatter_class' => '',
    'linked_field' => array(
      'linked' => 0,
      'destination' => '',
      'advanced' => array(
        'title' => '',
        'target' => '',
        'class' => '',
        'rel' => '',
        'text' => '',
      ),
    ),
  );
  /* Field: Global: PHP */
  $handler->display->display_options['fields']['php']['id'] = 'php';
  $handler->display->display_options['fields']['php']['table'] = 'views';
  $handler->display->display_options['fields']['php']['field'] = 'php';
  $handler->display->display_options['fields']['php']['label'] = '';
  $handler->display->display_options['fields']['php']['exclude'] = TRUE;
  $handler->display->display_options['fields']['php']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['php']['use_php_setup'] = 0;
  $handler->display->display_options['fields']['php']['php_output'] = '<?php
	$site_name = variable_get(\'site_name\');

	if (isset($data ->field_field_alternate_title[0][\'raw\'][\'value\']) && ($data ->field_field_alternate_title[0][\'raw\'][\'value\']) == "none") {
		return false;
	}
	elseif (isset($data ->field_field_alternate_title[0][\'raw\'][\'value\'])) {
		print ($data ->field_field_alternate_title[0][\'raw\'][\'value\']);
		
	}
	else {
		print $site_name;
	}
?>';
  $handler->display->display_options['fields']['php']['use_php_click_sortable'] = '0';
  $handler->display->display_options['fields']['php']['php_click_sortable'] = '';
  /* Field: Field collection item: Slider Button */
  $handler->display->display_options['fields']['field_slider_button']['id'] = 'field_slider_button';
  $handler->display->display_options['fields']['field_slider_button']['table'] = 'field_data_field_slider_button';
  $handler->display->display_options['fields']['field_slider_button']['field'] = 'field_slider_button';
  $handler->display->display_options['fields']['field_slider_button']['relationship'] = 'field_top_slider_value';
  $handler->display->display_options['fields']['field_slider_button']['label'] = '';
  $handler->display->display_options['fields']['field_slider_button']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_slider_button']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_slider_button']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_slider_button']['click_sort_column'] = 'url';
  $handler->display->display_options['fields']['field_slider_button']['settings'] = array(
    'field_formatter_class' => '',
  );
  /* Field: Field: Slider Counter */
  $handler->display->display_options['fields']['field_test']['id'] = 'field_test';
  $handler->display->display_options['fields']['field_test']['table'] = 'field_data_field_test';
  $handler->display->display_options['fields']['field_test']['field'] = 'field_test';
  $handler->display->display_options['fields']['field_test']['label'] = '';
  $handler->display->display_options['fields']['field_test']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_test']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_test']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_test']['settings'] = array(
    'empty_fields_handler' => '',
    'empty_fields_emptyfieldtext_empty_text' => '',
    'field_formatter_class' => '',
    'linked_field' => array(
      'linked' => 0,
      'destination' => '',
      'advanced' => array(
        'title' => '',
        'target' => '',
        'class' => '',
        'rel' => '',
        'text' => '',
      ),
    ),
    'conditions' => array(
      0 => array(
        'condition' => '',
      ),
    ),
  );
  $handler->display->display_options['fields']['field_test']['field_api_classes'] = TRUE;
  /* Field: Global: PHP */
  $handler->display->display_options['fields']['php_1']['id'] = 'php_1';
  $handler->display->display_options['fields']['php_1']['table'] = 'views';
  $handler->display->display_options['fields']['php_1']['field'] = 'php';
  $handler->display->display_options['fields']['php_1']['label'] = '';
  $handler->display->display_options['fields']['php_1']['exclude'] = TRUE;
  $handler->display->display_options['fields']['php_1']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['php_1']['use_php_setup'] = 0;
  $handler->display->display_options['fields']['php_1']['php_output'] = '<?php
if (isset($data->field_field_test[0][\'rendered\']) && ($data->field_field_test[0][\'rendered\']) == 1) {
print (\'single-item-slider-content\');
}
else {
print (\'slider_content\');
}
?>';
  $handler->display->display_options['fields']['php_1']['use_php_click_sortable'] = '0';
  $handler->display->display_options['fields']['php_1']['php_click_sortable'] = '';
  /* Field: Content: Darken Title */
  $handler->display->display_options['fields']['field_darken_title']['id'] = 'field_darken_title';
  $handler->display->display_options['fields']['field_darken_title']['table'] = 'field_data_field_darken_title';
  $handler->display->display_options['fields']['field_darken_title']['field'] = 'field_darken_title';
  $handler->display->display_options['fields']['field_darken_title']['label'] = '';
  $handler->display->display_options['fields']['field_darken_title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_darken_title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_darken_title']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_darken_title']['settings'] = array(
    'empty_fields_handler' => '',
    'empty_fields_emptyfieldtext_empty_text' => '',
    'field_formatter_class' => '',
    'linked_field' => array(
      'linked' => 0,
      'destination' => '',
      'advanced' => array(
        'title' => '',
        'target' => '',
        'class' => '',
        'rel' => '',
        'text' => '',
      ),
    ),
    'conditions' => array(
      0 => array(
        'condition' => '',
      ),
    ),
  );
  /* Field: Content: Darken Title */
  $handler->display->display_options['fields']['field_darken_title_1']['id'] = 'field_darken_title_1';
  $handler->display->display_options['fields']['field_darken_title_1']['table'] = 'field_data_field_darken_title';
  $handler->display->display_options['fields']['field_darken_title_1']['field'] = 'field_darken_title';
  $handler->display->display_options['fields']['field_darken_title_1']['label'] = '';
  $handler->display->display_options['fields']['field_darken_title_1']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_darken_title_1']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_darken_title_1']['alter']['text'] = '</div></div>';
  $handler->display->display_options['fields']['field_darken_title_1']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_darken_title_1']['settings'] = array(
    'empty_fields_handler' => '',
    'empty_fields_emptyfieldtext_empty_text' => '',
    'field_formatter_class' => '',
    'linked_field' => array(
      'linked' => 0,
      'destination' => '',
      'advanced' => array(
        'title' => '',
        'target' => '',
        'class' => '',
        'rel' => '',
        'text' => '',
      ),
    ),
    'conditions' => array(
      0 => array(
        'condition' => '',
      ),
    ),
  );
  /* Field: Global: PHP */
  $handler->display->display_options['fields']['php_2']['id'] = 'php_2';
  $handler->display->display_options['fields']['php_2']['table'] = 'views';
  $handler->display->display_options['fields']['php_2']['field'] = 'php';
  $handler->display->display_options['fields']['php_2']['label'] = '';
  $handler->display->display_options['fields']['php_2']['exclude'] = TRUE;
  $handler->display->display_options['fields']['php_2']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['php_2']['use_php_setup'] = 0;
  $handler->display->display_options['fields']['php_2']['php_output'] = '<?php
if (isset($data->_field_data[\'nid\'][\'entity\']->field_darken_title[\'und\'][0][\'value\'])) {
	$darken_title = $data->_field_data[\'nid\'][\'entity\']->field_darken_title[\'und\'][0][\'value\'];
	if ($darken_title ==1) {
		$darken_class = ("<div class=\'darken-absolute\'><div class=\'darken-title-1\'>");
	}
	elseif ($darken_title ==2) {
		$darken_class = ("<div class=\'darken-absolute\'><div class=\'darken-title-2\'>");
	}
}

else {
		$darken_class = NULL;
}
	
	print $darken_class;

?>
';
  $handler->display->display_options['fields']['php_2']['use_php_click_sortable'] = '0';
  $handler->display->display_options['fields']['php_2']['php_click_sortable'] = '';
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = '';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '[php_2] <div class="container"><div class="main-page-title"><h1>[php]</h1></div></div>[field_darken_title_1]
<div class="[php_1]">
<div class="slider_title">
<h3>[field_title]</h3>
</div>
<div class="slider_desc">
[field_slider_description]</div>
[field_video_link]
<div class="main-page-button">[field_slider_button]</div>
</div>
';
  $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['nothing']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['nothing']['hide_alter_empty'] = TRUE;
  /* Contextual filter: Content: Nid */
  $handler->display->display_options['arguments']['nid']['id'] = 'nid';
  $handler->display->display_options['arguments']['nid']['table'] = 'node';
  $handler->display->display_options['arguments']['nid']['field'] = 'nid';
  $handler->display->display_options['arguments']['nid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['nid']['default_argument_type'] = 'node';
  $handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '25';
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'page' => 'page',
    'main_page' => 'main_page',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Field: Image (field_image:fid) */
  $handler->display->display_options['filters']['field_image_fid']['id'] = 'field_image_fid';
  $handler->display->display_options['filters']['field_image_fid']['table'] = 'field_data_field_image';
  $handler->display->display_options['filters']['field_image_fid']['field'] = 'field_image_fid';
  $handler->display->display_options['filters']['field_image_fid']['relationship'] = 'field_top_slider_value';
  $handler->display->display_options['filters']['field_image_fid']['operator'] = 'not empty';

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $export['main_page_top_slider'] = $view;

  return $export;
}
