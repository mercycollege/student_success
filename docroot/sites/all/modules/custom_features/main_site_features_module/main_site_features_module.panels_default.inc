<?php
/**
 * @file
 * main_site_features_module.panels_default.inc
 */

/**
 * Implements hook_default_panels_mini().
 */
function main_site_features_module_default_panels_mini() {
  $export = array();

  $mini = new stdClass();
  $mini->disabled = FALSE; /* Edit this to true to make a default mini disabled initially */
  $mini->api_version = 1;
  $mini->name = 'mini_panel_top_slider';
  $mini->category = '';
  $mini->admin_title = 'Mini Panel Top Slider';
  $mini->admin_description = 'A mini panel to house top slider.';
  $mini->requiredcontexts = array();
  $mini->contexts = array();
  $mini->relationships = array();
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '232418a9-0f7e-49eb-807b-8585aa2a44a7';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-5b32adff-5b2e-4ced-aecc-f3605f0e6fed';
    $pane->panel = 'middle';
    $pane->type = 'views';
    $pane->subtype = 'main_page_top_slider';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '0',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block',
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'main-site',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '5b32adff-5b2e-4ced-aecc-f3605f0e6fed';
    $display->content['new-5b32adff-5b2e-4ced-aecc-f3605f0e6fed'] = $pane;
    $display->panels['middle'][0] = 'new-5b32adff-5b2e-4ced-aecc-f3605f0e6fed';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $mini->display = $display;
  $export['mini_panel_top_slider'] = $mini;

  return $export;
}
