<?php
/**
 * @file
 * main_site_features_module.default_field_validation_rules.inc
 */

/**
 * Implements hook_default_field_validation_rule().
 */
function main_site_features_module_default_field_validation_rule() {
  $export = array();

  $rule = new stdClass();
  $rule->disabled = FALSE; /* Edit this to true to make a default rule disabled initially */
  $rule->api_version = 2;
  $rule->rulename = 'Minimun Character Number';
  $rule->name = 'minimun_character_number';
  $rule->field_name = 'field_content_description';
  $rule->col = 'value';
  $rule->entity_type = 'field_collection_item';
  $rule->bundle = 'field_pdf_uploader';
  $rule->validator = 'field_validation_length_validator';
  $rule->settings = array(
    'min' => '60',
    'max' => '',
    'bypass' => 0,
    'roles' => array(
      1 => 0,
      2 => 0,
      3 => 0,
      4 => 0,
      27 => 0,
      5 => 0,
      7 => 0,
      21 => 0,
      22 => 0,
      23 => 0,
      24 => 0,
      26 => 0,
      28 => 0,
    ),
    'errors' => 0,
  );
  $rule->error_message = '[field-name] must be at least 60 Character';
  $export['minimun_character_number'] = $rule;

  return $export;
}
