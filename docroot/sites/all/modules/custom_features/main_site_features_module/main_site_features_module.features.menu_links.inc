<?php
/**
 * @file
 * main_site_features_module.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function main_site_features_module_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-main-menu-secondary-links_athletics:http://www.mercyathletics.com
  $menu_links['menu-main-menu-secondary-links_athletics:http://www.mercyathletics.com'] = array(
    'menu_name' => 'menu-main-menu-secondary-links',
    'link_path' => 'http://www.mercyathletics.com',
    'router_path' => '',
    'link_title' => 'Athletics',
    'options' => array(
      'identifier' => 'menu-main-menu-secondary-links_athletics:http://www.mercyathletics.com',
      'icon' => array(
        'icon' => '',
        'position' => 'title_before',
        'title_wrapper_element' => 'span',
        'title_wrapper_class' => 'title',
        'breadcrumb' => 0,
        'title_wrapper' => 0,
      ),
      'attributes' => array(
        'title' => 'Athletics',
        'target' => '_blank',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Athletics');


  return $menu_links;
}
