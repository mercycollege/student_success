<?php
/**
 * @file
 * main_site_features_module.ldap_servers.inc
 */

/**
 * Implements hook_default_ldap_servers().
 */
function main_site_features_module_default_ldap_servers() {
  $export = array();

  $ldap_servers_conf = new stdClass();
  $ldap_servers_conf->disabled = FALSE; /* Edit this to true to make a default ldap_servers_conf disabled initially */
  $ldap_servers_conf->api_version = 1;
  $ldap_servers_conf->sid = 'mercydfdc001';
  $ldap_servers_conf->name = 'Mercy Auth';
  $ldap_servers_conf->status = TRUE;
  $ldap_servers_conf->ldap_type = 'ad';
  $ldap_servers_conf->address = 'ldap://172.23.161.112';
  $ldap_servers_conf->port = 389;
  $ldap_servers_conf->tls = FALSE;
  $ldap_servers_conf->bind_method = TRUE;
  $ldap_servers_conf->binddn = 'CN=Mentor_Lookup,CN=Users,DC=mercy,DC=local';
  $ldap_servers_conf->bindpw = 'Zimpleneedszimpleconcept[]1!';
  $ldap_servers_conf->basedn = array(
    0 => 'OU=Domain Users,DC=mercy,DC=local',
  );
  $ldap_servers_conf->user_attr = 'sAMAccountName';
  $ldap_servers_conf->account_name_attr = '';
  $ldap_servers_conf->mail_attr = 'mail';
  $ldap_servers_conf->mail_template = '';
  $ldap_servers_conf->allow_conflicting_drupal_accts = TRUE;
  $ldap_servers_conf->unique_persistent_attr = '';
  $ldap_servers_conf->user_dn_expression = '';
  $ldap_servers_conf->ldap_to_drupal_user = '';
  $ldap_servers_conf->testing_drupal_username = '';
  $ldap_servers_conf->group_object_category = '';
  $ldap_servers_conf->search_pagination = FALSE;
  $ldap_servers_conf->search_page_size = 1000;
  $ldap_servers_conf->weight = 0;
  $export['mercydfdc001'] = $ldap_servers_conf;

  return $export;
}
