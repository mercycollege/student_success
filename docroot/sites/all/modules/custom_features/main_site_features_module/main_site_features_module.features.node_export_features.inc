<?php
/**
 * @file
 * main_site_features_module.features.node_export_features.inc
 */

/**
 * Implements hook_node_export_features_default().
 */
function main_site_features_module_node_export_features_default() {
  $node_export = array(
  'code_string' => 'array(
  (object) array(
      \'vid\' => \'44\',
      \'uid\' => \'5\',
      \'title\' => \'404 Page Error\',
      \'log\' => \'\',
      \'status\' => \'1\',
      \'comment\' => \'1\',
      \'promote\' => \'0\',
      \'sticky\' => \'0\',
      \'vuuid\' => \'a852df9f-0ee7-4f87-bbe5-291fe229e794\',
      \'nid\' => \'44\',
      \'type\' => \'page\',
      \'language\' => \'en\',
      \'created\' => \'1397144567\',
      \'changed\' => \'1397874332\',
      \'tnid\' => \'0\',
      \'translate\' => \'0\',
      \'uuid\' => \'7064e792-5587-4020-89f1-94105ce3d887\',
      \'revision_timestamp\' => \'1397874332\',
      \'revision_uid\' => \'5\',
      \'field_slider_images\' => array(),
      \'field_slider_title\' => array(),
      \'field_slider_description\' => array(),
      \'rdf_mapping\' => array(
        \'rdftype\' => array(
          \'foaf:Document\',
        ),
        \'title\' => array(
          \'predicates\' => array(
            \'dc:title\',
          ),
        ),
        \'created\' => array(
          \'predicates\' => array(
            \'dc:date\',
            \'dc:created\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
        \'changed\' => array(
          \'predicates\' => array(
            \'dc:modified\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
        \'body\' => array(
          \'predicates\' => array(
            \'content:encoded\',
          ),
        ),
        \'uid\' => array(
          \'predicates\' => array(
            \'sioc:has_creator\',
          ),
          \'type\' => \'rel\',
        ),
        \'name\' => array(
          \'predicates\' => array(
            \'foaf:name\',
          ),
        ),
        \'comment_count\' => array(
          \'predicates\' => array(
            \'sioc:num_replies\',
          ),
          \'datatype\' => \'xsd:integer\',
        ),
        \'last_activity\' => array(
          \'predicates\' => array(
            \'sioc:last_activity_date\',
          ),
          \'datatype\' => \'xsd:dateTime\',
          \'callback\' => \'date_iso8601\',
        ),
      ),
      \'panelizer\' => array(
        \'page_manager\' => (object) array(
            \'entity_type\' => \'node\',
            \'entity_id\' => \'44\',
            \'revision_id\' => \'44\',
            \'name\' => NULL,
            \'no_blocks\' => \'0\',
            \'css_id\' => \'\',
            \'css\' => \'\',
            \'pipeline\' => \'ipe\',
            \'contexts\' => array(),
            \'relationships\' => array(),
            \'did\' => \'32\',
            \'view_mode\' => \'page_manager\',
            \'css_class\' => \'\',
            \'title_element\' => \'H2\',
            \'link_to_entity\' => \'1\',
            \'extra\' => array(),
            \'display\' => panels_display::__set_state(array(
               \'args\' => 
              array (
              ),
               \'content\' => 
              array (
                263 => 
                stdClass::__set_state(array(
                   \'pid\' => \'263\',
                   \'did\' => \'32\',
                   \'panel\' => \'middle\',
                   \'type\' => \'entity_field\',
                   \'subtype\' => \'node:body\',
                   \'shown\' => \'1\',
                   \'access\' => 
                  array (
                  ),
                   \'configuration\' => 
                  array (
                    \'label\' => \'title\',
                    \'formatter\' => \'text_default\',
                    \'delta_limit\' => 0,
                    \'delta_offset\' => \'0\',
                    \'delta_reversed\' => false,
                    \'formatter_settings\' => 
                    array (
                    ),
                    \'context\' => \'panelizer\',
                    \'override_title\' => 1,
                    \'override_title_text\' => \'\',
                  ),
                   \'cache\' => 
                  array (
                  ),
                   \'style\' => 
                  array (
                    \'settings\' => NULL,
                  ),
                   \'css\' => 
                  array (
                  ),
                   \'extras\' => 
                  array (
                  ),
                   \'position\' => \'0\',
                   \'locks\' => 
                  array (
                  ),
                   \'uuid\' => \'321a9ed9-2974-402f-a208-ef3351697d5f\',
                )),
              ),
               \'panels\' => 
              array (
                \'middle\' => 
                array (
                  0 => \'263\',
                ),
              ),
               \'incoming_content\' => NULL,
               \'css_id\' => NULL,
               \'context\' => 
              array (
              ),
               \'did\' => \'32\',
               \'renderer\' => \'standard\',
               \'layout\' => \'threecol_25_50_25\',
               \'layout_settings\' => 
              array (
              ),
               \'panel_settings\' => 
              array (
                \'style_settings\' => 
                array (
                  \'default\' => NULL,
                  \'center\' => NULL,
                  \'top\' => NULL,
                  \'left\' => NULL,
                  \'middle\' => NULL,
                  \'right\' => NULL,
                  \'bottom\' => NULL,
                ),
              ),
               \'cache\' => 
              array (
              ),
               \'title\' => \'%node:title\',
               \'hide_title\' => \'0\',
               \'title_pane\' => \'0\',
               \'uuid\' => \'2aa4942f-9d75-41cd-91c4-4b799ff01bc7\',
            )),
          ),
        \'default\' => (object) array(
            \'entity_type\' => \'node\',
            \'entity_id\' => \'44\',
            \'revision_id\' => \'44\',
            \'name\' => NULL,
            \'no_blocks\' => \'0\',
            \'css_id\' => \'\',
            \'css\' => \'\',
            \'pipeline\' => \'ipe\',
            \'contexts\' => NULL,
            \'relationships\' => NULL,
            \'did\' => \'40\',
            \'view_mode\' => \'default\',
            \'css_class\' => \'\',
            \'title_element\' => \'H2\',
            \'link_to_entity\' => \'0\',
            \'extra\' => NULL,
            \'display\' => panels_display::__set_state(array(
               \'args\' => 
              array (
              ),
               \'content\' => 
              array (
                298 => 
                stdClass::__set_state(array(
                   \'pid\' => \'298\',
                   \'did\' => \'40\',
                   \'panel\' => \'middle\',
                   \'type\' => \'custom\',
                   \'subtype\' => \'custom\',
                   \'shown\' => \'1\',
                   \'access\' => 
                  array (
                  ),
                   \'configuration\' => 
                  array (
                    \'admin_title\' => \'\',
                    \'title\' => \'404 Page Error\',
                    \'body\' => \'<p>The page that you are looking for does not exist, please try again.</p>\',
                    \'format\' => \'filtered_html\',
                    \'substitute\' => 1,
                  ),
                   \'cache\' => 
                  array (
                  ),
                   \'style\' => 
                  array (
                    \'settings\' => 
                    array (
                      \'modifier_class\' => \'\',
                      \'collapsible\' => 0,
                      \'collapsed\' => 0,
                      \'hide_classes\' => 
                      array (
                        \'hidden-xs\' => 0,
                        \'hidden-sm\' => 0,
                        \'hidden-md\' => 0,
                        \'hidden-lg\' => 0,
                      ),
                      \'additional_classes\' => 
                      array (
                        \'pull-left\' => 0,
                        \'pull-right\' => 0,
                        \'clearfix\' => 0,
                      ),
                    ),
                    \'style\' => \'bootstrap_styles:panel\',
                  ),
                   \'css\' => 
                  array (
                  ),
                   \'extras\' => 
                  array (
                  ),
                   \'position\' => \'0\',
                   \'locks\' => 
                  array (
                  ),
                   \'uuid\' => \'9ef80b15-ba1a-4de4-970a-1585a31a010b\',
                )),
              ),
               \'panels\' => 
              array (
                \'middle\' => 
                array (
                  0 => \'298\',
                ),
              ),
               \'incoming_content\' => NULL,
               \'css_id\' => NULL,
               \'context\' => 
              array (
              ),
               \'did\' => \'40\',
               \'renderer\' => \'standard\',
               \'layout\' => \'threecol_33_34_33_stacked\',
               \'layout_settings\' => 
              array (
              ),
               \'panel_settings\' => 
              array (
                \'style_settings\' => 
                array (
                  \'default\' => NULL,
                  \'top\' => NULL,
                  \'left\' => NULL,
                  \'middle\' => NULL,
                  \'right\' => NULL,
                  \'bottom\' => NULL,
                ),
              ),
               \'cache\' => 
              array (
              ),
               \'title\' => \'No Title\',
               \'hide_title\' => \'1\',
               \'title_pane\' => \'291\',
               \'uuid\' => \'38e2aaa9-5fee-47d5-83e0-f2b859b40345\',
            )),
          ),
      ),
      \'cid\' => \'0\',
      \'last_comment_timestamp\' => \'1397144567\',
      \'last_comment_name\' => NULL,
      \'last_comment_uid\' => \'5\',
      \'comment_count\' => \'0\',
      \'name\' => \'jgarcia\',
      \'picture\' => \'4\',
      \'data\' => \'a:3:{s:19:"ldap_authentication";a:1:{s:4:"init";a:3:{s:3:"sid";s:12:"mercydfdc001";s:2:"dn";s:67:"CN=axiomrunner,OU=VPN Users,OU=Domain Users,DC=mercy,DC=local";s:4:"mail";s:17:"webmaster@mercy.edu";}}s:7:"overlay";i:1;s:7:"contact";i:1;}\',
      \'path\' => array(
        \'pid\' => \'57\',
        \'source\' => \'node/44\',
        \'alias\' => \'content/404-page-error\',
        \'language\' => \'en\',
      ),
      \'menu\' => NULL,
      \'node_export_drupal_version\' => \'7\',
    ),
)',
);
  return $node_export;
}
