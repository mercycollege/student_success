<?php
/**
 * @file
 * main_site_features_module.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function main_site_features_module_user_default_roles() {
  $roles = array();

  // Exported role: Academics.
  $roles['Academics'] = array(
    'name' => 'Academics',
    'weight' => 26,
  );

  // Exported role: Admissions.
  $roles['Admissions'] = array(
    'name' => 'Admissions',
    'weight' => 36,
  );

  // Exported role: Alumni Office.
  $roles['Alumni Office'] = array(
    'name' => 'Alumni Office',
    'weight' => 43,
  );

  // Exported role: Content Administrator.
  $roles['Content Administrator'] = array(
    'name' => 'Content Administrator',
    'weight' => 7,
  );

  // Exported role: Content Provider.
  $roles['Content Provider'] = array(
    'name' => 'Content Provider',
    'weight' => 5,
  );

  // Exported role: Department of Communication and the Arts.
  $roles['Department of Communication and the Arts'] = array(
    'name' => 'Department of Communication and the Arts',
    'weight' => 31,
  );

  // Exported role: Department of Humanities.
  $roles['Department of Humanities'] = array(
    'name' => 'Department of Humanities',
    'weight' => 33,
  );

  // Exported role: Department of Literature and Language.
  $roles['Department of Literature and Language'] = array(
    'name' => 'Department of Literature and Language',
    'weight' => 32,
  );

  // Exported role: Department of Mathematics and Computer Science.
  $roles['Department of Mathematics and Computer Science'] = array(
    'name' => 'Department of Mathematics and Computer Science',
    'weight' => 34,
  );

  // Exported role: Faculty Center for Teaching and Learning.
  $roles['Faculty Center for Teaching and Learning'] = array(
    'name' => 'Faculty Center for Teaching and Learning',
    'weight' => 45,
  );

  // Exported role: Global Engagement.
  $roles['Global Engagement'] = array(
    'name' => 'Global Engagement',
    'weight' => 38,
  );

  // Exported role: HEOP.
  $roles['HEOP'] = array(
    'name' => 'HEOP',
    'weight' => 44,
  );

  // Exported role: Human Resources.
  $roles['Human Resources'] = array(
    'name' => 'Human Resources',
    'weight' => 22,
  );

  // Exported role: Information Technology.
  $roles['Information Technology'] = array(
    'name' => 'Information Technology',
    'weight' => 27,
  );

  // Exported role: Library.
  $roles['Library'] = array(
    'name' => 'Library',
    'weight' => 21,
  );

  // Exported role: Mercy Home Page.
  $roles['Mercy Home Page'] = array(
    'name' => 'Mercy Home Page',
    'weight' => 35,
  );

  // Exported role: Mercy Online.
  $roles['Mercy Online'] = array(
    'name' => 'Mercy Online',
    'weight' => 46,
  );

  // Exported role: Office of the Provost.
  $roles['Office of the Provost'] = array(
    'name' => 'Office of the Provost',
    'weight' => 47,
  );

  // Exported role: PACT.
  $roles['PACT'] = array(
    'name' => 'PACT',
    'weight' => 28,
  );

  // Exported role: Public Relations.
  $roles['Public Relations'] = array(
    'name' => 'Public Relations',
    'weight' => 4,
  );

  // Exported role: Registrar.
  $roles['Registrar'] = array(
    'name' => 'Registrar',
    'weight' => 24,
  );

  // Exported role: Residential Life.
  $roles['Residential Life'] = array(
    'name' => 'Residential Life',
    'weight' => 23,
  );

  // Exported role: Safety and Security.
  $roles['Safety and Security'] = array(
    'name' => 'Safety and Security',
    'weight' => 48,
  );

  // Exported role: School of Business.
  $roles['School of Business'] = array(
    'name' => 'School of Business',
    'weight' => 39,
  );

  // Exported role: School of Education.
  $roles['School of Education'] = array(
    'name' => 'School of Education',
    'weight' => 37,
  );

  // Exported role: School of Health and Natural Science.
  $roles['School of Health and Natural Science'] = array(
    'name' => 'School of Health and Natural Science',
    'weight' => 29,
  );

  // Exported role: School of Liberal Arts.
  $roles['School of Liberal Arts'] = array(
    'name' => 'School of Liberal Arts',
    'weight' => 30,
  );

  // Exported role: School of Social and Behaviorial Sciences.
  $roles['School of Social and Behaviorial Sciences'] = array(
    'name' => 'School of Social and Behaviorial Sciences',
    'weight' => 40,
  );

  // Exported role: Student Affairs.
  $roles['Student Affairs'] = array(
    'name' => 'Student Affairs',
    'weight' => 41,
  );

  // Exported role: Student Services Support Center.
  $roles['Student Services Support Center'] = array(
    'name' => 'Student Services Support Center',
    'weight' => 49,
  );

  // Exported role: Visit Mercy.
  $roles['Visit Mercy'] = array(
    'name' => 'Visit Mercy',
    'weight' => 42,
  );

  // Exported role: Webmaster.
  $roles['Webmaster'] = array(
    'name' => 'Webmaster',
    'weight' => 3,
  );

  // Exported role: administrator.
  $roles['administrator'] = array(
    'name' => 'administrator',
    'weight' => 2,
  );

  return $roles;
}
