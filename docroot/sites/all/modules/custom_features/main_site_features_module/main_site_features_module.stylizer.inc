<?php
/**
 * @file
 * main_site_features_module.stylizer.inc
 */

/**
 * Implements hook_default_stylizer().
 */
function main_site_features_module_default_stylizer() {
  $export = array();

  $style = new stdClass();
  $style->disabled = FALSE; /* Edit this to true to make a default style disabled initially */
  $style->api_version = 1;
  $style->name = 'mercy_classic_style';
  $style->admin_title = 'Mercy Classic Style';
  $style->admin_description = '';
  $style->settings = array(
    'name' => 'mercy_classic_style',
    'style_base' => 'pane_plain_box',
    'palette' => array(
      'background' => '#FFF',
      'text' => '#002b55',
      'border' => '#000000',
      'header-background' => '#FFF',
      'header-text' => '#002b55',
      'header-border' => '#000000',
    ),
    'font' => array(
      'font' => '',
      'size' => '',
      'letter_spacing' => '',
      'word_spacing' => '',
      'decoration' => '',
      'weight' => '',
      'style' => '',
      'variant' => '',
      'case' => '',
      'alignment' => '',
    ),
    'header_font' => array(
      'font' => '',
      'size' => '',
      'letter_spacing' => '',
      'word_spacing' => '',
      'decoration' => '',
      'weight' => '',
      'style' => '',
      'variant' => '',
      'case' => '',
      'alignment' => '',
    ),
    'border' => array(
      'thickness' => '',
      'style' => '',
    ),
    'header_border' => array(
      'thickness' => '',
      'style' => '',
    ),
    'padding' => array(
      'top' => '0.1em',
      'right' => '0.5em',
      'bottom' => '0.5em',
      'left' => '0.5em',
    ),
  );
  $export['mercy_classic_style'] = $style;

  return $export;
}
