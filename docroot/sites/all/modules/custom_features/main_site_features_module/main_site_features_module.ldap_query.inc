<?php
/**
 * @file
 * main_site_features_module.ldap_query.inc
 */

/**
 * Implements hook_default_ldap_query().
 */
function main_site_features_module_default_ldap_query() {
  $export = array();

  $qid = new stdClass();
  $qid->disabled = FALSE; /* Edit this to true to make a default qid disabled initially */
  $qid->api_version = 1;
  $qid->qid = 'mercydfdc001';
  $qid->name = 'LDAP Query';
  $qid->sid = 'mercydfdc001';
  $qid->status = TRUE;
  $qid->base_dn_str = 'OU=Domain Users,DC=mercy,DC=local
OU=VPN Users,OU=Domain Users,DC=mercy,DC=local
OU=Bronx,OU=Domain Users,DC=mercy,DC=local
OU=Dobbs-Ferry,OU=Domain Users,DC=mercy,DC=local
OU=Manhattan,OU=Domain Users,DC=mercy,DC=local
OU=York-Town,OU=Domain Users,DC=mercy,DC=local
OU=White-Plains,OU=Domain Users,DC=mercy,DC=local
OU=IPK Users,OU=Domain Users,DC=mercy,DC=local
OU=TEMP OU,OU=Domain Users,DC=mercy,DC=local
OU=Adjuncts,OU=Domain Users,DC=mercy,DC=local
OU=adjuncts,OU=ExpiredPasswordAge,OU=Domain Users,DC=mercy,DC=local
OU=users,OU=ExpiredPasswordAge,OU=Domain Users,DC=mercy,DC=local';
  $qid->filter = '(&(objectClass=user)(objectCategory=person)(!(MSExchHideFromAddressLists=TRUE))(!(flags=3))(!(flags=2))(!(flags=9))(!(flags=7))(!(userAccountControl:1.2.840.113556.1.4.803:=2)))';
  $qid->attributes_str = 'samaccountname,displayname,title,telephonenumber,mail,department,streetaddress,l,physicaldeliveryofficename,st,postalcode,givenName,sn,flags,employeeid';
  $qid->sizelimit = 0;
  $qid->timelimit = 0;
  $qid->deref = FALSE;
  $qid->scope = TRUE;
  $export['mercydfdc001'] = $qid;

  return $export;
}
