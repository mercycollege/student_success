<?php
/**
 * @file
 * mercy_users.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function mercy_users_user_default_roles() {
  $roles = array();

  // Exported role: Academics.
  $roles['Academics'] = array(
    'name' => 'Academics',
    'weight' => 15,
  );

  // Exported role: Admission Applications.
  $roles['Admission Applications'] = array(
    'name' => 'Admission Applications',
    'weight' => 42,
  );

  // Exported role: Admissions.
  $roles['Admissions'] = array(
    'name' => 'Admissions',
    'weight' => 10,
  );

  // Exported role: Alumni Office.
  $roles['Alumni Office'] = array(
    'name' => 'Alumni Office',
    'weight' => 29,
  );

  // Exported role: Athletics & Recreation.
  $roles['Athletics & Recreation'] = array(
    'name' => 'Athletics & Recreation',
    'weight' => 43,
  );

  // Exported role: Career Services.
  $roles['Career Services'] = array(
    'name' => 'Career Services',
    'weight' => 38,
  );

  // Exported role: Content Administrator.
  $roles['Content Administrator'] = array(
    'name' => 'Content Administrator',
    'weight' => 7,
  );

  // Exported role: Content Provider.
  $roles['Content Provider'] = array(
    'name' => 'Content Provider',
    'weight' => 6,
  );

  // Exported role: DASA.
  $roles['DASA'] = array(
    'name' => 'DASA',
    'weight' => 40,
  );

  // Exported role: Degree and Program Administrator.
  $roles['Degree and Program Administrator'] = array(
    'name' => 'Degree and Program Administrator',
    'weight' => 49,
  );

  // Exported role: Department of Communication and the Arts.
  $roles['Department of Communication and the Arts'] = array(
    'name' => 'Department of Communication and the Arts',
    'weight' => 19,
  );

  // Exported role: Department of Humanities.
  $roles['Department of Humanities'] = array(
    'name' => 'Department of Humanities',
    'weight' => 21,
  );

  // Exported role: Department of Literature and Language.
  $roles['Department of Literature and Language'] = array(
    'name' => 'Department of Literature and Language',
    'weight' => 20,
  );

  // Exported role: Department of Mathematics and Computer Science.
  $roles['Department of Mathematics and Computer Science'] = array(
    'name' => 'Department of Mathematics and Computer Science',
    'weight' => 22,
  );

  // Exported role: Events Coordinator.
  $roles['Events Coordinator'] = array(
    'name' => 'Events Coordinator',
    'weight' => 37,
  );

  // Exported role: Faculty Center for Teaching and Learning.
  $roles['Faculty Center for Teaching and Learning'] = array(
    'name' => 'Faculty Center for Teaching and Learning',
    'weight' => 31,
  );

  // Exported role: Global Engagement.
  $roles['Global Engagement'] = array(
    'name' => 'Global Engagement',
    'weight' => 24,
  );

  // Exported role: Graduate Decision Form.
  $roles['Graduate Decision Form'] = array(
    'name' => 'Graduate Decision Form',
    'weight' => 39,
  );

  // Exported role: HEOP.
  $roles['HEOP'] = array(
    'name' => 'HEOP',
    'weight' => 30,
  );

  // Exported role: Honors.
  $roles['Honors'] = array(
    'name' => 'Honors',
    'weight' => 44,
  );

  // Exported role: Human Resources.
  $roles['Human Resources'] = array(
    'name' => 'Human Resources',
    'weight' => 12,
  );

  // Exported role: Inauguration.
  $roles['Inauguration'] = array(
    'name' => 'Inauguration',
    'weight' => 48,
  );

  // Exported role: Information Technology.
  $roles['Information Technology'] = array(
    'name' => 'Information Technology',
    'weight' => 3,
  );

  // Exported role: Institutional Effectiveness.
  $roles['Institutional Effectiveness'] = array(
    'name' => 'Institutional Effectiveness',
    'weight' => 47,
  );

  // Exported role: Institutional Research.
  $roles['Institutional Research'] = array(
    'name' => 'Institutional Research',
    'weight' => 46,
  );

  // Exported role: Library.
  $roles['Library'] = array(
    'name' => 'Library',
    'weight' => 8,
  );

  // Exported role: Marketing.
  $roles['Marketing'] = array(
    'name' => 'Marketing',
    'weight' => 0,
  );

  // Exported role: Mercy Home Page.
  $roles['Mercy Home Page'] = array(
    'name' => 'Mercy Home Page',
    'weight' => 9,
  );

  // Exported role: Mercy Online.
  $roles['Mercy Online'] = array(
    'name' => 'Mercy Online',
    'weight' => 32,
  );

  // Exported role: Office of the Provost.
  $roles['Office of the Provost'] = array(
    'name' => 'Office of the Provost',
    'weight' => 33,
  );

  // Exported role: PACT.
  $roles['PACT'] = array(
    'name' => 'PACT',
    'weight' => 16,
  );

  // Exported role: Public Relations.
  $roles['Public Relations'] = array(
    'name' => 'Public Relations',
    'weight' => 5,
  );

  // Exported role: Purchasing.
  $roles['Purchasing'] = array(
    'name' => 'Purchasing',
    'weight' => 36,
  );

  // Exported role: Registrar.
  $roles['Registrar'] = array(
    'name' => 'Registrar',
    'weight' => 14,
  );

  // Exported role: Residential Life.
  $roles['Residential Life'] = array(
    'name' => 'Residential Life',
    'weight' => 13,
  );

  // Exported role: Safety and Security.
  $roles['Safety and Security'] = array(
    'name' => 'Safety and Security',
    'weight' => 34,
  );

  // Exported role: School of Business.
  $roles['School of Business'] = array(
    'name' => 'School of Business',
    'weight' => 25,
  );

  // Exported role: School of Education.
  $roles['School of Education'] = array(
    'name' => 'School of Education',
    'weight' => 23,
  );

  // Exported role: School of Education - MISTI.
  $roles['School of Education - MISTI'] = array(
    'name' => 'School of Education - MISTI',
    'weight' => 45,
  );

  // Exported role: School of Health and Natural Science.
  $roles['School of Health and Natural Science'] = array(
    'name' => 'School of Health and Natural Science',
    'weight' => 17,
  );

  // Exported role: School of Liberal Arts.
  $roles['School of Liberal Arts'] = array(
    'name' => 'School of Liberal Arts',
    'weight' => 18,
  );

  // Exported role: School of Social and Behaviorial Sciences.
  $roles['School of Social and Behaviorial Sciences'] = array(
    'name' => 'School of Social and Behaviorial Sciences',
    'weight' => 26,
  );

  // Exported role: Student Affairs.
  $roles['Student Affairs'] = array(
    'name' => 'Student Affairs',
    'weight' => 27,
  );

  // Exported role: Student Dashboard.
  $roles['Student Dashboard'] = array(
    'name' => 'Student Dashboard',
    'weight' => 41,
  );

  // Exported role: Student Services Support Center.
  $roles['Student Services Support Center'] = array(
    'name' => 'Student Services Support Center',
    'weight' => 35,
  );

  // Exported role: Visit Mercy.
  $roles['Visit Mercy'] = array(
    'name' => 'Visit Mercy',
    'weight' => 28,
  );

  // Exported role: Webmaster.
  $roles['Webmaster'] = array(
    'name' => 'Webmaster',
    'weight' => 4,
  );

  // Exported role: administrator.
  $roles['administrator'] = array(
    'name' => 'administrator',
    'weight' => 2,
  );

  return $roles;
}
