<?php
/**
 * @file
 * mercy_users.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function mercy_users_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'user_import';
  $feeds_importer->config = array(
    'name' => 'user_import',
    'description' => 'Imports basic users\' information from AD',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'txt csv tsv xml opml',
        'direct' => 0,
        'directory' => 'private://feeds/users-export',
        'allowed_schemes' => array(
          'public' => 'public',
          'private' => 'private',
        ),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsUserProcessor',
      'config' => array(
        'roles' => array(
          68 => 0,
          3 => 0,
          28 => 0,
          4 => 0,
          27 => 0,
          5 => 0,
          7 => 0,
          21 => 0,
          36 => 0,
          37 => 0,
          22 => 0,
          23 => 0,
          24 => 0,
          26 => 0,
          29 => 0,
          30 => 0,
          31 => 0,
          32 => 0,
          33 => 0,
          34 => 0,
          35 => 0,
          38 => 0,
          39 => 0,
          40 => 0,
          41 => 0,
          42 => 0,
          43 => 0,
          44 => 0,
          45 => 0,
          46 => 0,
          47 => 0,
          48 => 0,
          49 => 0,
          50 => 0,
          51 => 0,
          52 => 0,
          53 => 0,
          54 => 0,
          56 => 0,
          57 => 0,
          58 => 0,
          59 => 0,
          60 => 0,
          61 => 0,
          63 => 0,
          64 => 0,
          65 => 0,
          66 => 0,
        ),
        'status' => '1',
        'defuse_mail' => 0,
        'mappings' => array(
          0 => array(
            'source' => 'User name',
            'target' => 'name',
            'unique' => 1,
          ),
          1 => array(
            'source' => 'Mail',
            'target' => 'mail',
            'unique' => 1,
          ),
          2 => array(
            'source' => 'Name',
            'target' => 'field_name',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'Title',
            'target' => 'field_title',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'Telephone',
            'target' => 'field_office_number',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'Mail',
            'target' => 'field_email',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'Street Address',
            'target' => 'field_adress',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'State',
            'target' => 'field_state',
            'unique' => FALSE,
          ),
          8 => array(
            'source' => 'Zip Code',
            'target' => 'field_zip_code',
            'unique' => FALSE,
          ),
          9 => array(
            'source' => 'Location',
            'target' => 'field_city',
            'unique' => FALSE,
          ),
          10 => array(
            'source' => 'Office',
            'target' => 'field_office',
            'unique' => FALSE,
          ),
          11 => array(
            'source' => 'Flags',
            'target' => 'field_flags',
            'unique' => FALSE,
          ),
          12 => array(
            'source' => 'Department',
            'target' => 'field_school_or_department',
            'unique' => FALSE,
          ),
          13 => array(
            'source' => 'Name and Title',
            'target' => 'field_name_and_title',
            'unique' => FALSE,
          ),
          14 => array(
            'source' => 'Department URL',
            'target' => 'field_department_url',
            'unique' => FALSE,
          ),
          15 => array(
            'source' => 'Last Name',
            'target' => 'field_common_name',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '2',
        'input_format' => 'plain_text',
        'skip_hash_check' => 1,
        'bundle' => 'user',
        'update_non_existent' => 'skip',
        'user_roles_create' => 1,
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '900',
    'expire_period' => 3600,
    'import_on_create' => 0,
    'process_in_background' => 1,
  );
  $export['user_import'] = $feeds_importer;

  return $export;
}
