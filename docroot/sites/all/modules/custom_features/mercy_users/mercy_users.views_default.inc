<?php
/**
 * @file
 * mercy_users.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function mercy_users_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'user_profile';
  $view->description = 'Users from AD. You must define a ldap data source whith ad_users identifier, and mail, name and samaccountname attributes';
  $view->tag = 'default';
  $view->base_table = 'ldap';
  $view->human_name = 'User Profile';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'User Profile';
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['qid'] = 'mercydfdc001';
  $handler->display->display_options['exposed_form']['type'] = 'input_required';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Search';
  $handler->display->display_options['exposed_form']['options']['text_input_required_format'] = 'admin_html';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: givenName */
  $handler->display->display_options['fields']['attribute_7']['id'] = 'attribute_7';
  $handler->display->display_options['fields']['attribute_7']['table'] = 'ldap';
  $handler->display->display_options['fields']['attribute_7']['field'] = 'attribute';
  $handler->display->display_options['fields']['attribute_7']['ui_name'] = 'givenName';
  $handler->display->display_options['fields']['attribute_7']['label'] = 'First Name';
  $handler->display->display_options['fields']['attribute_7']['exclude'] = TRUE;
  $handler->display->display_options['fields']['attribute_7']['hide_alter_empty'] = FALSE;
  $handler->display->display_options['fields']['attribute_7']['index_value'] = '0';
  $handler->display->display_options['fields']['attribute_7']['attribute_name'] = 'givenName';
  /* Field: sn */
  $handler->display->display_options['fields']['attribute_8']['id'] = 'attribute_8';
  $handler->display->display_options['fields']['attribute_8']['table'] = 'ldap';
  $handler->display->display_options['fields']['attribute_8']['field'] = 'attribute';
  $handler->display->display_options['fields']['attribute_8']['ui_name'] = 'sn';
  $handler->display->display_options['fields']['attribute_8']['label'] = 'Last Name';
  $handler->display->display_options['fields']['attribute_8']['exclude'] = TRUE;
  $handler->display->display_options['fields']['attribute_8']['hide_alter_empty'] = FALSE;
  $handler->display->display_options['fields']['attribute_8']['index_value'] = '0';
  $handler->display->display_options['fields']['attribute_8']['attribute_name'] = 'sn';
  /* Field: cn */
  $handler->display->display_options['fields']['cn']['id'] = 'cn';
  $handler->display->display_options['fields']['cn']['table'] = 'ldap';
  $handler->display->display_options['fields']['cn']['field'] = 'cn';
  $handler->display->display_options['fields']['cn']['ui_name'] = 'cn';
  $handler->display->display_options['fields']['cn']['label'] = 'Common Name';
  $handler->display->display_options['fields']['cn']['exclude'] = TRUE;
  $handler->display->display_options['fields']['cn']['hide_alter_empty'] = FALSE;
  $handler->display->display_options['fields']['cn']['multivalue'] = 'v-index';
  $handler->display->display_options['fields']['cn']['value_separator'] = ' ';
  $handler->display->display_options['fields']['cn']['index_value'] = '0';
  /* Field: dn */
  $handler->display->display_options['fields']['dn']['id'] = 'dn';
  $handler->display->display_options['fields']['dn']['table'] = 'ldap';
  $handler->display->display_options['fields']['dn']['field'] = 'dn';
  $handler->display->display_options['fields']['dn']['ui_name'] = 'dn';
  $handler->display->display_options['fields']['dn']['label'] = 'Distinguished Name';
  $handler->display->display_options['fields']['dn']['exclude'] = TRUE;
  $handler->display->display_options['fields']['dn']['hide_alter_empty'] = FALSE;
  $handler->display->display_options['fields']['dn']['index_value'] = '';
  /* Field: username */
  $handler->display->display_options['fields']['attribute']['id'] = 'attribute';
  $handler->display->display_options['fields']['attribute']['table'] = 'ldap';
  $handler->display->display_options['fields']['attribute']['field'] = 'attribute';
  $handler->display->display_options['fields']['attribute']['ui_name'] = 'username';
  $handler->display->display_options['fields']['attribute']['label'] = '';
  $handler->display->display_options['fields']['attribute']['exclude'] = TRUE;
  $handler->display->display_options['fields']['attribute']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['attribute']['hide_alter_empty'] = FALSE;
  $handler->display->display_options['fields']['attribute']['multivalue'] = 'v-index';
  $handler->display->display_options['fields']['attribute']['index_value'] = '0';
  $handler->display->display_options['fields']['attribute']['attribute_name'] = 'samaccountname';
  /* Field: name */
  $handler->display->display_options['fields']['attribute_1']['id'] = 'attribute_1';
  $handler->display->display_options['fields']['attribute_1']['table'] = 'ldap';
  $handler->display->display_options['fields']['attribute_1']['field'] = 'attribute';
  $handler->display->display_options['fields']['attribute_1']['ui_name'] = 'name';
  $handler->display->display_options['fields']['attribute_1']['label'] = '';
  $handler->display->display_options['fields']['attribute_1']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['attribute_1']['alter']['text'] = '<div class="design-title"><h3>[attribute_7] [attribute_8]</h3>';
  $handler->display->display_options['fields']['attribute_1']['alter']['path'] = 'users/[attribute]';
  $handler->display->display_options['fields']['attribute_1']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['attribute_1']['hide_alter_empty'] = FALSE;
  $handler->display->display_options['fields']['attribute_1']['multivalue'] = 'v-index';
  $handler->display->display_options['fields']['attribute_1']['index_value'] = '0';
  $handler->display->display_options['fields']['attribute_1']['attribute_name'] = 'samaccountname';
  /* Field: mail */
  $handler->display->display_options['fields']['attribute_2']['id'] = 'attribute_2';
  $handler->display->display_options['fields']['attribute_2']['table'] = 'ldap';
  $handler->display->display_options['fields']['attribute_2']['field'] = 'attribute';
  $handler->display->display_options['fields']['attribute_2']['ui_name'] = 'mail';
  $handler->display->display_options['fields']['attribute_2']['label'] = '';
  $handler->display->display_options['fields']['attribute_2']['exclude'] = TRUE;
  $handler->display->display_options['fields']['attribute_2']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['attribute_2']['hide_alter_empty'] = FALSE;
  $handler->display->display_options['fields']['attribute_2']['index_value'] = '0';
  $handler->display->display_options['fields']['attribute_2']['attribute_name'] = 'mail';
  /* Field: title */
  $handler->display->display_options['fields']['attribute_3']['id'] = 'attribute_3';
  $handler->display->display_options['fields']['attribute_3']['table'] = 'ldap';
  $handler->display->display_options['fields']['attribute_3']['field'] = 'attribute';
  $handler->display->display_options['fields']['attribute_3']['ui_name'] = 'title';
  $handler->display->display_options['fields']['attribute_3']['label'] = '';
  $handler->display->display_options['fields']['attribute_3']['exclude'] = TRUE;
  $handler->display->display_options['fields']['attribute_3']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['attribute_3']['hide_alter_empty'] = FALSE;
  $handler->display->display_options['fields']['attribute_3']['index_value'] = '0';
  $handler->display->display_options['fields']['attribute_3']['attribute_name'] = 'title';
  /* Field: department */
  $handler->display->display_options['fields']['attribute_4']['id'] = 'attribute_4';
  $handler->display->display_options['fields']['attribute_4']['table'] = 'ldap';
  $handler->display->display_options['fields']['attribute_4']['field'] = 'attribute';
  $handler->display->display_options['fields']['attribute_4']['ui_name'] = 'department';
  $handler->display->display_options['fields']['attribute_4']['label'] = '';
  $handler->display->display_options['fields']['attribute_4']['exclude'] = TRUE;
  $handler->display->display_options['fields']['attribute_4']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['attribute_4']['hide_alter_empty'] = FALSE;
  $handler->display->display_options['fields']['attribute_4']['index_value'] = '0';
  $handler->display->display_options['fields']['attribute_4']['attribute_name'] = 'department';
  /* Field: telephonenumber */
  $handler->display->display_options['fields']['attribute_5']['id'] = 'attribute_5';
  $handler->display->display_options['fields']['attribute_5']['table'] = 'ldap';
  $handler->display->display_options['fields']['attribute_5']['field'] = 'attribute';
  $handler->display->display_options['fields']['attribute_5']['ui_name'] = 'telephonenumber';
  $handler->display->display_options['fields']['attribute_5']['label'] = '';
  $handler->display->display_options['fields']['attribute_5']['exclude'] = TRUE;
  $handler->display->display_options['fields']['attribute_5']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['attribute_5']['hide_alter_empty'] = FALSE;
  $handler->display->display_options['fields']['attribute_5']['index_value'] = '0';
  $handler->display->display_options['fields']['attribute_5']['attribute_name'] = 'telephonenumber';
  /* Field: l */
  $handler->display->display_options['fields']['attribute_6']['id'] = 'attribute_6';
  $handler->display->display_options['fields']['attribute_6']['table'] = 'ldap';
  $handler->display->display_options['fields']['attribute_6']['field'] = 'attribute';
  $handler->display->display_options['fields']['attribute_6']['ui_name'] = 'l';
  $handler->display->display_options['fields']['attribute_6']['label'] = '';
  $handler->display->display_options['fields']['attribute_6']['exclude'] = TRUE;
  $handler->display->display_options['fields']['attribute_6']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['attribute_6']['hide_alter_empty'] = FALSE;
  $handler->display->display_options['fields']['attribute_6']['index_value'] = '0';
  $handler->display->display_options['fields']['attribute_6']['attribute_name'] = 'l';
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing_1']['id'] = 'nothing_1';
  $handler->display->display_options['fields']['nothing_1']['table'] = 'views';
  $handler->display->display_options['fields']['nothing_1']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing_1']['label'] = '';
  $handler->display->display_options['fields']['nothing_1']['alter']['text'] = '<div class="content-box">
<div class="user-profile-general-information">[attribute_6]</div>
<div class="user-profile-title">[attribute_3]</div><br>
[attribute_5]<br>
[attribute_2]

</div>';
  $handler->display->display_options['fields']['nothing_1']['element_label_colon'] = FALSE;
  /* Sort criterion: LDAP Query: LDAP Attribute */
  $handler->display->display_options['sorts']['attribute']['id'] = 'attribute';
  $handler->display->display_options['sorts']['attribute']['table'] = 'ldap';
  $handler->display->display_options['sorts']['attribute']['field'] = 'attribute';
  $handler->display->display_options['sorts']['attribute']['attribute_name'] = 'samaccountname';
  /* Contextual filter: LDAP Query: LDAP Attribute */
  $handler->display->display_options['arguments']['attribute']['id'] = 'attribute';
  $handler->display->display_options['arguments']['attribute']['table'] = 'ldap';
  $handler->display->display_options['arguments']['attribute']['field'] = 'attribute';
  $handler->display->display_options['arguments']['attribute']['title_enable'] = TRUE;
  $handler->display->display_options['arguments']['attribute']['title'] = '<div class="hidden">%1 Profile</div>';
  $handler->display->display_options['arguments']['attribute']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['attribute']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['attribute']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['attribute']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['attribute']['attribute_name'] = 'samaccountname';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'users';
  $handler->display->display_options['menu']['title'] = 'Directory';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $export['user_profile'] = $view;

  return $export;
}
