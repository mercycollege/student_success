<?php
/**
 * @file
 * mercy_config.default_instagram_social_feed_presets.inc
 */

/**
 * Implements hook_default_nitf_xml_preset().
 */
function mercy_config_default_nitf_xml_preset() {
  $export = array();

  $preset = new stdClass();
  $preset->disabled = FALSE; /* Edit this to true to make a default preset disabled initially */
  $preset->api_version = 1;
  $preset->name = 'MercyCollege Photos';
  $preset->feed_type = 2;
  $preset->search_term = '';
  $preset->auto_publish = 1;
  $preset->enabled = 1;
  $export['MercyCollege Photos'] = $preset;

  return $export;
}
