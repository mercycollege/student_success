<?php
/**
 * @file
 * mercy_config.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function mercy_config_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'alchemy_apikey';
  $strongarm->value = '756ca6a92103dbb2063aff1ab6f35e773f2d4e46';
  $export['alchemy_apikey'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'alchemy_usecurl';
  $strongarm->value = 0;
  $export['alchemy_usecurl'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'instagram_social_feed_api_key';
  $strongarm->value = '6689400.2d7bf6c.9f99207446a5410e8e0b456d411c7138';
  $export['instagram_social_feed_api_key'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'instagram_social_feed_client_id';
  $strongarm->value = '2d7bf6ce11d34e5497831b2c97df2ef6';
  $export['instagram_social_feed_client_id'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'instagram_social_feed_client_secret';
  $strongarm->value = 'c1400916bb2a4364b683feb1effc80ad';
  $export['instagram_social_feed_client_secret'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'instagram_social_feed_redirect_uri';
  $strongarm->value = 'http://mercy.mcdev/admin/config/services/instagram_social_feed/settings';
  $export['instagram_social_feed_redirect_uri'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'instagram_social_feed_username';
  $strongarm->value = 'mercycollege';
  $export['instagram_social_feed_username'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'instagram_social_feed_user_id';
  $strongarm->value = '6689400';
  $export['instagram_social_feed_user_id'] = $strongarm;

  return $export;
}
