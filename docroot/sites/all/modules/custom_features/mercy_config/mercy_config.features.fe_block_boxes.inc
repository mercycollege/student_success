<?php
/**
 * @file
 * mercy_config.features.fe_block_boxes.inc
 */

/**
 * Implements hook_default_fe_block_boxes().
 */
function mercy_config_default_fe_block_boxes() {
  $export = array();

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Dropdowns for the secondary menus';
  $fe_block_boxes->format = 'admin_html';
  $fe_block_boxes->machine_name = 'dropdowns_secondary_menus';
  $fe_block_boxes->body = '<div id ="dropdown-house">
	<div id ="about-mercy-dropdown" class="blue">
		<div class="container">
			<div class="dropdowns-styler">
				<ul>
					<li class="header"><span class="section-title">About Mercy<span></li>
					<li><a href="/about-mercy/">Overview</a></li>
					<li><a href="/about-mercy/mercy-college-profile/mercy-college-history">History</a></li>
					<li><a href="/about-mercy/mercy-college-profile/mission-statement">Mission Statement</a></li>
					<li><a href="/about-mercy/mercy-college-profile/distinctions">Distinctions</a></li>
					<li><a href="/about-mercy/mercy-college-profile/fast-facts">Fast Facts</a></li>
					<li><a href="/mercy-college-web-cam">New Residence Hall</a></li>
				</ul>
				<ul>
					<li class="header"><span class="section-title">Leadership<span></li>
					<li><a href="/about-mercy/leadership">Overview</a></li>
					<li><a href="/about-mercy/office-of-the-president">Office of the President</a></li>
                                        <li><a href="/inaug2015">Presidential Inauguration</a></li>
					<li><a href="/about-mercy/board-of-trustees">Board of Trustees</a></li>
					<li><a href="/academics/office-provost">Office of the Provost</a></li>
				</ul>
				<ul>
					<li class="header"><span class="section-title">Human Resources<span></li>
					<li><a href="/about-mercy/human-resources">Overview</a></li>
					<li><a href="/about-mercy/human-resources/prospective-employees">Prospective Employees </a></li>
					<li><a href="/about-mercy/human-resources/current-employees">Current Employees</a></li>
					<li><a href="/about-mercy/human-resources/hric">HRIC</a></li>
					<li><a href="/about-mercy/human-resources/employment-opportunities">Employment Opportunities</a></li>
				</ul>
				<ul>
					<li class="header"><span class="section-title">Campus Safety and Security<span></li>
					<li><a href="/about-mercy/campus-safety-and-security">Overview</a></li>
					<li><a href="/about-mercy/fire-safety">Fire Safety</a></li>
					<li><a href="/about-mercy/severe-weather-or-disasters">Severe Weather or Disasters</a></li>
					<li><a href="/about-mercy/safety-tips">Safety Tips</a></li>
					<li><a href="/about-mercy/campus-safety-and-security/parking">Parking</a></li>
				</ul>
				<ul>
					<li class="header"><span class="section-title">Additional Resources<span></li>
					<li><a href="/resources/information-technology">Information Technology</a></li>
					<li><a href="https://mercy.blackboard.com/" target="_blank">Blackboard</a></li>
					<li><a href="http://connect.mercy.edu/cp/home/loginf" target="_blank">Mercy Connect</a></li>
					<li><a href="/directory">Directory</a></li>
					<li><a href="/events">Calendar</a></li>
					<li><a href="/newsroom">Newsroom</a></li>
					<li><a href="/about-mercy/consumer-information">Consumer Information</a></li>
				</ul>
			</div>
		</div>
	</div>
	<div id ="admissions-and-aid-dropdown" class="green">
		<div class="container">
			<div class="dropdowns-styler">
				<ul>
					<li class="header"><span class="section-title">First Time Freshman<span></li>
					<li><a href="/admissions/first-time-freshman">Overview</a></li>
					<li><a href="/admissions/first-time-freshman/admission-requirements">Admission Requirements</a></li>
					<li><a href="/admissions/first-time-freshman/financial-aid-and-scholarships">Financial Aid and Scholarships</a></li>
					<li><a href="/admissions/first-time-freshman/parents">Parents or Guardians</a></li>
					<li><a href="/admissions/first-time-freshman/guidance-counselors">Guidance Counselors</a></li>
					<li><a href="/admissions/first-time-freshman/high-school-programs">High School Programs</a></li>
				</ul>
				<ul>
					<li class="header"><span class="section-title">Transfers<span></li>
					<li><a href="/admissions/transfers">Overview</a></li>
					<li><a href="/admissions/transfers/admission-requirements">Admission Requirements</a></li>
					<li><a href="/admissions/transfers/transfer-credit-evaluation">Transfer Credit Evaluation</a></li>
					<li><a href="/admissions/transfers/transfer-agreements">Transfer Agreements</a></li>
					<li><a href="/admissions/transfers/financial-aid-and-scholarships">Financial Aid and Scholarships</a></li>
				</ul>
				<ul>
					<li class="header"><span class="section-title">Graduate<span></li>
					<li><a href="/admissions/graduate">Overview</a></li>
					<li><a href="/admissions/graduate/application-process">Application Process</a></li>
					<li><a href=" /admissions/graduate/financial-aid-and-fees">Financial Aid and Fees</a></li>
					<li><a href="/admissions/graduate/graduate-services">Graduate Services</a></li>
                                        <li><a href="/graduate/uft-registration">NYSUT ELT / UFT Online Registration</a></li>
				</ul>
				<ul>
					<li class="header"><span class="section-title">Financial Aid<span></li>
					<li><a href="/admissions/financial-aid">Overview</a></li>
					<li><a href="/admissions/financial-aid/financial-aid-basics">Financial Aid Basics</a></li>
					<li><a href="/admissions/financial-aid/scholarships-and-grants">Scholarships and Grants</a></li>
					<li><a href="/admissions/financial-aid/direct-loan-program">Direct Loan Program</a></li>
					<li><a href="/student-affairs/career-services/federal-work-study">Work Study Program</a></li>
					<li><a href="/admissions/financial-aid/net-price-calculator">Net Price Calculator</a></li>
					<li><a href="/admissions/financial-aid/tuition-and-fees">Tuition and Fees</a></li>
				</ul>
				<ul>
					<li class="header"><span class="section-title">Additional Resources<span></li>
					<li><a href="/admissions/adult">Adult Continuing Education</a></li>
					<li><a href="/admissions/international">International</a></li>
					<li><a href="/admissions/military">Military</a></li>
					<li><a href="/admissions/online">Online</a></li>
					<li><a href="/admissions/honors">Honors</a></li>
				</ul>
			</div>
		</div>
	</div>
	<div id ="academics-dropdown" class="purple">
		<div class="container">
			<div class="dropdowns-styler">
				<ul>
					<li class="header"><span class="section-title">Schools<span></li>
					<li><a href="/business">Business</a></li>
					<li><a href="/education">Education</a></li>
					<li><a href="/health-and-natural-sciences">Health & Natural Sciences</a></li>
					<li><a href="/liberal-arts">Liberal Arts</a></li>
					<li><a href="/social-and-behavioral-sciences/">Social & Behavioral Sciences</a></li>
				</ul>
				<ul>
					<li class="header"><span class="section-title">Leadership<span></li>
					<li><a href="/academics/office-provost">Office of the Provost</a></li>
					<li><a href="/academics/office-provost/provosts-biography">Meet the Provost</a></li>
					<li><a href="/academics/office-provost/provosts-staff">Provost Staff</a></li>
				</ul>
				<ul>
					<li class="header"><span class="section-title">Center for Global Engagement<span></li>
					<li><a href="/global-engagement">Overview</a></li>
					<li><a href="/global-engagement/study-abroad">Study Abroad</a></li>
					<li><a href="/global-engagement/exchange-students">Exchange Students</a></li>
				</ul>
				<ul>
					<li class="header"><span class="section-title"> Libraries </span></li>
					<li><a href="/academics/libraries">Library Home</a></li>
					<li><a href="/academics/libraries/about-libraries">About the Libraries</a></li>
				</ul>
				<ul>
					<li class="header"><span class="section-title">Additional Resources<span></li>
					<li><a href="/degrees-programs">Degrees & Programs</a></li>
					<li><a href="/academics/academic-centers">Academic Centers</a></li>
					<li><a href="/directory">Faculty Directory</a></li>
					<li><a href="/academics/research-and-grants">Research & Grants</a></li>
					<li><a href="/academics/bulletins-catalogs">Bulletins and Catalogs</a></li>
					<li><a href="/academics/academic-tutoring">Center for Academic Excellence and Innovation</a></li>
				</ul>
			</div>
		</div>
	</div>
	<div id ="student-affairs-dropdown" class="red">
		<div class="container">
			<div class="dropdowns-styler">
	            <ul>
					<li class="header"><span class="section-title">Student Affairs<span></li>
					<li><a href="/student-affairs/">Overview</a></li>
					<li><a href="/student-affairs/student-success-and-assessment">Student Success & Assessment</a></li>
					<li><a href="/student-affairs/international-services">International Student\'s Office</a></li>
					<li><a href="/student-affairs/health-office">Health Office</a></li>
					<li><a href="http://www.mercyathletics.com/landing/index">Athletics</a></li>
					<li><a href="/student-affairs/residential-life">Residential Life</a></li>
				</ul>
	               <ul>
				<li class="header"><span class="section-title">Career and Professional Development<span></li>
				<li><a href="/student-affairs/career-services">Overview</a></li>
                                <li><a href="/student-affairs/career-services/current-students">Students</a></li>
                                <li><a href="/student-affairs/career-services/find-internships">Find Internships</a></li>
                                <li><a href="student-affairs/career-services/find-job">Find Jobs</a></li>
                                <li><a href="/student-affairs/career-services/federal-work-study">Federal Work Study</a></li>
				<li><a href="/student-affairs/programs-and-events/lunch-leaders">Lunch with a Leader</a></li>
				<li><a href="/student-affairs/career-services/employers">For Employers</a></li>
			</ul>
	            <ul>
					<li class="header"><span class="section-title">PACT<span></li>
					<li><a href="/student-affairs/pact">Overview</a></li>
					<li><a href="/student-affairs/pact/meet-your-mentor">Meet Your Mentor</a></li>
					<li><a href="/student-affairs/pact/students">Students</a></li>
					<li><a href="/student-affairs/pact/pact-facts">PACT Facts</a></li>
				</ul>
	            <ul>
					<li class="header"><span class="section-title">Student Life<span></li>
					<li><a href="/student-affairs/student-life">Overview</a></li>
					<li><a href="/student-affairs/student-life/events">Events</a></li>
					<li><a href="/student-affairs/student-life/fye">First Year Experience</a></li>
					<li><a href="/student-affairs/student-life/student-activities">Student Activities</a></li>
					<li><a href="/student-affairs/student-life/transportation">Transportation</a></li>
					<li><a href="/student-affairs/student-life/explore-new-york">Explore New York</a></li>
				</ul>
	            <ul>
					<li class="header"><span class="section-title">Additional Resources<span></li>
					<li><a href="/resources/information-technology">Information Technology</a></li>
					<li><a href="https://mercy.blackboard.com/" target="_blank">Blackboard</a></li>
					<li><a href="http://connect.mercy.edu/cp/home/loginf" target="_blank">Mercy Connect</a></li>
					<li><a href="/directory">Directory</a></li>
					<li><a href="/events">Calendar</a></li>
                                        <li><a href="/student-affairs/commencement">Commencement</a></li>
				</ul>
			</div>
		</div>
	</div>
	<div id ="visit-dropdown" class="orange">
		<div class="container">
			<div class="dropdowns-styler">
				<ul>
					<li class="header"><span class="section-title">Dobbs Ferry Campus<span></li>
					<li><a href="/visit/dobbs-ferry-campus">Overview</a></li>
					<li><a href="/visit/dobbs-ferry-campus/directions-dobbs-ferry">Directions</a></li>
					<li><a href="/visit/dobbs-ferry-campus/dobbs-ferry-campus-map">Campus Map</a></li>
					<li><a href="/visit/things-do-westchester">Things to do in Westchester</a></li>
					<li><a href="/visit/dobbs-ferry-campus/local-restaurants-near-dobbs-ferry">Local Restaurants</a></li>
					<li><a href="/visit/dobbs-ferry-campus/nearby-hotels-westchester">Nearby Hotels</a></li>
				</ul>
	            <ul>
					<li class="header"><span class="section-title">Manhattan Campus<span></li>
					<li><a href="/visit/manhattan-campus">Overview</a></li>
					<li><a href="/visit/manhattan-campus/directions-manhattan-campus">Directions</a></li>
					<li><a href="/visit/manhattan-campus/things-do-new-york-city">Things to do in NYC</a></li>
					<li><a href="/visit/manhattan-campus/local-restaurants-near-manhattan-campus">Local Restaurants</a></li>
					<li><a href="/visit/manhattan-campus/nearby-hotels-new-york-city">Nearby Hotels</a></li>
				</ul>

	            <ul>
					<li class="header"><span class="section-title">Bronx Campus<span></li>
					<li><a href="/visit/bronx-campus">Overview</a></li>
					<li><a href="/visit/directions-bronx-campus">Directions</a></li>
					<li><a href="/visit/bronx-campus/things-to-do-in-the-bronx">Things to do in the Bronx</a></li>
					<li><a href="/visit/bronx-campus/local-restaurants-bronx">Local Restaurants</a></li>
					<li><a href="/visit/bronx-campus/nearby-hotels-bronx">Nearby Hotels</a></li>
				</ul>

	            <ul>
					<li class="header"><span class="section-title">Yorktown Heights Campus<span></li>
					<li><a href="/visit/yorktown-campus">Overview</a></li>
					<li><a href="/visit/yorktown-heights-campus/directions-yorktown-heights">Directions</a></li>
					<li><a href="/visit/things-do-westchester">Things to do in Westchester</a></li>
					<li><a href="/visit/dobbs-ferry-campus/local-restaurants-near-dobbs-ferry">Local Restaurants</a></li>
					<li><a href="/visit/dobbs-ferry-campus/nearby-hotels-westchester">Nearby Hotels</a></li>
				</ul>
	            <ul>
					<li class="header"><span class="section-title">Additional Resources<span></li>
					<li><a href="/visit/visitors-and-community">Visitors & Community</a></li>
				</ul>
			</div>
		</div>
	</div>
	<div id ="athletics-dropdown" class="darkblue">
		<div class="container">
			<div class="dropdowns-styler">
	            <ul>
<li class="header"><span class="section-title">Mercy Athletics</span></li>
<li><a target="_blank" href="http://www.mercyathletics.com/">Athletics Homepage</a></li>
<li><a target="_blank" href="http://www.mercyathletics.com/composite">Composite Schedule</a></li>
<li><a target="_blank" href="http://www.mercyathletics.com/information/staff_directory/index">Staff Directory</a></li>
<li><a target="_blank" href="http://www.mercyathletics.com/information/athleticsinformation">Athletics Info</a></li>
<li><a target="_blank" href="http://www.mercyathletics.com/information/prospective_studentathletes/index">Prospective Student-Athletes</a></li>
		</ul>
		<ul>
<li class="header"><span class="section-title">Men\'s Sports</span></li>
<li><a target="_blank" href="http://www.mercyathletics.com/sports/bsb/index">Baseball</a></li>
<li><a target="_blank" href="http://www.mercyathletics.com/sports/mbkb/index">Basketball</a></li>
<li><a target="_blank" href="http://www.mercyathletics.com/sports/mlax/index">Lacrosse</a></li>
<li><a target="_blank" href="http://www.mercyathletics.com/sports/msoc/index">Soccer</a></li>
		</ul>
		<ul>
<li class="header"><span class="section-title">Women\'s Sports</span></li>
<li><a target="_blank" href="http://www.mercyathletics.com/sports/wbkb/index">Basketball</a></li>
<li><a target="_blank" href="http://www.mercyathletics.com/sports/fh/index">Field Hockey</a></li>
<li><a target="_blank" href="http://www.mercyathletics.com/sports/wlax/index">Lacrosse</a></li>
<li><a target="_blank" href="http://www.mercyathletics.com/sports/wsoc/index">Soccer</a></li>
<li><a target="_blank" href="http://www.mercyathletics.com/sports/sball/index">Softball</a></li>
<li><a target="_blank" href="http://www.mercyathletics.com/sports/wvball/index">Volleyball</a></li>
				</ul>
				<ul>
				</ul>
			</div>
		</div>
	</div>
</div>';

  $export['dropdowns_secondary_menus'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Footer';
  $fe_block_boxes->format = 'admin_html';
  $fe_block_boxes->machine_name = 'main_footer';
  $fe_block_boxes->body = '<div class="container">
<div class="footer-menu">
<span class="uppercaser"><b><a href="/about-mercy/">About Mercy</a></b></span><br>
<a href="/about-mercy/mercy-college-profile/mercy-college-history">History</a><br>
<a href="/about-mercy/mission-statement">Mission Statement</a><br>
<a href="/about-mercy/distinctions">Distinctions</a><br>
<a href="/about-mercy/fast-facts">Fast Facts</a><br>
<a href="/about-mercy/accreditations">Accreditations</a><br>
<a href="/about-mercy/leadership">Leadership</a><br>
<a href="/about-mercy/human-resources">Human Resources</a><br>
<a href="/about-mercy/campus-safety-and-security">Safety & Security</a><br><br><br>
<a href="/privacy-policy">Privacy Policy</a><br>
&copy; 2014 Mercy College<br>
</div>

<div class="footer-menu">
<span class="uppercaser"><b><a href="/admissions/">Admissions & Aid</a></b></span><br>
<a href="/admissions/financial-aid">Financial Aid</a><br>
<a href="/admissions/first-time-freshman">First Time Freshman</a><br>
<a href="/admissions/transfers">Transfers</a><br>
<a href="/admissions/graduate">Graduate</a><br>
<a href="/admissions/adult">Adult Continuing Education</a><br>
<a href="/admissions/international">International</a><br>
<a href="/admissions/military">Military</a><br>
<a href="/admissions/online">Online</a><br>
<a href="/admissions/honors">Honors</a><br>
</div>

<div class="footer-menu">
<span class="uppercaser"><b><a href="/academics/">Academics</a></b></span><br>
<a href="/business">Business</a><br>
<a href="/education">Education</a><br>
<a href="/health-and-natural-sciences">Health & Natural Sciences</a><br>
<a href="/liberal-arts">Liberal Arts</a><br>
<a href="/social-and-behavioral-sciences/">Social & Behavioral Sciences</a><br>
<a href="/degrees-programs">Degrees & Programs</a>
<br><br>
<b><a href="/student-affairs/">Student Affairs</a></b><br>
<a href="/student-affairs/career-services">Career Services</a><br>
<a href="/student-affairs/pact">PACT</a><br>
<a href="/student-affairs/student-life">Student Life</a><br>
<a href="/student-affairs/residential-life">Residential Life</a><br>
<a href="http://www.mercyathletics.com/landing/index" target="new">Athletics</a><br>

</div>

<div class="footer-menu">
<span class="uppercaser"><b><a href="/visit/">Visit</a></b></span><br>
<a href="/visit/dobbs-ferry-campus">Dobbs Ferry Campus</a><br>
<a href="/visit/manhattan-campus">Manhattan Campus</a><br>
<a href="/visit/bronx-campus">Bronx Campus</a><br>
<a href="/visit/yorktown-campus">Yorktown Heights Campus</a><br><br>

<div id="map_wrapper" class="map_wrapper">
    <div id="map_canvas" class="map_canvas"></div>
</div>
<div>
<div class="footer_social"style="margin-left:-12px;"><a href="https://www.facebook.com/mercycollegeny" target="_blank"> <img src="/sites/default/files/default_images/footer_fb.png"></a></div>
<div class="footer_social"><a href="https://twitter.com/mercycollege" target="_blank"> <img src="/sites/default/files/default_images/footer_twitter.png"></a></div>
<div class="footer_social"><a href="https://www.youtube.com/user/1800mercyny" target="_blank"> <img src="/sites/default/files/default_images/footer_yt.png"></a></div>
<div class="footer_social"><a href="https://www.instagram.com/mercycollege/" target="_blank"> <img src="/sites/default/files/default_images/footer_ig.png"></a></div>
<div class="footer_social"><a href="#" target="_blank"> <img src="/sites/default/files/default_images/footer_chat.png"></a></div>
</div>
</div>';

  $export['main_footer'] = $fe_block_boxes;

  return $export;
}
