<?php
/**
 * @file
 * mercy_config.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function mercy_config_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "instagram_social_feed" && $api == "default_instagram_social_feed_presets") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_image_default_styles().
 */
function mercy_config_image_default_styles() {
  $styles = array();

  // Exported image style: 110x215.
  $styles['110x215'] = array(
    'label' => '110x215',
    'effects' => array(
      8 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 215,
          'height' => 110,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: 215_x_215.
  $styles['215_x_215'] = array(
    'label' => '215 x 215',
    'effects' => array(
      16 => array(
        'name' => 'image_resize',
        'data' => array(
          'width' => 215,
          'height' => 215,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: 307x307.
  $styles['307x307'] = array(
    'label' => '307x307',
    'effects' => array(
      9 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 307,
          'height' => 307,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: 315x315.
  $styles['315x315'] = array(
    'label' => '315x315',
    'effects' => array(
      11 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 315,
          'height' => 315,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: 361x404.
  $styles['361x404'] = array(
    'label' => '361x404',
    'effects' => array(
      15 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 361,
          'height' => 390,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: 400x200.
  $styles['400x200'] = array(
    'label' => '400x200',
    'effects' => array(
      14 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 400,
          'height' => 200,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: content-kits.
  $styles['content-kits'] = array(
    'label' => 'Content-Kit-Video',
    'effects' => array(
      2 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 600,
          'height' => 400,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: profile_pictiure.
  $styles['profile_pictiure'] = array(
    'label' => 'Profile Pictiure',
    'effects' => array(
      13 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 200,
          'height' => 300,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: programs_top_photo.
  $styles['programs_top_photo'] = array(
    'label' => 'Programs Top Photo',
    'effects' => array(
      10 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 1560,
          'height' => 500,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: top_slider.
  $styles['top_slider'] = array(
    'label' => 'Top Slider',
    'effects' => array(
      6 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 1560,
          'height' => 600,
        ),
        'weight' => -10,
      ),
    ),
  );

  // Exported image style: vertical_content_kit_video.
  $styles['vertical_content_kit_video'] = array(
    'label' => 'Vertical Content Kit Video',
    'effects' => array(
      7 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 400,
          'height' => 600,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}
