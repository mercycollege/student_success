(function ($) {
  Drupal.behaviors.campusMap = {
    attach: function (context, settings) {
      $('#map_wrapper', context).once('load-map-api', function () {
        // Asynchronously Load the map API.
        var script = document.createElement('script');
        script.src = "https://maps.googleapis.com/maps/api/js?sensor=false&callback=initialize";
        document.body.appendChild(script);
      });
    }
  };
}(jQuery));

function initialize() {
  var map;
  var bounds = new google.maps.LatLngBounds();
  var mapOptions = {
    mapTypeId: 'roadmap'
  };

  // Display a map on the page.
  map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
  map.setTilt(45);

  // Multiple Markers.
  var markers = [
    ['Dobbs Ferry Campus', 41.0221023, -73.8745927],
    ['Manhattan Campus', 40.7501772, -73.9869234],
    ['Bronx Campus', 40.8527661, -73.845732],
    ['Yorktown Heights Campus', 41.2955269, -73.8226254]
  ];

  // Info Window Content.
  var infoWindowContent = [
    ['<div class="blarg">' +
    '<h4>Dobbs Ferry Campus</h4>' +
    '<p><a href="https://www.google.com/maps/place/Mercy+College/@41.0221023,-73.8745927,15z/data=!4m2!3m1!1s0x0:0xe7f214ded234828d" target="_blank">555 Broadway<br>Dobbs Ferry, NY</a></p><p>1-877-MERCY-GO</p>' + '</div>'],
    ['<div class="info_content">' +
    '<h4>Manhattan Campus</h4>' +
    '<p><a href="https://www.google.com/maps/place/Mercy+College/@40.7501772,-73.9869234,15z/data=!4m2!3m1!1s0x0:0xad479f58c2ffd467" target="_blank">66 W 35th St<br>New York, NY</a></p><p>1-877-MERCY-GO</p>' + '</div>'],
    ['<div class="info_content">' +
    '<h4>Bronx Campus</h4>' +
    '<p><a href="https://www.google.com/maps/place/Mercy+College/@40.8527661,-73.845732,15z/data=!4m2!3m1!1s0x0:0xdba7eb885e7fd296" target="_blank">1200 Waters Pl<br>Bronx, NY</a></p><p>1-877-MERCY-GO</p>' + '</div>'],
    ['<div class="info_content">' +
    '<h4>Yorktown Heights Campus</h4>' +
    '<p><a href="https://www.google.com/maps/place/Mercy+College/@41.2955269,-73.8226254,17z/data=!3m1!4b1!4m2!3m1!1s0x0:0xb97848885e43682e" target="_blank">2651 Strang Blvd<br>Yorktown Heights, NY</a></p><p>1-877-MERCY-GO</p>' + '</div>'],
  ];

  // Display multiple markers on a map.
  var infoWindow = new google.maps.InfoWindow(), marker, i;

  // Loop through our array of markers & place each one on the map.
  for( i = 0; i < markers.length; i++ ) {
    var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
    bounds.extend(position);
    marker = new google.maps.Marker({
      position: position,
      map: map,
      title: markers[i][0]
    });

    // Allow each marker to have an info window.
    google.maps.event.addListener(marker, 'click', (function(marker, i) {
      return function() {
        infoWindow.setContent(infoWindowContent[i][0]);
        infoWindow.open(map, marker);
      }
    })(marker, i));

    // Automatically center the map fitting all markers on the screen.
    map.fitBounds(bounds);
  }
}
