<?php
/**
 * @file
 * self_manage_dasa.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
 
 
function self_manage_dasa_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_self_manage_dasa|node|self_manage_dasa|form';
  $field_group->group_name = 'group_self_manage_dasa';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'self_manage_dasa';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Form Sections',
    'weight' => '5',
    'children' => array(
      0 => 'field_dasa_section_a',
      1 => 'field_dasa_section_b',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Form Sections',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-self-manage-dasa field-group-fieldset',
        'description' => 'Click "Add another item" To add a new section select option. <br>
All sections will automatically expire on the date the event is held. <br>
You can setup a workshop limit to close a course once it reaches the set amount of confirmed paid registrants, leave blank for no limit, you can increase the limit at anytime<br>
Click "Remove" to remove a section from the select option. <br>
If a section is no longer valid, please remove it to make managing sections easier. <br>
The "Registration Count (Paid)" reflexes the number of people who have registered and paid for a section.',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_self_manage_dasa|node|self_manage_dasa|form'] = $field_group;
  
  return $export;
}
