<?php
/**
 * @file
 * self_manage_dasa.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function self_manage_dasa_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['views-payment_confirmation-block_1'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'payment_confirmation-block_1',
    'icon' => 'a:2:{s:4:"icon";s:0:"";s:8:"position";s:12:"title_before";}',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'node/17671/webform-results',
    'roles' => array(),
    'themes' => array(
      'mercy_bootstrap_subtheme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'mercy_bootstrap_subtheme',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  return $export;
}
