<?php
/**
 * @file
 * ct_basic_page.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function ct_basic_page_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_basic_page_slider|node|page|form';
  $field_group->group_name = 'group_basic_page_slider';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'page';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Slider Pictures',
    'weight' => '5',
    'children' => array(
      0 => 'field_top_slider',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Slider Pictures',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => 'node_page_form_group_basic_page_slider',
        'classes' => 'well',
        'description' => 'Add a slider below the secondary menu along with a slider title and description. You will be able to add more content once the page has been created and saved. Just click the Customize this Page button to the bottom of the page.

You can add subsequent slider images by clicking the "Add another item" button below.',
        'show_label' => '1',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_basic_page_slider|node|page|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_page_title_and_main_body|node|page|form';
  $field_group->group_name = 'group_page_title_and_main_body';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'page';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Page Title',
    'weight' => '3',
    'children' => array(
      0 => 'field_alternate_title',
      1 => 'field_darken_title',
      2 => 'title',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Page Title',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => 'node_page_form_group_page_title_and_main_body',
        'classes' => 'well',
        'description' => 'Add a page title. You will be able to add content once the page has been created and saved. Just click the Customize this Page button to the bottom of the page.',
        'show_label' => '1',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_page_title_and_main_body|node|page|form'] = $field_group;

  return $export;
}
