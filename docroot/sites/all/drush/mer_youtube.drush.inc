<?php
#!/usr/bin/env drush

/**
 * Implements hook_drush_command().
 */
function mer_youtube_drush_command() {
  $items = array();

  $items['mer-youtube-urls'] = array(
    'description' => "Update YouTube URLs to work with YouTube Feed 2.1.",
    'aliases' => array('meryt'),
    'examples' => array(
      'drush mer-youtube-urls' => 'drush meryt',
    ),
  );

  return $items;
}

/**
 * Implements drush_COMMANDFILE().
 */
function drush_mer_youtube_urls() {

  // Check for bootstrap.
  $self = drush_sitealias_get_record('@self');
  if (empty($self)) {
    drush_die("Cannot bootstrap Drupal.", 0);
  }

  if (!drush_confirm('This will alter Media YouTube Item field values. Continue?')) {
    drush_user_abort();
    drush_die();
  }

  drush_print("Getting YouTube Media Item entries...");

  // Find distinct nid values.
  $result = db_query("SELECT nid FROM {node} WHERE type LIKE 'youtube_media_item' ORDER BY nid ASC");

  foreach ($result as $row) {
    $node = node_load($row->nid);

    // Update YouTube video URL value.
    if (isset($node->field_video_url[LANGUAGE_NONE])) {
      $video = $node->field_video_url[LANGUAGE_NONE][0];
      if (isset($video['url']) && $video['url'] != '') {
        $url = $video['url'];
        drupal_set_message("Found $url");
        $url_string = parse_url($url, PHP_URL_QUERY);
        parse_str($url_string, $args);
        $url = isset($args['v']) ? $args['v'] : false;

        if ($url !== false) {
          drupal_set_message("Setting value $url");
          $video['title'] = $url;
          $video['url'] = '';
          $node->field_video_url[LANGUAGE_NONE][0] = $video;
          node_save($node);
        }
      }
    }
  }
  drupal_set_message("Done.");
}
