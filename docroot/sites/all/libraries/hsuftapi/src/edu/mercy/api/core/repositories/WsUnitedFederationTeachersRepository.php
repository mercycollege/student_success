<?php

namespace edu\mercy\api\core\repositories;

use edu\mercy\api\core\base\WsRepository;
use edu\mercy\api\data\models\CashNetSuccess;

class WsUnitedFederationTeachersRepository extends WsRepository {

    public function findAll() {
        return $this->get("/united_federation_teachers/find_all");
    }

    public function findBySchoolCode($schoolCode) {
        return $this->get("/united_federation_teachers/find_by_school_code?schoolCode=$schoolCode");
    }

    public function findByCourseId($courseId) {
        return $this->get("/united_federation_teachers/find_by_course_id?courseId=$courseId");
    }

    public function create(CashNetSuccess $cashNetSuccess) {
        return $this->post("/united_federation_teachers/create", $cashNetSuccess);
    }

}
