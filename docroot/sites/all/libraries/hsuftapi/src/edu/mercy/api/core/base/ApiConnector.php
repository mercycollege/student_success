<?php

namespace edu\mercy\api\core\base;

class ApiConnector {
    
    public $server;
    public $sessionToken;
    
    public function __construct($server, $sessionToken) {
        $this->server = $server;
        $this->sessionToken = $sessionToken;
    }
    
}