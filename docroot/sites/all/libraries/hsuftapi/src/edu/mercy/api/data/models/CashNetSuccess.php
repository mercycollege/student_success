<?php

namespace edu\mercy\api\data\models;

class CashNetSuccess {

    public $wwwId;
    public $term;
    public $ssn;
    public $firstName;
    public $lastName;
    public $street1;
    public $street2;
    public $city;
    public $state;
    public $zip;
    public $phone;
    public $bday;
    public $sex;
    public $email;
    public $courses;
    public $amount;
    public $street3;
    public $natn;
    public $cnty;
    public $mi;
    public $pymntType;
    public $race;
    public $ethn;
    
}