<?php

namespace edu\mercy\api\core\repositories;

use edu\mercy\api\core\base\WsRepository;
use edu\mercy\api\data\models\CashNetSuccess;

class WsHsArticulationsRepository extends WsRepository {

    public function test() {
        return $this->get("/hs_articulation/test");
    }

    public function findAll() {
        return $this->get("/hs_articulation/find_all");
    }

    public function findBySchoolCode($schoolCode) {
        return $this->get("/hs_articulation/find_by_school_code?schoolCode=$schoolCode");
    }

    public function findByCourseId($courseId) {
        return $this->get("/hs_articulation/find_by_course_id?courseId=$courseId");
    }

    public function create(CashNetSuccess $cashNetSuccess) {
        return $this->post("/hs_articulation/create", $cashNetSuccess);
    }

}
