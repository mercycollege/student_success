<?php

namespace edu\mercy\api\core\base;

class WsRepository {

    public $apiConnector;
    public $protocol = 'https://';

    public function __construct(ApiConnector $apiConnector) {
        $this->apiConnector = $apiConnector;
    }

    public function get($uri) {
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $this->protocol . $this->apiConnector->server . $uri,
            CURLOPT_HTTPHEADER => [
                'Authorization:' . $this->apiConnector->sessionToken,
            ],
        ]);
        $resp = curl_exec($curl);

        if ($resp === false) {
            throw new \Exception('CURL error: ' . curl_error($curl));
        }

        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);

        if ($httpcode != 200) {
            throw new \Exception('API error: ' . $httpcode);
        }

        return json_decode($resp);
    }

    public function post($uri, $data) {
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_HTTPHEADER => [
                'Authorization:' . $this->apiConnector->sessionToken
            ],
            CURLOPT_URL => $this->protocol . $this->apiConnector->server . $uri,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => (array) $data,
        ]);
        $resp = curl_exec($curl);

        if ($resp === false) {
            throw new \Exception('CURL error: ' . curl_error($ch));
        }

        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);

        if ($httpcode != 200) {
            throw new \Exception('API error: ' . $httpcode);
        }

        return json_decode($resp);
    }

}
