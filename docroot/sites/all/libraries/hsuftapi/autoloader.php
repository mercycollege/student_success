<?php

namespace edu\mercy\api;
// Auto load classes based on namespace
spl_autoload_register(function ($class) {
    if (strpos($class, __NAMESPACE__) > -1) {
        require_once 'src/' . str_replace(['\\'], ['/'], $class) . '.php';
    }
});