(function ($) {
	url = '/sites/all/scripts/blackboard/courses.php';
	$(document).ready(function() {
		$('#email').focus();
		$('#email').change(function(){
			resetFields();
			$('#from-term').val('');
		})
		$('#from-term').change(function() {
			resetFields();
			email = $('#email').val();
			var from_term = $('#from-term').val();

			var validEmail = /^([\w-\.]+@)(mercy.edu)|(mercymavericks.edu)$/;

			if (!validEmail.test(email)) {
				$('.email-field').addClass('has-error');
				$('.hidden-div').show();

			}
			else if (from_term === '') {
				$('.from_term').addClass('has-error');
			}
			else {
				$('#copy-from').attr("placeholder","Loading...");
				postGet(email, from_term, '#copy-from', 'Transfer From', processSecondPart);
			}
		});
		//Clicking or hitting enter on add selected copies from and to into hidden fields and displays them for user.
		$('.add-selected').click(function(){
			addTheseValues();
		});
		$('.add-selected').keypress(function(e){
	        if(e.which == 13){//Enter key pressed
	            $('selected').click();//Trigger search button click event
	        }
    	});

		$('body').on("click keypress", ".panel-col-last", function() {
			$('#edit-webform-ajax-submit-996').click(function (){
				transferValues();
			});
    	});
    	$('#edit-webform-ajax-submit-996').keypress(function(e){
    		if (e.which == 13) {
    			$('#edit-webform-ajax-submit-996').click();
    		}

    	});
	});

 	var postGet = function ( valueEmail, valueTerm, inputClass, placeholder, afterFunction ) {
 		var posting = $.post( url, { 'email': valueEmail, 'term': valueTerm } );
		posting.done(function(data) {
			$(inputClass).attr("placeholder","Loading...");
			result = $.parseJSON(data);
			$(inputClass).attr("placeholder", placeholder);
			$(inputClass).removeAttr('disabled', 'disabled')
			
			if (result.response == 'fail') {
				courses = ["No results for the selected email and term criteria."];
			}
			else {
				var courses = [];
				$.each(result, function(key, val) {
					object = {};
					object = val.SSBSECT_CRN + '.' + val.SSBSECT_TERM_CODE + '|' + val.SSBSECT_SUBJ_CODE + ' ' + val.SSBSECT_CRSE_NUMB + ' ' + val.SSBSECT_SEQ_NUMB + '|' + val.SCBCRSE_TITLE;
					courses.push(object);
				});
			}

			$(inputClass).autocomplete({
				minLength: 0,
				source: courses,
				open: function () {
		        	$(this).data("uiAutocomplete").menu.element.addClass("blackboard-autocomplete");
		    	},
				select: function(value, data){
					afterFunction(data);
				}
			}).focus(function () {
		    	$(inputClass).autocomplete("search", "");
			});
		});
 	}

 	var processSecondPart = function(data){
 		selectedFrom = data.item.value;
 		if (selectedFrom !== "No results for the selected email and term criteria.") {
 			$('#current-term').removeAttr('disabled', 'disabled');
 		}
 		else {
 			$('#current-term').attr('disabled', 'disabled')
 		}

 		$('#current-term').change(function(){
 			$('.current_term').removeClass('has-error');
 			$('#transfer-to').val('');
 			var current_term = $('#current-term').val();
 			if (current_term === '') {
 				$('.current_term').addClass('has-error');
 				$('#transfer-to').attr("disabled", "disabled");
 			}
 			else {
 				$('#transfer-to').attr("placeholder","Loading...");
 				$('#transfer-to').attr("disabled","disabled");
 				postGet(email, current_term, '#transfer-to', 'Transfer To', processThirdPart);
 			}

 		});
 	}

 	var processThirdPart = function(data){
 		selectedTo = data.item.value;
 		if (selectedTo !== "No results for the selected email and term criteria.") {
 			$('.add-selected').removeAttr('disabled', 'disabled');
 		}
 	}
 	var resetFields = function() {
		$('.hidden-div').hide();
		$('.email-field, .from_term').removeClass('has-error');
		$('#copy-from, #current-term, #transfer-to, #copy-from').val('');
		$('#copy-from, #current-term, #transfer-to, .add-selected').attr("disabled","disabled");

 	}

 	var addTheseValues = function() {
 		$('.course-info-display').fadeIn( "slow", function() {
	 		var count = $(".course-info-list li").length;
	 		var number = 'number-' + (count);
	 		listFrom = $('<li class="well odd" id="from-' + number + '">' + selectedFrom + '</li>');
	 		listTo = $('<li class="well even" id="to-' + number + '">' + selectedTo + '</li>');
	 		$('.course-info-list').append(listFrom);
	 		$('.course-info-list').append(listTo);
	 		$('#transfer-to, #copy-from').val('');
	 		$('.add-selected').attr('disabled', 'disabled');
	 		if (count === 6) {
	 			resetFields();
	 			$('#email, #from-term').attr('disabled', 'disabled');
	 			$('#from-term').val('');
	 			$('.course-info-list').append('<p class="info-list">You have reached the maximum amount of requests.</p>');
	 		}
		});
 	}
 	var transferValues = function() {
		var fname = $('#fname').val();
		var lname = $('#lname').val();
		var cwid = $('#cwid').val();
		var extraCheckEmail = $('#email').val();

		//variables that contain the values of all added courses.
		var from1 = $('#from-number-0').text();
		var to1 = $('#to-number-0').text();
		var from2 = $('#from-number-2').text();
		var to2 = $('#to-number-2').text();
		var from3 = $('#from-number-4').text();
		var to3 = $('#to-number-4').text();
		var from4 = $('#from-number-6').text();
		var to4 = $('#to-number-6').text();

		$('input[name="submitted[first_name]"]').val(fname);
		$('input[name="submitted[last_name]"]').val(lname);
		$('input[name="submitted[cwid]"]').val(cwid);
		$('input[name="submitted[email_address]"]').val(extraCheckEmail);

		$('input[name="submitted[copy_from_1]"]').val(from1);
		$('input[name="submitted[copy_to_1]"]').val(to1);
		$('input[name="submitted[copy_from_2]"]').val(from2);
		$('input[name="submitted[copy_to_2]"]').val(to2);
		$('input[name="submitted[copy_from_3]"]').val(from3);
		$('input[name="submitted[copy_to_3]"]').val(to3);
		$('input[name="submitted[copy_from_4]"]').val(from4);
		$('input[name="submitted[copy_to_4]"]').val(to4);


		if (fname === '') {
			$('.fname-field').addClass('has-error');
		}
		if (lname === '') {
			$('.lname-field').addClass('has-error');
		}
		if (extraCheckEmail === '') {
			$('.email-field').addClass('has-error');
		}
 	}

})(jQuery);

