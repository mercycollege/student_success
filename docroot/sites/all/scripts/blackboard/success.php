<?php
#bootstrap main site
if ($_SERVER['HTTP_HOST'] === 'drupal.mercy.local') {
	$prefix = 'http://';
	define('DRUPAL_ROOT', '/var/www/drupal7');
}
else {
	$prefix = 'https://';
	define('DRUPAL_ROOT', '/var/www/html/website');
}
require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

#include custom module with function to convert component label key to component number
module_load_include('module', 'dynamic_values');
#include webform module to load submissions and update them.
module_load_include('inc', 'webform', 'includes/webform.submissions');

#temporary, Krumo debugging tool being loaded
include DRUPAL_ROOT . '/sites/all/modules/contrib/devel/krumo/class.krumo.php';
drupal_add_js(DRUPAL_ROOT . '/sites/all/modules/contrib/devel/devel_krumo_path.js');

if ($_POST) {
	$allVals = array();
	foreach ($_POST as $a => $b) {
		$allVals[$a] = $b;
	}
	$receipt = $allVals['tx'];

	if ($allVals['EXTAPPID']) {
		$ids = explode(",", $allVals['EXTAPPID']);
		$sid = $ids['1'];
		$page_id = $ids['0'];
		$pmt_type = $allVals['pmttype'];

		$form_obj = db_select('webform_submissions', 'ws')
	      ->fields('ws')
	      ->condition('ws.sid', $sid)
	      ->execute()
	      ->fetchAssoc();
	  		$nid = $form_obj['nid'];

		$origin_url = $prefix . $_SERVER['HTTP_HOST'] . '/node/' . $page_id;
  	}
  	$origin_url = $origin_url ?: $prefix . $_SERVER['HTTP_HOST'];

	if ($allVals['respmessage'] === 'SUCCESS') {

		$submission = webform_get_submission($nid, $sid);
		$node = node_load($nid);
		$mapping = _dynamic_values_webform_component_mapping($node);
		if (isset($submission->data[$mapping['payment_confirmation']][0])) {
			$submission->data[$mapping['payment_confirmation']][0] = 1;
			webform_submission_update($node, $submission);

			if ($nid === '17671') {
				$sections = array($submission->data[$mapping['SECTION_A']][0], $submission->data[$mapping['SECTION_B']][0]);
				$registered_a = str_replace(" ", "_", $allVals['DASA_SECTION_A']);
				$registered_b = str_replace(" ", "_", $allVals['DASA_SECTION_B']);
				dasa_get_view_options($node, $sections, $registered_a, $registered_b);
			}
		}
		drupal_goto($origin_url, array(
			'query' => array(
				'message' => 'Thank you for your payment. An email has been sent. Your receipt number is: ' . $receipt,
				'type' => 'status',
			)
		));
		#krumo($origin_url);
	}
	else {
		drupal_goto($origin_url, array(
			'query' => array(
				'message' => 'Your payment was not completed.',
				'type' => 'error',
			)
		));
	}
}

else {
	echo ("An error has occured, there seems to be no POSTed data");
}

function dasa_get_view_options($node, $sections, $registered_a, $registered_b) {
global $spl_chars;
global $repl_chars;
$items = array();
#$object = node_load(21523);
	foreach($sections as $key => $section) {
		$view = views_get_view('self_manage_dasa', true);
		if ($key === 0) {
			$view->set_display('block');
			$sec = "Part A";
		}
		else {
			$view->set_display('block_1');
			$sec = "Part B";
		}
		$view->execute();
		foreach ($view->result as $item) {
			$campus = $item->field_field_dasa_campus[0]['rendered']['#markup'];
			$date = format_date(strtotime($item->field_field_dasa_date[0]['raw']['value']), 'custom', 'l, F j, Y');
			$time = format_date(strtotime($item->field_field_dasa_date[0]['raw']['value']), 'custom', 'h:ia');
			$time_end = format_date(strtotime($item->field_field_dasa_date[0]['raw']['value2']), 'custom', 'h:ia');
			$room = $item->field_field_dasa_room_number[0]['rendered']['#markup'];
			$facilitator = $item->field_field_dasa_instructor[0]['rendered']['#markup'];

			$dasa_list = "$date $campus DASA $sec $time-$time_end $room $facilitator";

			$safe_key = str_replace($spl_chars, $repl_chars, $dasa_list);

			if (($safe_key === $registered_a) || ($safe_key === $registered_b)) {
				if (isset($item->field_collection_item_field_data_field_dasa_section_a_item_i)) {
					$item_id = $item->field_collection_item_field_data_field_dasa_section_a_item_i;
					$item_obj = entity_load('field_collection_item', array($item->field_collection_item_field_data_field_dasa_section_a_item_i));
				}
				elseif (isset($item->field_collection_item_field_data_field_dasa_section_b_item_i)) {
					$item_id = $item->field_collection_item_field_data_field_dasa_section_b_item_i;
					$item_obj = entity_load('field_collection_item', array($item->field_collection_item_field_data_field_dasa_section_b_item_i));
				}
				$item_obj[$item_id]->field_dasa_current_count['und'][0]['value'] = $item_obj[$item_id]->field_dasa_current_count['und'][0]['value']+1;
				$item_obj[$item_id]->save(TRUE);
				if (isset($item_obj[$item_id]->field_dasa_workshop_limit['und'][0]['value'])) {
					$count = $item_obj[$item_id]->field_dasa_current_count['und'][0]['value'];
					$limit = $item_obj[$item_id]->field_dasa_workshop_limit['und'][0]['value'];
					if ($limit == $count) {
						$item_obj[$item_id]->field_dasa_close_workshop['und'][0]['value'] = 1;
						$item_obj[$item_id]->save(TRUE);
					}
					else {
						$item_obj[$item_id]->field_dasa_close_workshop['und'][0]['value'] = NULL;
						$item_obj[$item_id]->save(TRUE);
					}
				}
			}
		}
	}
	return;
}

?>