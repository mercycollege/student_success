<!-- <!DOCTYPE html>
<html>
<head>
	<title>Blackboard Course Copy</title>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
	<script src="/sites/all/scripts/blackboard/js/blackboard.js"></script>
	<link rel="stylesheet" type="text/css" href="/sites/all/scripts/blackboard/css/style.css">
	<link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
</head>

<body><br><br> -->
	<div class="blackboard-wrapper">
		
		<noscript>Javascript is required to use this form. Please enable javascript.</noscript>
		<div class="form-container">

			<div class="form-group col-md-12 col-md-offset-0 email-field">
			 	<label for="email">Enter your Mercy Email @mercy or @mercymavericks</label>
				<input type='text' name='email' class="form-control" id="email" value="" placeholder="Email Address (Required)">
				<div style="display:none" class="hidden-div alert alert-danger">Please enter a valid Mercy email address.</div>
			</div>

			<div class="form-group col-md-4 fname-field">
			 	<label for="fname">Enter your First Name</label>
				<input type='text' name='fname' class="form-control" id="fname" value="" placeholder="First Name (Required)">
			</div>

			<div class="form-group col-md-4 lname-field">
			 	<label for="fname">Enter your Last Name</label>
				<input type='text' name='fname' class="form-control" id="lname" value="" placeholder="Last Name (Required)">
			</div>

			<div class="form-group col-md-4 cwid-field">
			 	<label for="cwid">Enter your College CWID</label>
				<input type='text' name='cwid' class="form-control" id="cwid" value="" placeholder="CWID (Required)">
			</div>

			<div class="blackboard-from col-md-6">
				<h3>Copy From:</h3>
				<div class="form-group from_term">
					<select name="from_term" id="from-term" class="form-control">
						<?= $terms ?>
					</select>
				</div>
				<div class="form-group transfer_from_wrapper">
					<label for="copy-from">Select Source Course</label>
					<input type="text" name="copy_from" id="copy-from" disabled="disabled" placeholder="Transfer From" class="form-control">
				</div>
			</div>
			<div class="blackboard-to col-md-6">
				<h3>Copy To:</h3>
				<div class="form-group current_term">
					<select name="current_term" id="current-term" disabled="disabled" class="form-control">
						<?= $current_term ?>
					</select>
				</div>
				<div class="form-group">
					<label for="transfer-to">Select Target Course</label>
					<input type="text" name="transfer_to" id="transfer-to" disabled="disabled" placeholder="Transfer To" class="form-control">
				</div>
			</div>
			<div class="form-group add-button">
				<button type="submit" class="btn btn-success add-selected" disabled="disabled" value="submit">Add selected</button>
			</div>
		</div>

		<!-- Form ends here
		Starts info display div -->
		<div style="display:none;" class="course-info-display">
			<ul class="course-info-list">
				<span><h3>Copy From:</h3></span>
				<span><h3>Copy To:</h3></span>
			</ul>

		</div>


	</div>
<!-- 
</body> -->





