#!/usr/bin/env drush

<?php
#Daily exports of online apps data to axiom/banner/talisma crm. First line loads a bash enviroment with php/drush support
#Created by: Jon Garcia jgarcia@ellucian.com/jgarcia@mercy.edu/garciajon@me.com

#accepts arguments specifying the nids of the forms that are going to be exported
$input_id=drush_get_option('nids'); //gets a string containing all specified webforms
if (is_null($input_id)) {
  echo ("No argument specified\n");
  return;
}

#accept range parameters if range is set
$range_start = drush_get_option('range_start'); // gets the sid, serial number, or start date from which to start exporting
$range_end = drush_get_option('range_end');
$date_flag = drush_get_option('date_flag'); // TRUE / FALSE flag


#bootstrap main site
if (isset($_ENV['AH_SITE_ENVIRONMENT'])) {
  switch ($_ENV['AH_SITE_ENVIRONMENT']) {
    case 'dev':
      define('DRUPAL_ROOT', '/var/www/html/mercycollegedev/docroot/');
      $env_root = '/var/www/html/mercycollegedev/docroot/sites/default/files/axiom/RFI/';
      break;

    case 'test':
      define('DRUPAL_ROOT', '/var/www/html/mercycollegestg/docroot/');
      $env_root = '/var/www/html/mercycollegestg/docroot/sites/default/files/axiom/RFI/';
      break;

    case 'prod':
      define('DRUPAL_ROOT', '/var/www/html/mercycollege/docroot/');
      $env_root = '/var/www/html/mercycollege/docroot/sites/default/files/axiom/RFI/';
      break;
  }
}
else {
  //set any local development options.
}

require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

$webform_ids=explode(",", $input_id); //converts arguments to array
#var_dump($webform_ids);
foreach($webform_ids as $wfid) { //iterate through every argument
  #find last webform submission exported by the axiomrunner user
  $query = db_select('webform_last_download', 'wld');
  $query->leftJoin('webform_submissions', 'wfs', 'wld.sid = wfs.sid');
  $info = $query
    ->fields('wld')
    ->fields('wfs', array('serial'))
    ->condition('wld.nid', $wfid)
    ->condition('wld.uid', 5)
    ->execute()
    ->fetchAssoc();

  $last_dl = ($info['sid']);

#echo $last_dl;

  #find last submission to be exported
  $last_submission = db_select('webform_submissions', 'ws')
    ->fields('ws')
    ->condition('ws.nid', $wfid)
    ->condition('ws.is_draft', 0)
    ->orderBy('ws.submitted', 'DESC')
    ->range(0,1)
    ->execute()
    ->fetchAssoc();
  $last_sid = ($last_submission['sid']);
  //var_dump($last_submission);


  /* if the last exported submission and the last submission ids are the same, dont export,
     or if the last submission doesn't exist, it's empty so don't export either
     or if the last exported sid doesn't exist then export all submissions as long as there is at least one new submission. */
  if(!$range_start && (($last_dl == $last_sid) || (is_null($last_sid)) || ((!is_null($last_dl)) && (is_null($last_sid))))) {

    echo "There're no new submissions, or this webform id $wfid has no submissions\n";

  } else {
    /* There is a range of some sort */
    #Get all submissions and create and save a file, calls webform.report.inc via drush command
    $file_path = DRUPAL_ROOT . "/sites/default/files/axiom";
    var_dump($file_path);
    $now = `date '+%m_%d_%y_%H_%M'.txt`;
    if (isset($range_start) && !($date_flag)) {
      $cmd_constant = '--user=5 --range-start=' . $range_start;
      if ($range_end) {
        $cmd_constant .= ' --range-end=' . $range_end . ' --range-type=range --format=delimited --select-keys=1';
      }
      else {
        $cmd_constant .= ' --range-end=' . $last_dl . '--range-type=range --format=delimited --select-keys=1';
      }

    }
    else {
      if ($range_start && $date_flag) {
        $cmd_constant = '--user=5 --range-start=' . $range_start;
        if ($range_end) {
          $cmd_constant .= ' --range-end=' . $range_end . ' --range-type=range-date --format=delimited --select-keys=1';
        }
        else {
          $cmd_constant .= ' --range-end=' . $now . '--range-type=range-date --format=delimited --select-keys=1';
        }
      }
      else {
        #no range was presented so select all new recordsc
        $cmd_constant = '--user=5 --range-type=new --format=delimited --select-keys=1';
        echo("No range was selected");
      }
    }
  }
  switch ($wfid) {
    case 8804:
      $file_name="$file_path/Undergrad_$now";
      $command="drush wfx 8804 $cmd_constant" . ' --delimiter="\t" ' . "--components='1,2,258,4,258,258,127,7,8,258,250,12,11,152,159,160,161,252,163,164,173,174,174,175,28,29,30,255,32,33,35,38,258,258,41,42,258,258,258,258,44,151,150,69,70,71,258,73,258,201,202,203,207,243,208,209,210,211,212,213,89,90,91,92,184,94,95,96,97,98,99,88,100,102,104,106,185,110,112,253,118,119,120,101,103,105,107,186,111,113,115,116,117,121,122,128,47,48,49,50,52,259,54,254,55,56,158,57,59,61,260,63,60,64,36,37,88' --file=$file_name";
      break;
    case 11571:
      $file_name="$file_path/Grad_$now";
      $command="drush wfx 11571 $cmd_constant" . ' --delimiter="\t" ' . "--components='1,180,199,4,5,8,10,199,187,14,13,136,17,18,19,191,142,141,21,199,140,199,25,26,27,192,29,30,31,199,199,32,199,34,35,199,199,199,199,199,181,181,39,40,41,199,43,45,47,49,193,53,55,57,59,60,63,44,46,48,50,194,54,56,58,61,62,63,65,67,69,71,195,75,77,79,81,82,199,199,199,199,199,199,199,199,199,199,199' --file=$file_name";
      break;
    case 35854:
      $file_name="$file_path/Scholar_$now";
      $command="drush wfx 35854 $cmd_constant" . ' --delimiter="\t" ' . "--components='25,2,4,3,14,16,15,5,6,7,8,9,10,11,12,13,33' --file=$file_name";
      break;
    case 18135:
      $file_name="$file_path/RFI/GenericRFIA.txt";
      $command="drush wfx 18135 $cmd_constant --delimiter='|' --components='9,1,35,3,35,35,35,4,35,35,5,6,28,35,8,35,14,35,35,29,27,2,19,12,15,32,34,16,11,35,35,35,35,35,35,35,35,35,35,35,35,35' --file=$file_name";
      break;
    default:
      echo "There is no export yet for specified webform $wfid \n";
  }

  if (!(is_null($command))) {
    drush_shell_exec($command);

    if ($wfid == 18135) {
      $fix_file="sed -i '1,3d' $file_name; sed -i '1iEmail|FirstName|MiddleName|LastName|Suffix|Gender|DateofBirth|Address1|Address2|Address3|City|State|PostalCode|Country|Phone|Mobile|SendSMS|CEEBCODE|HSGradDate|LeadSource|LeadSourceComment|AcademicInterest|StudentType|StartTermCode|Veteran|EventID|GroupTotal|sdgen1|sdgen2|sdgen3|sdgen4|sdgen5|sdgen6|sdgen7|sdgen8|sdgen9|sdgen10|sdgen11|sdgen12|sdgen13|sdgen14|sdgen15 \n' $file_name";
      $campus_code = "sed -i 's/Yorktown_Heights/Yorktown/g' $file_name; sed -i 's/Dobbs_Ferry/Dobbs Ferry/g' $file_name";
      drush_shell_exec($fix_file);
      drush_shell_exec($campus_code);
    }
    $win_line_end = "sed -i 's/$/\r/' $file_name";
    $remove_quotes="sed -i 's/\"//g' $file_name";
    $remove_lead_zeroes="sed -i 's/000-//'  $file_name";
    $remove_lead_ones="sed -i 's/001-//'  $file_name";
    drush_shell_exec($remove_lead_zeroes);
    drush_shell_exec($remove_lead_ones);
    drush_shell_exec($remove_quotes);
    drush_shell_exec($win_line_end);

    #find and remove unwanted characters from the exported file
    if ($wfid != 18135) {
      $remove_headers="sed -i '1,3d' $file_name";
      $remove_vet="sed -i 's/N_O_/ /g' $file_name";
      $remove_phone_prefix="sed -i 's/+1/ /g' $file_name";
      drush_shell_exec($remove_headers);
      drush_shell_exec($remove_vet);
      drush_shell_exec($remove_phone_prefix);
      $form_file1 = current(explode("_".$now, $file_name));
      # echo $form_file1;
     # echo "<br>";

      switch($form_file1){

        case DRUPAL_ROOT . "sites/default/files/axiom/Undergrad":
          $cp="scp $file_name axsftpusr@149.24.140.59:/cygdrive/c/Axiom/import/Admissions/ug/";
          break;
        case DRUPAL_ROOT . "sites/default/files/axiom/Grad":
          $cp = "scp $file_name axsftpusr@149.24.140.59:/cygdrive/c/Axiom/import/Admissions/grad/";
          break;
        case DRUPAL_ROOT . "sites/default/files/axiom/Scholar":
          $cp = "scp $file_name axsftpusr@149.24.140.59:/cygdrive/c/Axiom/import/Admissions/scholar/";
          break;
        default:
          break;
      }


      #secure copy export to the axiom server.
      $form_file = strpos($file_name, 'Undergrad');
      if($form_file !== false) {
        $cp="scp $file_name axsftpusr@149.24.140.59:/cygdrive/c/Axiom/import/Admissions/ug/";
      } else {
        $cp = "scp $file_name axsftpusr@149.24.140.59:/cygdrive/c/Axiom/import/Admissions/grad/";
      }
      $string = trim(preg_replace('/\s\s+/', ' ', $cp));
      //echo "String = ".$string."<br>";
      drush_shell_exec($string);
    }

    if ($wfid == 18135) {
      $sftp = 'sftp -b ' . $env_root . 'sftp.cmd -o User=296sshuser@sftp3.campusnet.net -o IdentityFile="' . $env_root . 'sftp/keys/sftpkey"' . ' sftp3.campusnet.net';
      #echo $sftp . "\n";
      drush_shell_exec($sftp);
    }


    # delete record if it exists
    if (!(is_null($last_dl))) {
      #Delete existing record.
      db_delete('webform_last_download')
        ->condition('nid', $wfid)
        ->condition('uid', 5)
        ->execute();
    }
    #Write new record.
    db_insert('webform_last_download')
      ->fields(array(
        'nid' => $wfid,
        'uid' => 5,
        'sid' => $last_sid,
        'requested' => REQUEST_TIME,
      ))
      ->execute();
  }
}
?>