#!/usr/bin/env bash
cd ../../default
drush updb -y
cd ..
find . -maxdepth 1 -type d -name "preprod.mercy.edu.*" -exec sh -c '(cd {} && drush updb -y )' ';'
