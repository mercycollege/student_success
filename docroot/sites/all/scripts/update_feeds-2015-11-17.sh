#!/usr/bin/env bash
cd $1
drush pm-disable feeds_facebook -y
drush pm-disable feeds_youtube -y
drush dl registry_rebuild -y
drush rr --fire-bazooka
drush dl feeds_facebook -y
drush dl feeds_youtube-7.x-3.x -y
drush rr --fire-bazooka
drush en feeds_facebook -y
drush en feeds_youtube -y
drush en mercy_college_social_feed_theme -y
drush en news_section -y
drush en programs_and_degrees -y
drush updb -y
