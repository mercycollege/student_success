#!/usr/bin/env drush
<?php

#bootstrap main site
$input_id=drush_get_option('nids'); //gets a string containing all specified webforms
if (is_null($input_id)) {
  echo ("No argument specified\n");
  return;
}
#bootstrap main site
define('DRUPAL_ROOT', '/var/www/html/website');
require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
#Daily exports of online apps data to axiom/banner/talisma crm. First line loads a bash enviroment with php/drush support
#Created by: Jon Garcia jgarcia@ellucian.com/jgarcia@mercy.edu/garciajon@me.com

#accepts arguments specifying the nids of the forms that are going to be exported

    /* There is a range of some sort */
    #Get all submissions and create and save a file, calls webform.report.inc via drush command
    $file_path = "/var/www/html/axiom/Scholar";
	$time=time();
	$date=date("Y-m-d", mktime(0,0,0,date("n", $time),date("j",$time)- 1 ,date("Y", $time)));
    //$now = `$date.csv`;
    #no range was presented so select all new recordsc
    $cmd_constant = "--format=excel --header-keys=0 --range-type=range-date --range-start=".$date;

    //$cmd_constant = ' --format=excel --header-keys=0 --range-type=range-date --range-start=2015-12-01';

     //echo "COMMAND =".$cmd_constant;

      $file_name="$file_path/Scholar_App.csv";
	  //echo "File Name =".$file_name."<br>";
      //$command="cd /var/www/html/website; drush wfx 35815 $cmd_constant" . " --file=$file_name";



	  $command="cd /var/www/html/website; drush wfx 35854 ".$cmd_constant." --file=".$file_name;

	  //echo "Command = ".$command."<br>";
    drush_shell_exec($command);


	  $cp = "zip -P Scholarapp1! /var/www/html/axiom/Scholar/Scholar_App.zip /var/www/html/axiom/Scholar/Scholar_App.csv";
	  $cp2 = "chown axiomrunner:apache -R /var/www/html/axiom/Scholar";
      $string = trim(preg_replace('/\s\s+/', ' ', $cp));
	  $string2 = trim(preg_replace('/\s\s+/', ' ', $cp2));
      #echo "$string";
      drush_shell_exec($string);
	  drush_shell_exec($string2);





?>
