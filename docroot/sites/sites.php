<?php

/**
 * @file
 * Configuration file for Drupal's multi-site directory aliasing feature.
 *
 * This file allows you to define a set of aliases that map hostnames, ports, and
 * pathnames to configuration directories in the sites directory. These aliases
 * are loaded prior to scanning for directories, and they are exempt from the
 * normal discovery rules. See default.settings.php to view how Drupal discovers
 * the configuration directory when no alias is found.
 *
 * Aliases are useful on development servers, where the domain name may not be
 * the same as the domain of the live server. Since Drupal stores file paths in
 * the database (files, system table, etc.) this will ensure the paths are
 * correct when the site is deployed to a live server.
 *
 * To use this file, copy and rename it such that its path plus filename is
 * 'sites/sites.php'. If you don't need to use multi-site directory aliasing,
 * then you can safely ignore this file, and Drupal will ignore it too.
 *
 * Aliases are defined in an associative array named $sites. The array is
 * written in the format: '<port>.<domain>.<path>' => 'directory'. As an
 * example, to map http://www.drupal.org:8080/mysite/test to the configuration
 * directory sites/example.com, the array should be defined as:
 * @code
 * $sites = array(
 *   '8080.www.drupal.org.mysite.test' => 'example.com',
 * );
 * @endcode
 * The URL, http://www.drupal.org:8080/mysite/test/, could be a symbolic link or
 * an Apache Alias directive that points to the Drupal root containing
 * index.php. An alias could also be created for a subdomain. See the
 * @link http://drupal.org/documentation/install online Drupal installation guide @endlink
 * for more information on setting up domains, subdomains, and subdirectories.
 *
 * The following examples look for a site configuration in sites/example.com:
 * @code
 * URL: http://dev.drupal.org
 * $sites['dev.drupal.org'] = 'example.com';
 *
 * URL: http://localhost/example
 * $sites['localhost.example'] = 'example.com';
 *
 * URL: http://localhost:8080/example
 * $sites['8080.localhost.example'] = 'example.com';
 *
 * URL: http://www.drupal.org:8080/mysite/test/
 * $sites['8080.www.drupal.org.mysite.test'] = 'example.com';
 * @endcode
 *
 * @see default.settings.php
 * @see conf_path()
 * @see http://drupal.org/documentation/install/multi-site
 */

if ($_SERVER['HTTP_HOST'] == 'mercycollegedev.prod.acquia-sites.com') {
  $sites = array(
    'mercycollegedev.prod.acquia-sites.com.about-mercy' => 'www.mercy.edu.about-mercy',
    'mercycollegedev.prod.acquia-sites.com.academics' => 'www.mercy.edu.academics',
    'mercycollegedev.prod.acquia-sites.com.admissions' => 'www.mercy.edu.admissions',
    'mercycollegedev.prod.acquia-sites.com.alumni' => 'www.mercy.edu.alumni',
    'mercycollegedev.prod.acquia-sites.com.business' => 'www.mercy.edu.business',
    'mercycollegedev.prod.acquia-sites.com.education' => 'www.mercy.edu.education',
    'mercycollegedev.prod.acquia-sites.com.global-engagement' => 'www.mercy.edu.global-engagement',
    'mercycollegedev.prod.acquia-sites.com.health-and-natural-sciences' => 'www.mercy.edu.health-and-natural-sciences',
    'mercycollegedev.prod.acquia-sites.com.liberal-arts' => 'www.mercy.edu.liberal-arts',
    'mercycollegedev.prod.acquia-sites.com.social-and-behavioral-sciences' => 'www.mercy.edu.social-and-behavioral-sciences',
    'mercycollegedev.prod.acquia-sites.com.student-affairs' => 'www.mercy.edu.student-affairs',
    'mercycollegedev.prod.acquia-sites.com.local.visit' => 'www.mercy.edu.visit',
  );

} elseif ($_SERVER['HTTP_HOST'] == 'mercycollegestg.prod.acquia-sites.com') {
  $sites = array(
    'mercycollegestg.prod.acquia-sites.com.about-mercy' => 'www.mercy.edu.about-mercy',
    'mercycollegestg.prod.acquia-sites.com.academics' => 'www.mercy.edu.academics',
    'mercycollegestg.prod.acquia-sites.com.admissions' => 'www.mercy.edu.admissions',
    'mercycollegestg.prod.acquia-sites.com.alumni' => 'www.mercy.edu.alumni',
    'mercycollegestg.prod.acquia-sites.com.business' => 'www.mercy.edu.business',
    'mercycollegestg.prod.acquia-sites.com.education' => 'www.mercy.edu.education',
    'mercycollegestg.prod.acquia-sites.com.global-engagement' => 'www.mercy.edu.global-engagement',
    'mercycollegestg.prod.acquia-sites.com.health-and-natural-sciences' => 'www.mercy.edu.health-and-natural-sciences',
    'mercycollegestg.prod.acquia-sites.com.liberal-arts' => 'www.mercy.edu.liberal-arts',
    'mercycollegestg.prod.acquia-sites.com.social-and-behavioral-sciences' => 'www.mercy.edu.social-and-behavioral-sciences',
    'mercycollegestg.prod.acquia-sites.com.student-affairs' => 'www.mercy.edu.student-affairs',
    'mercycollegestg.prod.acquia-sites.com.visit' => 'www.mercy.edu.visit',
  );

} elseif ($_SERVER['HTTP_HOST'] == 'mercycollege.prod.acquia-sites.com') {
  $sites = array(
    'mercycollege.prod.acquia-sites.com.about-mercy' => 'www.mercy.edu.about-mercy',
    'mercycollege.prod.acquia-sites.com.academics' => 'www.mercy.edu.academics',
    'mercycollege.prod.acquia-sites.com.admissions' => 'www.mercy.edu.admissions',
    'mercycollege.prod.acquia-sites.com.alumni' => 'www.mercy.edu.alumni',
    'mercycollege.prod.acquia-sites.com.business' => 'www.mercy.edu.business',
    'mercycollege.prod.acquia-sites.com.education' => 'www.mercy.edu.education',
    'mercycollege.prod.acquia-sites.com.global-engagement' => 'www.mercy.edu.global-engagement',
    'mercycollege.prod.acquia-sites.com.health-and-natural-sciences' => 'www.mercy.edu.health-and-natural-sciences',
    'mercycollege.prod.acquia-sites.com.liberal-arts' => 'www.mercy.edu.liberal-arts',
    'mercycollege.prod.acquia-sites.com.social-and-behavioral-sciences' => 'www.mercy.edu.social-and-behavioral-sciences',
    'mercycollege.prod.acquia-sites.com.student-affairs' => 'www.mercy.edu.student-affairs',
    'mercycollege.prod.acquia-sites.com.visit' => 'www.mercy.edu.visit',
  );

} elseif ($_SERVER['HTTP_HOST'] == 'www.mercy.edu') {
  $sites = array(
    'www.mercy.edu.about-mercy' => 'www.mercy.edu.about-mercy',
    'www.mercy.edu.academics' => 'www.mercy.edu.academics',
    'www.mercy.edu.admissions' => 'www.mercy.edu.admissions',
    'www.mercy.edu.alumni' => 'www.mercy.edu.alumni',
    'www.mercy.edu.business' => 'www.mercy.edu.business',
    'www.mercy.edu.education' => 'www.mercy.edu.education',
    'www.mercy.edu.global-engagement' => 'www.mercy.edu.global-engagement',
    'www.mercy.edu.health-and-natural-sciences' => 'www.mercy.edu.health-and-natural-sciences',
    'www.mercy.edu.liberal-arts' => 'www.mercy.edu.liberal-arts',
    'www.mercy.edu.social-and-behavioral-sciences' => 'www.mercy.edu.social-and-behavioral-sciences',
    'www.mercy.edu.student-affairs' => 'www.mercy.edu.student-affairs',
    'www.mercy.edu.visit' => 'www.mercy.edu.visit',
  );
}
/**
 * DEV SERVER SUBSITE DEFINITIONS
 */
if ($_SERVER['HTTP_HOST'] === 'mercycollegedev.prod.acquia-sites.com') {

  $sites['mercycollegedev.prod.acquia-sites.com.about-mercy'] = 'www.mercy.edu.about-mercy';
  $sites['mercycollegedev.prod.acquia-sites.com.academics'] = 'www.mercy.edu.academics';
  $sites['mercycollegedev.prod.acquia-sites.com.admissions'] = 'www.mercy.edu.admissions';
  $sites['mercycollegedev.prod.acquia-sites.com.alumni'] = 'www.mercy.edu.alumni';
  $sites['mercycollegedev.prod.acquia-sites.com.business'] = 'www.mercy.edu.business';
  $sites['mercycollegedev.prod.acquia-sites.com.education'] = 'www.mercy.edu.education';
  $sites['mercycollegedev.prod.acquia-sites.com.global-engagement'] = 'www.mercy.edu.global-engagement';
  $sites['mercycollegedev.prod.acquia-sites.com.health-and-natural-sciences'] = 'www.mercy.edu.health-and-natural-sciences';
  $sites['mercycollegedev.prod.acquia-sites.com.liberal-arts'] = 'www.mercy.edu.liberal-arts';
  $sites['mercycollegedev.prod.acquia-sites.com.social-and-behavioral-sciences'] = 'www.mercy.edu.social-and-behavioral-sciences';
  $sites['mercycollegedev.prod.acquia-sites.com.student-affairs'] = 'www.mercy.edu.student-affairs';
  $sites['mercycollegedev.prod.acquia-sites.com.visit'] = 'www.mercy.edu.visit';
} elseif ($_SERVER['HTTP_HOST'] === 'mercycollegestg.prod.acquia-sites.com') {

  /**
   * STAGING SERVER SUBSITE DEFINITIONS
   */

  $sites['mercycollegestg.prod.acquia-sites.com.about-mercy'] = 'www.mercy.edu.about-mercy';
  $sites['mercycollegestg.prod.acquia-sites.com.academics'] = 'www.mercy.edu.academics';
  $sites['mercycollegestg.prod.acquia-sites.com.admissions'] = 'www.mercy.edu.admissions';
  $sites['mercycollegestg.prod.acquia-sites.com.alumni'] = 'www.mercy.edu.alumni';
  $sites['mercycollegestg.prod.acquia-sites.com.business'] = 'www.mercy.edu.business';
  $sites['mercycollegestg.prod.acquia-sites.com.education'] = 'www.mercy.edu.education';
  $sites['mercycollegestg.prod.acquia-sites.com.global-engagement'] = 'www.mercy.edu.global-engagement';
  $sites['mercycollegestg.prod.acquia-sites.com.health-and-natural-sciences'] = 'www.mercy.edu.health-and-natural-sciences';
  $sites['mercycollegestg.prod.acquia-sites.com.liberal-arts'] = 'www.mercy.edu.liberal-arts';
  $sites['mercycollegestg.prod.acquia-sites.com.social-and-behavioral-sciences'] = 'www.mercy.edu.social-and-behavioral-sciences';
  $sites['mercycollegestg.prod.acquia-sites.com.student-affairs'] = 'www.mercy.edu.student-affairs';
  $sites['mercycollegestg.prod.acquia-sites.com.visit'] = 'www.mercy.edu.visit';
} elseif ($_SERVER['HTTP_HOST'] === 'mercycollege.prod.acquia-sites') {

  /**
   * PRODUCTION SERVER SUBSITE DEFINITIONS
   */

  $sites['mercycollege.prod.acquia-sites.com.about-mercy'] = 'www.mercy.edu.about-mercy';
  $sites['mercycollege.prod.acquia-sites.com.academics'] = 'www.mercy.edu.academics';
  $sites['mercycollege.prod.acquia-sites.com.admissions'] = 'www.mercy.edu.admissions';
  $sites['mercycollege.prod.acquia-sites.com.alumni'] = 'www.mercy.edu.alumni';
  $sites['mercycollege.prod.acquia-sites.com.business'] = 'www.mercy.edu.business';
  $sites['mercycollege.prod.acquia-sites.com.education'] = 'www.mercy.edu.education';
  $sites['mercycollege.prod.acquia-sites.com.global-engagement'] = 'www.mercy.edu.global-engagement';
  $sites['mercycollege.prod.acquia-sites.com.health-and-natural-sciences'] = 'www.mercy.edu.health-and-natural-sciences';
  $sites['mercycollege.prod.acquia-sites.com.liberal-arts'] = 'www.mercy.edu.liberal-arts';
  $sites['mercycollege.prod.acquia-sites.com.social-and-behavioral-sciences'] = 'www.mercy.edu.social-and-behavioral-sciences';
  $sites['mercycollege.prod.acquia-sites.com.student-affairs'] = 'www.mercy.edu.student-affairs';
  $sites['mercycollege.prod.acquia-sites.com.visit'] = 'www.mercy.edu.visit';

} elseif ($_SERVER['HTTP_HOST'] === 'www.mercy.edu') {

  $sites['www.mercy.edu.about-mercy'] = 'www.mercy.edu.about-mercy';
  $sites['www.mercy.edu.academics'] = 'www.mercy.edu.academics';
  $sites['www.mercy.edu.admissions'] = 'www.mercy.edu.admissions';
  $sites['www.mercy.edu.alumni'] = 'www.mercy.edu.alumni';
  $sites['www.mercy.edu.business'] = 'www.mercy.edu.business';
  $sites['www.mercy.edu.education'] = 'www.mercy.edu.education';
  $sites['www.mercy.edu.global-engagement'] = 'www.mercy.edu.global-engagement';
  $sites['www.mercy.edu.health-and-natural-sciences'] = 'www.mercy.edu.health-and-natural-sciences';
  $sites['www.mercy.edu.liberal-arts'] = 'www.mercy.edu.liberal-arts';
  $sites['www.mercy.edu.social-and-behavioral-sciences'] = 'www.mercy.edu.social-and-behavioral-sciences';
  $sites['www.mercy.edu.student-affairs'] = 'www.mercy.edu.student-affairs';
  $sites['www.mercy.edu.visit'] = 'www.mercy.edu.visit';
}
